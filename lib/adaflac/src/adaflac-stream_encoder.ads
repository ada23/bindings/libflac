pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with adaflac.ordinals ;
with adaflac.format ;
with adaflac.stream_decoder ;

package adaflac.stream_encoder is

   use Interfaces.C.Extensions ;

   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_OK : constant := 0;  --  ../include/FLAC/stream_encoder.h:291
   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_NOT_COMPILED_WITH_MULTITHREADING_ENABLED : constant := 1;  --  ../include/FLAC/stream_encoder.h:292
   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_ALREADY_INITIALIZED : constant := 2;  --  ../include/FLAC/stream_encoder.h:293
   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_TOO_MANY_THREADS : constant := 3;  --  ../include/FLAC/stream_encoder.h:294

   type FLAC_u_StreamEncoderState is 
     (FLAC_u_STREAM_ENCODER_OK,
      FLAC_u_STREAM_ENCODER_UNINITIALIZED,
      FLAC_u_STREAM_ENCODER_OGG_ERROR,
      FLAC_u_STREAM_ENCODER_VERIFY_DECODER_ERROR,
      FLAC_u_STREAM_ENCODER_VERIFY_MISMATCH_IN_AUDIO_DATA,
      FLAC_u_STREAM_ENCODER_CLIENT_ERROR,
      FLAC_u_STREAM_ENCODER_IO_ERROR,
      FLAC_u_STREAM_ENCODER_FRAMING_ERROR,
      FLAC_u_STREAM_ENCODER_MEMORY_ALLOCATION_ERROR)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:281

   FLAC_u_StreamEncoderStateString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:288
   with Import => True, 
        Convention => C, 
     External_Name => "FLAC__StreamEncoderStateString";
   function State( st : size_t ) return String ;
   function State( st : FLAC_u_StreamEncoderState ) return String ;
   
   type FLAC_u_StreamEncoderInitStatus is 
     (FLAC_u_STREAM_ENCODER_INIT_STATUS_OK,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_ENCODER_ERROR,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_UNSUPPORTED_CONTAINER,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_CALLBACKS,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_NUMBER_OF_CHANNELS,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_BITS_PER_SAMPLE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_SAMPLE_RATE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_BLOCK_SIZE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_MAX_LPC_ORDER,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_QLP_COEFF_PRECISION,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_BLOCK_SIZE_TOO_SMALL_FOR_LPC_ORDER,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_NOT_STREAMABLE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_METADATA,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_ALREADY_INITIALIZED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:355

   FLAC_u_StreamEncoderInitStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:362
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderInitStatusString";
   function InitStatus(st : size_t) return String ;
   function InitStatus(st : FLAC_u_StreamEncoderInitStatus) return String ;
   
   type FLAC_u_StreamEncoderReadStatus is 
     (FLAC_u_STREAM_ENCODER_READ_STATUS_CONTINUE,
      FLAC_u_STREAM_ENCODER_READ_STATUS_END_OF_STREAM,
      FLAC_u_STREAM_ENCODER_READ_STATUS_ABORT,
      FLAC_u_STREAM_ENCODER_READ_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:381

   FLAC_u_StreamEncoderReadStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:388
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderReadStatusString";
   function ReadStatus( st : FLAC_u_StreamEncoderReadStatus) return String ;
   function ReadStatus( st : size_t ) return String ;
   
   type FLAC_u_StreamEncoderWriteStatus is 
     (FLAC_u_STREAM_ENCODER_WRITE_STATUS_OK,
      FLAC_u_STREAM_ENCODER_WRITE_STATUS_FATAL_ERROR)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:401

   FLAC_u_StreamEncoderWriteStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:408
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderWriteStatusString";

   function WriteStatus( st : size_t ) return String ;
   function WriteStatus( st : FLAC_u_StreamEncoderWriteStatus ) return String ;
   
   type FLAC_u_StreamEncoderSeekStatus is 
     (FLAC_u_STREAM_ENCODER_SEEK_STATUS_OK,
      FLAC_u_STREAM_ENCODER_SEEK_STATUS_ERROR,
      FLAC_u_STREAM_ENCODER_SEEK_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:424

   FLAC_u_StreamEncoderSeekStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:431
   with Import => True, 
        Convention => C, 
     External_Name => "FLAC__StreamEncoderSeekStatusString";
   
   function SeekStatus( st : size_t ) return String ;
   function SeekStatus( st : FLAC_u_StreamEncoderSeekStatus ) return String ;
   
   type FLAC_u_StreamEncoderTellStatus is 
     (FLAC_u_STREAM_ENCODER_TELL_STATUS_OK,
      FLAC_u_STREAM_ENCODER_TELL_STATUS_ERROR,
      FLAC_u_STREAM_ENCODER_TELL_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:447

   FLAC_u_StreamEncoderTellStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:454
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderTellStatusString";
   
   function TellStatus( st : size_t ) return String ;
   function TellStatus( st : FLAC_u_StreamEncoderTellStatus ) return String ;

   
   type FLAC_u_StreamEncoderProtected is null record;   -- incomplete struct

   type FLAC_u_StreamEncoderPrivate is null record;   -- incomplete struct

   type FLAC_u_StreamEncoder is record
      protected_u : access FLAC_u_StreamEncoderProtected;  -- ../include/FLAC/stream_encoder.h:470
      private_u : access FLAC_u_StreamEncoderPrivate;  -- ../include/FLAC/stream_encoder.h:471
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/stream_encoder.h:472
     
   type FLAC_u_StreamEncoderReadCallback is access function
        (encoder : access constant FLAC_u_StreamEncoder;
         buffer : System.Address ;
         bytes : access unsigned_long;
         client_data : System.Address) return FLAC_u_StreamEncoderReadStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:523

     
   type FLAC_u_StreamEncoderWriteCallback is access function
        (encoder : access constant FLAC_u_StreamEncoder;
         buffer : System.Address ;
         bytes : size_t;
         samples : Unsigned_32;
         current_frame : Unsigned_32;
         client_data : System.Address) return FLAC_u_StreamEncoderWriteStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:561
     
   type FLAC_u_StreamEncoderSeekCallback is access function
        (encoder : access constant FLAC_u_StreamEncoder;
         absolute_byte_offset : adaflac.ordinals.FLAC_u_uint64;
         client_data : System.Address) return FLAC_u_StreamEncoderSeekStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:595
     
   type FLAC_u_StreamEncoderTellCallback is access function
        (encoder : access constant FLAC_u_StreamEncoder;
         absolute_byte_offset : access Extensions.unsigned_long_long;
         client_data : System.Address) return FLAC_u_StreamEncoderTellStatus
     with Convention => C;  -- ../include/FLAC/stream_encoder.h:640
   
   type FLAC_u_StreamEncoderMetadataCallback is access procedure
        (encoder : access constant FLAC_u_StreamEncoder;
         metadata : access constant adaflac.format.FLAC_u_StreamMetadata;
         client_data : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:659
     
   type FLAC_u_StreamEncoderProgressCallback is access procedure
        (encoder : access constant FLAC_u_StreamEncoder;
         bytes_written : adaflac.ordinals.FLAC_u_uint64;
         samples_written : adaflac.ordinals.FLAC_u_uint64;
         frames_written :  Interfaces.C.Extensions.Unsigned_32;
         total_frames_estimate :  Interfaces.C.Extensions.Unsigned_32;
         client_data : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:682

   function Cnew return access FLAC_u_StreamEncoder  -- ../include/FLAC/stream_encoder.h:698
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_new";

   procedure delete (encoder : access FLAC_u_StreamEncoder)  -- ../include/FLAC/stream_encoder.h:706
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_delete";

   function set_ogg_serial_number (encoder : access FLAC_u_StreamEncoder; serial_number : long) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:732
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_ogg_serial_number";

   function set_verify (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:748
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_verify";

   function set_streamable_subset (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:766
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_streamable_subset";

   function set_channels (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:778
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_channels";

   function set_bits_per_sample (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:794
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_bits_per_sample";

   function set_sample_rate (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:806
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_sample_rate";

   function set_compression_level (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:869
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_compression_level";

   function set_blocksize (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:884
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_blocksize";

   function set_do_mid_side_stereo (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:898
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_mid_side_stereo";

   function set_loose_mid_side_stereo (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:914
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_loose_mid_side_stereo";

   function set_apodization (encoder : access FLAC_u_StreamEncoder; specification : Interfaces.C.Strings.chars_ptr) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:996
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_apodization";

   function set_max_lpc_order (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1008
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_max_lpc_order";

   function set_qlp_coeff_precision (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1022
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_qlp_coeff_precision";

   function set_do_qlp_coeff_prec_search (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1036
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_qlp_coeff_prec_search";

   function set_do_escape_coding (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1048
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_escape_coding";

   function set_do_exhaustive_model_search (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1062
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_exhaustive_model_search";

   function set_min_residual_partition_order (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1085
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_min_residual_partition_order";

   function set_max_residual_partition_order (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1108
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_max_residual_partition_order";

   function set_num_threads (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1154
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_num_threads";

   function set_rice_parameter_search_dist (encoder : access FLAC_u_StreamEncoder; value : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1166
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_rice_parameter_search_dist";

   function set_total_samples_estimate (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_uint64) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1182
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_total_samples_estimate";

   function set_metadata
     (encoder : access FLAC_u_StreamEncoder;
      metadata : System.Address;
      num_blocks : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1265
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_metadata";

   function set_limit_min_bitrate (encoder : access FLAC_u_StreamEncoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1283
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_limit_min_bitrate";

   function get_state (encoder : access constant FLAC_u_StreamEncoder) return FLAC_u_StreamEncoderState  -- ../include/FLAC/stream_encoder.h:1293
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_state";

   function get_verify_decoder_state (encoder : access constant FLAC_u_StreamEncoder) return adaflac.stream_decoder.FLAC_u_StreamDecoderState  -- ../include/FLAC/stream_encoder.h:1305
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_verify_decoder_state";

   function get_resolved_state_string (encoder : access constant FLAC_u_StreamEncoder) return Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:1318
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_resolved_state_string";

   procedure get_verify_decoder_error_stats
     (encoder : access constant FLAC_u_StreamEncoder;
      absolute_sample : access Extensions.unsigned_long_long;
      frame_number : access unsigned;
      channel : access unsigned;
      sample : access unsigned;
      expected : access int;
      got : access int)  -- ../include/FLAC/stream_encoder.h:1337
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_verify_decoder_error_stats";

   function get_verify (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1347
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_verify";

   function get_streamable_subset (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1357
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_streamable_subset";

   function get_channels (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1367
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_channels";

   function get_bits_per_sample (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1377
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_bits_per_sample";

   function get_sample_rate (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1387
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_sample_rate";

   function get_blocksize (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1397
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_blocksize";

   function get_do_mid_side_stereo (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1407
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_mid_side_stereo";

   function get_loose_mid_side_stereo (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1417
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_loose_mid_side_stereo";

   function get_max_lpc_order (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1427
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_max_lpc_order";

   function get_qlp_coeff_precision (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1437
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_qlp_coeff_precision";

   function get_do_qlp_coeff_prec_search (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1447
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_qlp_coeff_prec_search";

   function get_do_escape_coding (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1457
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_escape_coding";

   function get_do_exhaustive_model_search (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1467
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_exhaustive_model_search";

   function get_min_residual_partition_order (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1477
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_min_residual_partition_order";

   function get_max_residual_partition_order (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1487
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_max_residual_partition_order";

   function get_num_threads (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1497
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_num_threads";

   function get_rice_parameter_search_dist (encoder : access constant FLAC_u_StreamEncoder) return Unsigned_32  -- ../include/FLAC/stream_encoder.h:1507
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_rice_parameter_search_dist";

   function get_total_samples_estimate (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_uint64  -- ../include/FLAC/stream_encoder.h:1520
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_total_samples_estimate";

   function get_limit_min_bitrate (encoder : access constant FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1530
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_limit_min_bitrate";

   function init_stream
     (encoder : access FLAC_u_StreamEncoder;
      write_callback : FLAC_u_StreamEncoderWriteCallback;
      seek_callback : FLAC_u_StreamEncoderSeekCallback;
      tell_callback : FLAC_u_StreamEncoderTellCallback;
      metadata_callback : FLAC_u_StreamEncoderMetadataCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1593
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_stream";

   function init_ogg_stream
     (encoder : access FLAC_u_StreamEncoder;
      read_callback : FLAC_u_StreamEncoderReadCallback;
      write_callback : FLAC_u_StreamEncoderWriteCallback;
      seek_callback : FLAC_u_StreamEncoderSeekCallback;
      tell_callback : FLAC_u_StreamEncoderTellCallback;
      metadata_callback : FLAC_u_StreamEncoderMetadataCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1661
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_ogg_stream";

   function init_FILE
     (encoder : access FLAC_u_StreamEncoder;
      the_file : ICS.FILEs;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1696
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_FILE";

   function init_ogg_FILE
     (encoder : access FLAC_u_StreamEncoder;
      the_file : ICS.FILEs;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1731
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_ogg_FILE";

   function init_file
     (encoder : access FLAC_u_StreamEncoder;
      filename : Interfaces.C.Strings.chars_ptr;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1767
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_file";

   function init_ogg_file
     (encoder : access FLAC_u_StreamEncoder;
      filename : Interfaces.C.Strings.chars_ptr;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1803
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_ogg_file";

   function finish (encoder : access FLAC_u_StreamEncoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1831
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_finish";

   function process
     (encoder : access FLAC_u_StreamEncoder;
      buffer : System.Address;
      samples : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1858
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_process";

   function process_interleaved
     (encoder : access FLAC_u_StreamEncoder;
      buffer : System.Address ;
      samples : Unsigned_32) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1890
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_process_interleaved";

end adaflac.stream_encoder ;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
