with System;
with Interfaces;
with Interfaces.C;         use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C_Streams;
with Interfaces.C.Extensions;
with Ada.Text_Io ;
package Adaflac is
   subtype adaflac_byte is Interfaces.Unsigned_8;
   subtype adaflac_int is Interfaces.C.int;
   package ICS renames Interfaces.C_Streams;
   package Unsigned_32_Text_Io is new Ada.Text_Io.Modular_Io( Interfaces.Unsigned_32 );
   package ExtUnsigned_32_text_Io is new Ada.Text_Io.Modular_Io( Interfaces.C.Extensions.Unsigned_32 );
end Adaflac;
