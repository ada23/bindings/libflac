pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");



with adaflac.ordinals ;
with adaflac.format ;

package adaflac.stream_decoder is

     use Interfaces.C.Extensions;

   type FLAC_u_StreamDecoderState is 
     (FLAC_u_STREAM_DECODER_SEARCH_FOR_METADATA,
      FLAC_u_STREAM_DECODER_READ_METADATA,
      FLAC_u_STREAM_DECODER_SEARCH_FOR_FRAME_SYNC,
      FLAC_u_STREAM_DECODER_READ_FRAME,
      FLAC_u_STREAM_DECODER_END_OF_STREAM,
      FLAC_u_STREAM_DECODER_OGG_ERROR,
      FLAC_u_STREAM_DECODER_SEEK_ERROR,
      FLAC_u_STREAM_DECODER_ABORTED,
      FLAC_u_STREAM_DECODER_MEMORY_ALLOCATION_ERROR,
      FLAC_u_STREAM_DECODER_UNINITIALIZED)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:244

   FLAC_u_StreamDecoderStateString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:251
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderStateString";
   function State(st : size_t) return String ;
   function State(st : FLAC_u_StreamDecoderState) return String ;
   
   type FLAC_u_StreamDecoderInitStatus is 
     (FLAC_u_STREAM_DECODER_INIT_STATUS_OK,
      FLAC_u_STREAM_DECODER_INIT_STATUS_UNSUPPORTED_CONTAINER,
      FLAC_u_STREAM_DECODER_INIT_STATUS_INVALID_CALLBACKS,
      FLAC_u_STREAM_DECODER_INIT_STATUS_MEMORY_ALLOCATION_ERROR,
      FLAC_u_STREAM_DECODER_INIT_STATUS_ERROR_OPENING_FILE,
      FLAC_u_STREAM_DECODER_INIT_STATUS_ALREADY_INITIALIZED)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:282

   FLAC_u_StreamDecoderInitStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:289
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderInitStatusString";
   function InitStatus(st : size_t) return String ;
   function InitStatus(st : FLAC_u_StreamDecoderInitStatus) return String ;
   
   type FLAC_u_StreamDecoderReadStatus is 
     (FLAC_u_STREAM_DECODER_READ_STATUS_CONTINUE,
      FLAC_u_STREAM_DECODER_READ_STATUS_END_OF_STREAM,
      FLAC_u_STREAM_DECODER_READ_STATUS_ABORT)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:313

   FLAC_u_StreamDecoderReadStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:320
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderReadStatusString";
   function ReadStatus( st : size_t ) return String ;
   function ReadStatus( st : FLAC_u_StreamDecoderReadStatus ) return String ;
   
   type FLAC_u_StreamDecoderSeekStatus is 
     (FLAC_u_STREAM_DECODER_SEEK_STATUS_OK,
      FLAC_u_STREAM_DECODER_SEEK_STATUS_ERROR,
      FLAC_u_STREAM_DECODER_SEEK_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:336

   FLAC_u_StreamDecoderSeekStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:343
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderSeekStatusString";
   function SeekStatus( st : size_t ) return String ;
   function SeekStatus( st : FLAC_u_StreamDecoderSeekStatus ) return String ;
   
   type FLAC_u_StreamDecoderTellStatus is 
     (FLAC_u_STREAM_DECODER_TELL_STATUS_OK,
      FLAC_u_STREAM_DECODER_TELL_STATUS_ERROR,
      FLAC_u_STREAM_DECODER_TELL_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:359

   
   FLAC_u_StreamDecoderTellStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:366
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderTellStatusString";
   function TellStatus( st : size_t ) return String ;
   function TellStatus( st : FLAC_u_StreamDecoderTellStatus ) return String ;
   
   type FLAC_u_StreamDecoderLengthStatus is 
     (FLAC_u_STREAM_DECODER_LENGTH_STATUS_OK,
      FLAC_u_STREAM_DECODER_LENGTH_STATUS_ERROR,
      FLAC_u_STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:382

   FLAC_u_StreamDecoderLengthStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:389
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderLengthStatusString";
   function LengthStatus( st : size_t ) return String ;
   function LengthStatus( st : FLAC_u_StreamDecoderLengthStatus ) return String ;
   
   type FLAC_u_StreamDecoderWriteStatus is 
     (FLAC_u_STREAM_DECODER_WRITE_STATUS_CONTINUE,
      FLAC_u_STREAM_DECODER_WRITE_STATUS_ABORT)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:402

   FLAC_u_StreamDecoderWriteStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:409
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderWriteStatusString";
   function WriteStatus( st : size_t ) return String ;
   function WriteStatus( st : FLAC_u_StreamDecoderWriteStatus ) return String ;
   
   type FLAC_u_StreamDecoderErrorStatus is 
     (FLAC_u_STREAM_DECODER_ERROR_STATUS_LOST_SYNC,
      FLAC_u_STREAM_DECODER_ERROR_STATUS_BAD_HEADER,
      FLAC_u_STREAM_DECODER_ERROR_STATUS_FRAME_CRC_MISMATCH,
      FLAC_u_STREAM_DECODER_ERROR_STATUS_UNPARSEABLE_STREAM,
      FLAC_u_STREAM_DECODER_ERROR_STATUS_BAD_METADATA)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:448

   FLAC_u_StreamDecoderErrorStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:455
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamDecoderErrorStatusString";
   function ErrorStatus( st : size_t ) return String ;
   function ErrorStatus( st : FLAC_u_StreamDecoderErrorStatus ) return String ;
   
   type FLAC_u_StreamDecoderProtected is null record;   -- incomplete struct

   type FLAC_u_StreamDecoderPrivate is null record;   -- incomplete struct

   type FLAC_u_StreamDecoder is record
      protected_u : access FLAC_u_StreamDecoderProtected;  -- ../include/FLAC/stream_decoder.h:471
      private_u : access FLAC_u_StreamDecoderPrivate;  -- ../include/FLAC/stream_decoder.h:472
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/stream_decoder.h:473

   type FLAC_u_StreamDecoderReadCallback is access function
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : access unsigned_char;
         arg3 : access unsigned_long;
         arg4 : System.Address) return FLAC_u_StreamDecoderReadStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:524

   type FLAC_u_StreamDecoderSeekCallback is access function
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : adaflac.ordinals.FLAC_u_uint64;
         arg3 : System.Address) return FLAC_u_StreamDecoderSeekStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:559

   type FLAC_u_StreamDecoderTellCallback is access function
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : access Extensions.unsigned_long_long;
         arg3 : System.Address) return FLAC_u_StreamDecoderTellStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:597

   type FLAC_u_StreamDecoderLengthCallback is access function
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : access Extensions.unsigned_long_long;
         arg3 : System.Address) return FLAC_u_StreamDecoderLengthStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:635

   type FLAC_u_StreamDecoderEofCallback is access function (arg1 : access constant FLAC_u_StreamDecoder; arg2 : System.Address) return adaflac.ordinals.FLAC_u_bool
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:662

   type FLAC_u_StreamDecoderWriteCallback is access function
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : access constant adaflac.format.FLAC_u_Frame;
         arg3 : System.Address;
         arg4 : System.Address) return FLAC_u_StreamDecoderWriteStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:690

   type FLAC_u_StreamDecoderMetadataCallback is access procedure
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : access constant adaflac.format.FLAC_u_StreamMetadata;
         arg3 : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:717

   type FLAC_u_StreamDecoderErrorCallback is access procedure
        (arg1 : access constant FLAC_u_StreamDecoder;
         arg2 : FLAC_u_StreamDecoderErrorStatus;
         arg3 : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:734

   function Cnew return access FLAC_u_StreamDecoder  -- ../include/FLAC/stream_decoder.h:750
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_new";

   procedure delete (decoder : access FLAC_u_StreamDecoder)  -- ../include/FLAC/stream_decoder.h:758
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_delete";

   function set_ogg_serial_number (decoder : access FLAC_u_StreamDecoder; serial_number : long) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:783
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_ogg_serial_number";

   function set_md5_checking (decoder : access FLAC_u_StreamDecoder; value : adaflac.ordinals.FLAC_u_bool) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:805
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_md5_checking";

   function set_metadata_respond (decoder : access FLAC_u_StreamDecoder; c_type : adaflac.format.FLAC_u_MetadataType) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:819
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_metadata_respond";

   function set_metadata_respond_application (decoder : access FLAC_u_StreamDecoder; id : access unsigned_char) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:834
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_metadata_respond_application";

   function set_metadata_respond_all (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:846
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_metadata_respond_all";

   function set_metadata_ignore (decoder : access FLAC_u_StreamDecoder; c_type : adaflac.format.FLAC_u_MetadataType) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:860
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_metadata_ignore";

   function set_metadata_ignore_application (decoder : access FLAC_u_StreamDecoder; id : access unsigned_char) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:875
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_metadata_ignore_application";

   function set_metadata_ignore_all (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:887
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_set_metadata_ignore_all";

   function get_state (decoder : access constant FLAC_u_StreamDecoder) return FLAC_u_StreamDecoderState  -- ../include/FLAC/stream_decoder.h:897
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_state";

   function get_resolved_state_string (decoder : access constant FLAC_u_StreamDecoder) return Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_decoder.h:907
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_resolved_state_string";

   function get_md5_checking (decoder : access constant FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:921
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_md5_checking";

   function get_total_samples (decoder : access constant FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_uint64  -- ../include/FLAC/stream_decoder.h:933
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_total_samples";

   function get_channels (decoder : access constant FLAC_u_StreamDecoder) return Unsigned_32  -- ../include/FLAC/stream_decoder.h:945
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_channels";

   function get_channel_assignment (decoder : access constant FLAC_u_StreamDecoder) return adaflac.format.FLAC_u_ChannelAssignment  -- ../include/FLAC/stream_decoder.h:957
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_channel_assignment";

   function get_bits_per_sample (decoder : access constant FLAC_u_StreamDecoder) return Unsigned_32  -- ../include/FLAC/stream_decoder.h:969
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_bits_per_sample";

   function get_sample_rate (decoder : access constant FLAC_u_StreamDecoder) return Unsigned_32  -- ../include/FLAC/stream_decoder.h:981
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_sample_rate";

   function get_blocksize (decoder : access constant FLAC_u_StreamDecoder) return Unsigned_32  -- ../include/FLAC/stream_decoder.h:993
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_blocksize";

   function get_decode_position (decoder : access constant FLAC_u_StreamDecoder; position : access Extensions.unsigned_long_long) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1014
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_decode_position";

   function get_client_data (decoder : access FLAC_u_StreamDecoder) return System.Address  -- ../include/FLAC/stream_decoder.h:1024
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_get_client_data";

   function init_stream
     (decoder : access FLAC_u_StreamDecoder;
      read_callback : FLAC_u_StreamDecoderReadCallback;
      seek_callback : FLAC_u_StreamDecoderSeekCallback;
      tell_callback : FLAC_u_StreamDecoderTellCallback;
      length_callback : FLAC_u_StreamDecoderLengthCallback;
      eof_callback : FLAC_u_StreamDecoderEofCallback;
      write_callback : FLAC_u_StreamDecoderWriteCallback;
      metadata_callback : FLAC_u_StreamDecoderMetadataCallback;
      error_callback : FLAC_u_StreamDecoderErrorCallback;
      client_data : System.Address) return FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC/stream_decoder.h:1090
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_init_stream";

   function init_ogg_stream
     (decoder : access FLAC_u_StreamDecoder;
      read_callback : FLAC_u_StreamDecoderReadCallback;
      seek_callback : FLAC_u_StreamDecoderSeekCallback;
      tell_callback : FLAC_u_StreamDecoderTellCallback;
      length_callback : FLAC_u_StreamDecoderLengthCallback;
      eof_callback : FLAC_u_StreamDecoderEofCallback;
      write_callback : FLAC_u_StreamDecoderWriteCallback;
      metadata_callback : FLAC_u_StreamDecoderMetadataCallback;
      error_callback : FLAC_u_StreamDecoderErrorCallback;
      client_data : System.Address) return FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC/stream_decoder.h:1171
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_init_ogg_stream";

   function init_FILE
     (decoder : access FLAC_u_StreamDecoder;
      the_file : ICS.FILEs;
      write_callback : FLAC_u_StreamDecoderWriteCallback;
      metadata_callback : FLAC_u_StreamDecoderMetadataCallback;
      error_callback : FLAC_u_StreamDecoderErrorCallback;
      client_data : System.Address) return FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC/stream_decoder.h:1221
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_init_FILE";

   function init_ogg_FILE
     (decoder : access FLAC_u_StreamDecoder;
      the_file : ICS.FILEs;
      write_callback : FLAC_u_StreamDecoderWriteCallback;
      metadata_callback : FLAC_u_StreamDecoderMetadataCallback;
      error_callback : FLAC_u_StreamDecoderErrorCallback;
      client_data : System.Address) return FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC/stream_decoder.h:1271
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_init_ogg_FILE";

   function init_file
     (decoder : access FLAC_u_StreamDecoder;
      filename : Interfaces.C.Strings.chars_ptr;
      write_callback : FLAC_u_StreamDecoderWriteCallback;
      metadata_callback : FLAC_u_StreamDecoderMetadataCallback;
      error_callback : FLAC_u_StreamDecoderErrorCallback;
      client_data : System.Address) return FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC/stream_decoder.h:1317
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_init_file";

   function init_ogg_file
     (decoder : access FLAC_u_StreamDecoder;
      filename : Interfaces.C.Strings.chars_ptr;
      write_callback : FLAC_u_StreamDecoderWriteCallback;
      metadata_callback : FLAC_u_StreamDecoderMetadataCallback;
      error_callback : FLAC_u_StreamDecoderErrorCallback;
      client_data : System.Address) return FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC/stream_decoder.h:1367
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_init_ogg_file";

   function finish (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1395
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_finish";

   function flush (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1410
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_flush";

   function reset (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1442
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_reset";

   function process_single (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1475
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_process_single";

   function process_until_end_of_metadata (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1496
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_process_until_end_of_metadata";

   function process_until_end_of_stream (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1517
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_process_until_end_of_stream";

   function skip_single_frame (decoder : access FLAC_u_StreamDecoder) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1558
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_skip_single_frame";

   function seek_absolute (decoder : access FLAC_u_StreamDecoder; sample : adaflac.ordinals.FLAC_u_uint64) return adaflac.ordinals.FLAC_u_bool  -- ../include/FLAC/stream_decoder.h:1576
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_decoder_seek_absolute";

end adaflac.stream_decoder;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
