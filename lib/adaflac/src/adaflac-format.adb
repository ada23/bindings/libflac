package body Adaflac.format is
   function VERSION_STRING return String is
   begin
      return Value (FLAC_u_VERSION_STRING);
   end VERSION_STRING;
   function VENDOR_STRING return String is
   begin
      return Value (FLAC_u_VENDOR_STRING);
   end VENDOR_STRING;

   function EntropyCodingMethodType(mt : size_t) return String is
   begin
      return Value(FLAC_u_EntropyCodingMethodTypeString(mt)) ;
   end EntropyCodingMethodType ;

   function EntropyCodingMethodType(mt : FLAC_u_EntropyCodingMethodType) return String is
      idx : constant size_t := FLAC_u_EntropyCodingMethodType'pos(mt) ;
   begin
      return Value(FLAC_u_EntropyCodingMethodTypeString(idx)) ;
   end EntropyCodingMethodType ;

   function ChannelAssignment(ca : size_t) return String is
   begin
      return Value(FLAC_u_ChannelAssignmentString(ca)) ;
   end ChannelAssignment ;

   function ChannelAssignment(ca : FLAC_u_ChannelAssignment) return String is
      idx : constant size_t := FLAC_u_ChannelAssignment'pos(ca);
   begin
      return Value(FLAC_u_ChannelAssignmentString(idx)) ;
   end ChannelAssignment ;


end Adaflac.format;
