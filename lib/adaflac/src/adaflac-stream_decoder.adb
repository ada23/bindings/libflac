package body adaflac.stream_decoder is

   function State(st : size_t) return String is
   begin
      return Value( FLAC_u_StreamDecoderStateString (st) ) ;
   end State ;

   function State(st : FLAC_u_StreamDecoderState) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderState'pos(st) );
   begin
      return Value( FLAC_u_StreamDecoderStateString (idx) ) ;
   end State ;

   function InitStatus(st : size_t) return String is
   begin
      return Value(FLAC_u_StreamDecoderInitStatusString(st)) ;
   end InitStatus ;

   function InitStatus(st : FLAC_u_StreamDecoderInitStatus) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderInitStatus'pos(st) );
   begin
      return Value( FLAC_u_StreamDecoderInitStatusString (idx) ) ;
   end InitStatus ;

   function ReadStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamDecoderReadStatusString(st)) ;
   end ReadStatus ;

   function ReadStatus( st : FLAC_u_StreamDecoderReadStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderReadStatus'pos(st) );
   begin
      return Value (FLAC_u_StreamDecoderReadStatusString(idx)) ;
   end ReadStatus ;

   function SeekStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamDecoderSeekStatusString(st)) ;
   end SeekStatus ;

   function SeekStatus( st : FLAC_u_StreamDecoderSeekStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderSeekStatus'pos(st) );
   begin
      return Value (FLAC_u_StreamDecoderSeekStatusString(idx)) ;
   end SeekStatus ;

   function TellStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamDecoderTellStatusString(st)) ;
   end TellStatus ;

   function TellStatus( st : FLAC_u_StreamDecoderTellStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderTellStatus'pos(st) );
   begin
      return Value (FLAC_u_StreamDecoderTellStatusString(idx)) ;
   end TellStatus ;


   function LengthStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamDecoderLengthStatusString(st)) ;
   end LengthStatus ;

   function LengthStatus( st : FLAC_u_StreamDecoderLengthStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderLengthStatus'pos(st) );
   begin
      return Value (FLAC_u_StreamDecoderLengthStatusString(idx)) ;
   end LengthStatus ;

   function WriteStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamDecoderWriteStatusString(st)) ;
   end WriteStatus ;

   function WriteStatus( st : FLAC_u_StreamDecoderWriteStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamDecoderWriteStatus'pos(st) );
   begin
      return Value (FLAC_u_StreamDecoderWriteStatusString(idx)) ;
   end WriteStatus ;

   function ErrorStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamDecoderErrorStatusString(st)) ;
   end ErrorStatus ;

   function ErrorStatus( st : FLAC_u_StreamDecoderErrorStatus ) return String is
      idx : constant size_t := size_t( FLAC_u_StreamDecoderErrorStatus'pos(st) );
   begin
      return Value (FLAC_u_StreamDecoderErrorStatusString(idx)) ;
   end ErrorStatus ;


end adaflac.stream_decoder ;
