package body adaflac.stream_encoder is

   function State( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamEncoderStateString(st)) ;
   end State ;

   function State( st : FLAC_u_StreamEncoderState ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamEncoderState'pos(st) );
   begin
      return Value(FLAC_u_StreamEncoderStateString(idx)) ;
   end State ;

   function InitStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamEncoderInitStatusString(st)) ;
   end InitStatus ;

   function InitStatus( st : FLAC_u_StreamEncoderInitStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamEncoderInitStatus'pos(st) );
   begin
      return Value(FLAC_u_StreamEncoderInitStatusString(idx)) ;
   end InitStatus ;

   function ReadStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamEncoderReadStatusString(st)) ;
   end ReadStatus ;

   function ReadStatus( st : FLAC_u_StreamEncoderReadStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamEncoderReadStatus'pos(st) );
   begin
      return Value(FLAC_u_StreamEncoderReadStatusString(idx)) ;
   end ReadStatus ;

   function WriteStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamEncoderWriteStatusString(st)) ;
   end WriteStatus ;

   function WriteStatus( st : FLAC_u_StreamEncoderWriteStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamEncoderWriteStatus'pos(st) );
   begin
      return Value(FLAC_u_StreamEncoderWriteStatusString(idx)) ;
   end WriteStatus ;

   function SeekStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamEncoderSeekStatusString(st)) ;
   end SeekStatus ;

   function SeekStatus( st : FLAC_u_StreamEncoderSeekStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamEncoderSeekStatus'pos(st) );
   begin
      return Value(FLAC_u_StreamEncoderSeekStatusString(idx)) ;
   end SeekStatus ;

   function TellStatus( st : size_t ) return String is
   begin
      return Value(FLAC_u_StreamEncoderTellStatusString(st)) ;
   end TellStatus ;

   function TellStatus( st : FLAC_u_StreamEncoderTellStatus ) return String is
      idx : constant  size_t := size_t( FLAC_u_StreamEncoderTellStatus'pos(st) );
   begin
      return Value(FLAC_u_StreamEncoderTellStatusString(idx)) ;
   end TellStatus ;

end adaflac.stream_encoder ;
