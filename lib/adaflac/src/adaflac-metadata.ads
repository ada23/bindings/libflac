pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

--with Interfaces.C; use Interfaces.C;
--with Interfaces.C.Strings;

with adaflac.format;
with adaflac.ordinals;
with System;

with adaflac.callback;

with Interfaces.C.Extensions;

package adaflac.metadata is

   function get_streaminfo
     (filename   : Interfaces.C.Strings.chars_ptr;
      streaminfo : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:164
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_get_streaminfo";

   function get_tags
     (filename : Interfaces.C.Strings.chars_ptr; tags : System.Address)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:183
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_get_tags";

   function get_cuesheet
     (filename : Interfaces.C.Strings.chars_ptr; cuesheet : System.Address)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:202
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_get_cuesheet";

   function get_picture
     (filename    : Interfaces.C.Strings.chars_ptr; picture : System.Address;
      c_type      : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_Picture_Type;
      mime_type   : Interfaces.C.Strings.chars_ptr;
      description : access unsigned_char;
      max_width   : Interfaces.C.Extensions.Unsigned_32;
      max_height  : Interfaces.C.Extensions.Unsigned_32;
      max_depth   : Interfaces.C.Extensions.Unsigned_32;
      max_colors  : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:242
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_get_picture";

   type FLAC_u_Metadata_SimpleIterator is null record;   -- incomplete struct

   type FLAC_u_Metadata_SimpleIteratorStatus is
     (FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_OK,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_ILLEGAL_INPUT,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_ERROR_OPENING_FILE,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_NOT_A_FLAC_FILE,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_NOT_WRITABLE,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_BAD_METADATA,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_READ_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_SEEK_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_WRITE_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_RENAME_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_UNLINK_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_MEMORY_ALLOCATION_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_INTERNAL_ERROR) with
      Convention => C;  -- ../include/FLAC/metadata.h:355

   FLAC_u_Metadata_SimpleIteratorStatusString : array
     (size_t) of Interfaces.C.Strings
     .chars_ptr  -- ../include/FLAC/metadata.h:362
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__Metadata_SimpleIteratorStatusString";

   function simple_iterator_new
      return access FLAC_u_Metadata_SimpleIterator  -- ../include/FLAC/metadata.h:370
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_new";

   procedure FLAC_u_metadata_simple_iterator_delete
     (iterator : access FLAC_u_Metadata_SimpleIterator)  -- ../include/FLAC/metadata.h:378
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_delete";

   function simple_iterator_status
     (iterator : access FLAC_u_Metadata_SimpleIterator)
      return FLAC_u_Metadata_SimpleIteratorStatus  -- ../include/FLAC/metadata.h:390
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_status";

   function simple_iterator_init
     (iterator            : access FLAC_u_Metadata_SimpleIterator;
      filename            : Interfaces.C.Strings.chars_ptr;
      read_only           : ADAFLAC.ORDINALS.FLAC_u_bool;
      preserve_file_stats : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:416
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_init";

   function simple_iterator_is_writable
     (iterator : access constant FLAC_u_Metadata_SimpleIterator)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:428
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_is_writable";

   function simple_iterator_next
     (iterator : access FLAC_u_Metadata_SimpleIterator)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:442
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_next";

   function simple_iterator_prev
     (iterator : access FLAC_u_Metadata_SimpleIterator)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:456
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_prev";

   function simple_iterator_is_last
     (iterator : access constant FLAC_u_Metadata_SimpleIterator)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:469
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_is_last";

   function simple_iterator_get_block_offset
     (iterator : access constant FLAC_u_Metadata_SimpleIterator)
      return adaflac.ordinals
     .FLAC_u_uint64 -- ../include/FLAC/metadata.h:485
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_get_block_offset";

   function simple_iterator_get_block_type
     (iterator : access constant FLAC_u_Metadata_SimpleIterator)
      return ADAFLAC.FORMAT
     .FLAC_u_MetadataType  -- ../include/FLAC/metadata.h:499
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_get_block_type";

   function simple_iterator_get_block_length
     (iterator : access constant FLAC_u_Metadata_SimpleIterator)
      return Interfaces.C.Extensions
     .Unsigned_32  -- ../include/FLAC/metadata.h:516
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_get_block_length";

   function simple_iterator_get_application_id
     (iterator : access FLAC_u_Metadata_SimpleIterator;
      id       : access unsigned_char) return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:541
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_get_application_id";

   function simple_iterator_get_block
     (iterator : access FLAC_u_Metadata_SimpleIterator)
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:559
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_get_block";

   function simple_iterator_set_block
     (iterator    : access FLAC_u_Metadata_SimpleIterator;
      block       : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      use_padding : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:615
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_set_block";

   function simple_iterator_insert_block_after
     (iterator    : access FLAC_u_Metadata_SimpleIterator;
      block       : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      use_padding : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:640
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_insert_block_after";

   function simple_iterator_delete_block
     (iterator    : access FLAC_u_Metadata_SimpleIterator;
      use_padding : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:659
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_simple_iterator_delete_block";

   type FLAC_u_Metadata_Chain is null record;   -- incomplete struct

   type FLAC_u_Metadata_Iterator is null record;   -- incomplete struct

   type FLAC_u_Metadata_ChainStatus is
     (FLAC_u_METADATA_CHAIN_STATUS_OK,
      FLAC_u_METADATA_CHAIN_STATUS_ILLEGAL_INPUT,
      FLAC_u_METADATA_CHAIN_STATUS_ERROR_OPENING_FILE,
      FLAC_u_METADATA_CHAIN_STATUS_NOT_A_FLAC_FILE,
      FLAC_u_METADATA_CHAIN_STATUS_NOT_WRITABLE,
      FLAC_u_METADATA_CHAIN_STATUS_BAD_METADATA,
      FLAC_u_METADATA_CHAIN_STATUS_READ_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_SEEK_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_WRITE_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_RENAME_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_UNLINK_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_MEMORY_ALLOCATION_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_INTERNAL_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_INVALID_CALLBACKS,
      FLAC_u_METADATA_CHAIN_STATUS_READ_WRITE_MISMATCH,
      FLAC_u_METADATA_CHAIN_STATUS_WRONG_WRITE_CALL) with
      Convention => C;  -- ../include/FLAC/metadata.h:793

   FLAC_u_Metadata_ChainStatusString : array
     (size_t) of Interfaces.C.Strings
     .chars_ptr  -- ../include/FLAC/metadata.h:800
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__Metadata_ChainStatusString";

   function chain_new
      return access FLAC_u_Metadata_Chain  -- ../include/FLAC/metadata.h:809
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_new";

   procedure FLAC_u_metadata_chain_delete
     (chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:817
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_delete";

   function chain_status
     (chain : access FLAC_u_Metadata_Chain)
      return FLAC_u_Metadata_ChainStatus  -- ../include/FLAC/metadata.h:829
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_status";

   function chain_read
     (chain    : access FLAC_u_Metadata_Chain;
      filename : Interfaces.C.Strings.chars_ptr)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:848
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_read";

   function chain_read_ogg
     (chain    : access FLAC_u_Metadata_Chain;
      filename : Interfaces.C.Strings.chars_ptr)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:870
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_read_ogg";

   function chain_read_with_callbacks
     (chain     : access FLAC_u_Metadata_Chain;
      handle    : ADAFLAC.CALLBACK.FLAC_u_IOHandle;
      callbacks : ADAFLAC.CALLBACK.FLAC_u_IOCallbacks)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:892
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_read_with_callbacks";

   function chain_read_ogg_with_callbacks
     (chain     : access FLAC_u_Metadata_Chain;
      handle    : ADAFLAC.CALLBACK.FLAC_u_IOHandle;
      callbacks : ADAFLAC.CALLBACK.FLAC_u_IOCallbacks)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:917
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_read_ogg_with_callbacks";

   function chain_check_if_tempfile_needed
     (chain       : access FLAC_u_Metadata_Chain;
      use_padding : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:945
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_check_if_tempfile_needed";

   function chain_write
     (chain               : access FLAC_u_Metadata_Chain;
      use_padding         : ADAFLAC.ORDINALS.FLAC_u_bool;
      preserve_file_stats : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:991
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_write";

   function chain_write_with_callbacks
     (chain       : access FLAC_u_Metadata_Chain;
      use_padding : ADAFLAC.ORDINALS.FLAC_u_bool;
      handle      : ADAFLAC.CALLBACK.FLAC_u_IOHandle;
      callbacks   : ADAFLAC.CALLBACK.FLAC_u_IOCallbacks)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1021
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_write_with_callbacks";

   function chain_write_with_callbacks_and_tempfile
     (chain          : access FLAC_u_Metadata_Chain;
      use_padding    : ADAFLAC.ORDINALS.FLAC_u_bool;
      handle         : ADAFLAC.CALLBACK.FLAC_u_IOHandle;
      callbacks      : ADAFLAC.CALLBACK.FLAC_u_IOCallbacks;
      temp_handle    : ADAFLAC.CALLBACK.FLAC_u_IOHandle;
      temp_callbacks : ADAFLAC.CALLBACK.FLAC_u_IOCallbacks)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1072
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_write_with_callbacks_and_tempfile";

   procedure FLAC_u_metadata_chain_merge_padding
     (chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:1086
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_merge_padding";

   procedure FLAC_u_metadata_chain_sort_padding
     (chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:1101
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_chain_sort_padding";

   function iterator_new
      return access FLAC_u_Metadata_Iterator  -- ../include/FLAC/metadata.h:1111
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_new";

   procedure FLAC_u_metadata_iterator_delete
     (iterator : access FLAC_u_Metadata_Iterator)  -- ../include/FLAC/metadata.h:1119
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_delete";

   procedure FLAC_u_metadata_iterator_init
     (iterator : access FLAC_u_Metadata_Iterator;
      chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:1130
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_init";

   function iterator_next
     (iterator : access FLAC_u_Metadata_Iterator)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1144
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_next";

   function iterator_prev
     (iterator : access FLAC_u_Metadata_Iterator)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1158
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_prev";

   function iterator_get_block_type
     (iterator : access constant FLAC_u_Metadata_Iterator)
      return ADAFLAC.FORMAT
     .FLAC_u_MetadataType  -- ../include/FLAC/metadata.h:1170
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_get_block_type";

   function iterator_get_block
     (iterator : access FLAC_u_Metadata_Iterator)
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:1191
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_get_block";

   function iterator_set_block
     (iterator : access FLAC_u_Metadata_Iterator;
      block    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1208
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_set_block";

   function iterator_delete_block
     (iterator             : access FLAC_u_Metadata_Iterator;
      replace_with_padding : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1226
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_delete_block";

   function iterator_insert_block_before
     (iterator : access FLAC_u_Metadata_Iterator;
      block    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1245
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_insert_block_before";

   function iterator_insert_block_after
     (iterator : access FLAC_u_Metadata_Iterator;
      block    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1263
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_iterator_insert_block_after";

   function object_new
     (c_type : ADAFLAC.FORMAT.FLAC_u_MetadataType)
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:1333
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_new";

   function object_clone
     (object : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:1347
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_clone";

   procedure FLAC_u_metadata_object_delete
     (object : access ADAFLAC.FORMAT
        .FLAC_u_StreamMetadata)  -- ../include/FLAC/metadata.h:1358
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_delete";

   function object_is_equal
     (block1 : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      block2 : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1373
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_is_equal";

   function object_application_set_data
     (object : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      data   : access unsigned_char;
      length : Interfaces.C.Extensions.Unsigned_32;
      copy   : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1396
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_application_set_data";

   function object_seektable_resize_points
     (object         : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      new_num_points : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1414
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_resize_points";

   procedure FLAC_u_metadata_object_seektable_set_point
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      point_num : Interfaces.C.Extensions.Unsigned_32;
      point     : ADAFLAC.FORMAT
        .FLAC_u_StreamMetadata_SeekPoint)  -- ../include/FLAC/metadata.h:1426
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_set_point";

   function object_seektable_insert_point
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      point_num : Interfaces.C.Extensions.Unsigned_32;
      point     : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_SeekPoint)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1440
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_insert_point";

   function object_seektable_delete_point
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      point_num : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1453
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_delete_point";

   function object_seektable_is_legal
     (object : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1466
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_is_legal";

   function object_seektable_template_append_placeholders
     (object : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      num    : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1483
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_template_append_placeholders";

   function object_seektable_template_append_point
     (object        : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      sample_number : ADAFLAC.ORDINALS.FLAC_u_uint64)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1500
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_template_append_point";

   function object_seektable_template_append_points
     (object         : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      sample_numbers : access Extensions.unsigned_long_long;
      num            : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1518
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_template_append_points";

   function object_seektable_template_append_spaced_points
     (object        : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      num           : Interfaces.C.Extensions.Unsigned_32;
      total_samples : ADAFLAC.ORDINALS.FLAC_u_uint64)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1540
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_template_append_spaced_points";

   function object_seektable_template_append_spaced_points_by_samples
     (object        : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      samples       : Interfaces.C.Extensions.Unsigned_32;
      total_samples : ADAFLAC.ORDINALS.FLAC_u_uint64)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1568
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_template_append_spaced_points_by_samples";

   function object_seektable_template_sort
     (object  : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      compact : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1585
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_seektable_template_sort";

   function object_vorbiscomment_set_vendor_string
     (object  : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      c_entry : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy    : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1610
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_set_vendor_string";

   function object_vorbiscomment_resize_comments
     (object           : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      new_num_comments : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1628
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_resize_comments";

   function object_vorbiscomment_set_comment
     (object      : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      comment_num : Interfaces.C.Extensions.Unsigned_32;
      c_entry     : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy        : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1655
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_set_comment";

   function object_vorbiscomment_insert_comment
     (object      : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      comment_num : Interfaces.C.Extensions.Unsigned_32;
      c_entry     : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy        : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1685
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_insert_comment";

   function object_vorbiscomment_append_comment
     (object  : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      c_entry : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy    : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1710
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_append_comment";

   function object_vorbiscomment_replace_comment
     (object  : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      c_entry : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      c_all   : ADAFLAC.ORDINALS.FLAC_u_bool;
      copy    : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1745
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_replace_comment";

   function object_vorbiscomment_delete_comment
     (object      : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      comment_num : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1758
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_delete_comment";

   function object_vorbiscomment_entry_from_name_value_pair
     (c_entry : access ADAFLAC.FORMAT
        .FLAC_u_StreamMetadata_VorbisComment_Entry;
      field_name  : Interfaces.C.Strings.chars_ptr;
      field_value : Interfaces.C.Strings.chars_ptr)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1779
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_entry_from_name_value_pair";

   function object_vorbiscomment_entry_to_name_value_pair
     (c_entry    : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      field_name : System.Address; field_value : System.Address)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1800
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_entry_to_name_value_pair";

   function object_vorbiscomment_entry_matches
     (c_entry : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_VorbisComment_Entry;
      field_name        : Interfaces.C.Strings.chars_ptr;
      field_name_length : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1814
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_entry_matches";

   function object_vorbiscomment_find_entry_from
     (object     : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      offset     : Interfaces.C.Extensions.Unsigned_32;
      field_name : Interfaces.C.Strings.chars_ptr)
      return int  -- ../include/FLAC/metadata.h:1833
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_find_entry_from";

   function object_vorbiscomment_remove_entry_matching
     (object     : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      field_name : Interfaces.C.Strings.chars_ptr)
      return int  -- ../include/FLAC/metadata.h:1846
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_remove_entry_matching";

   function object_vorbiscomment_remove_entries_matching
     (object     : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      field_name : Interfaces.C.Strings.chars_ptr)
      return int  -- ../include/FLAC/metadata.h:1859
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_vorbiscomment_remove_entries_matching";

   function object_cuesheet_track_new
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata_CueSheet_Track  -- ../include/FLAC/metadata.h:1868
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_new";

   function object_cuesheet_track_clone
     (object : access constant ADAFLAC.FORMAT
        .FLAC_u_StreamMetadata_CueSheet_Track)
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata_CueSheet_Track  -- ../include/FLAC/metadata.h:1883
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_clone";

   procedure FLAC_u_metadata_object_cuesheet_track_delete
     (object : access ADAFLAC.FORMAT
        .FLAC_u_StreamMetadata_CueSheet_Track)  -- ../include/FLAC/metadata.h:1891
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_delete";

   function object_cuesheet_track_resize_indices
     (object          : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num       : Interfaces.C.Extensions.Unsigned_32;
      new_num_indices : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1912
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_resize_indices";

   function object_cuesheet_track_insert_index
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32;
      index_num : Interfaces.C.Extensions.Unsigned_32;
      index     : ADAFLAC.FORMAT.FLAC_u_StreamMetadata_CueSheet_Index)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1935
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_insert_index";

   function object_cuesheet_track_insert_blank_index
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32;
      index_num : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1959
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_insert_blank_index";

   function object_cuesheet_track_delete_index
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32;
      index_num : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1978
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_track_delete_index";

   function object_cuesheet_resize_tracks
     (object         : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      new_num_tracks : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:1996
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_resize_tracks";

   function object_cuesheet_set_track
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32;
      track     : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata_CueSheet_Track;
      copy      : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2018
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_set_track";

   function object_cuesheet_insert_track
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32;
      track     : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata_CueSheet_Track;
      copy      : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2041
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_insert_track";

   function object_cuesheet_insert_blank_track
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2060
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_insert_blank_track";

   function object_cuesheet_delete_track
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      track_num : Interfaces.C.Extensions.Unsigned_32)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2075
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_delete_track";

   function object_cuesheet_is_legal
     (object : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      check_cd_da_subset : ADAFLAC.ORDINALS.FLAC_u_bool;
      violation          : System.Address) return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2096
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_is_legal";

   function object_cuesheet_calculate_cddb_id
     (object : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return ADAFLAC.ORDINALS
     .FLAC_u_uint32  -- ../include/FLAC/metadata.h:2109
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_cuesheet_calculate_cddb_id";

   function object_picture_set_mime_type
     (object    : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      mime_type : Interfaces.C.Strings.chars_ptr;
      copy      : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2132
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_picture_set_mime_type";

   function object_picture_set_description
     (object      : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      description : access unsigned_char; copy : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2154
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_picture_set_description";

   function object_picture_set_data
     (object : access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      data   : access unsigned_char; length : ADAFLAC.ORDINALS.FLAC_u_uint32;
      copy   : ADAFLAC.ORDINALS.FLAC_u_bool)
      return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2179
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_picture_set_data";

   function object_picture_is_legal
     (object    : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      violation : System.Address) return ADAFLAC.ORDINALS
     .FLAC_u_bool  -- ../include/FLAC/metadata.h:2198
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_picture_is_legal";

   function object_get_raw
     (object : access constant ADAFLAC.FORMAT.FLAC_u_StreamMetadata)
      return access unsigned_char  -- ../include/FLAC/metadata.h:2212
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_get_raw";

   function object_set_raw
     (buffer : access unsigned_char; length : ADAFLAC.ORDINALS.FLAC_u_uint32)
      return access ADAFLAC.FORMAT
     .FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:2227
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC__metadata_object_set_raw";

end adaflac.metadata;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
