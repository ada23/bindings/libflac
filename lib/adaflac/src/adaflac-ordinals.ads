pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Ada.Text_Io;
package Adaflac.ordinals is

   subtype FLAC_u_int8 is
     Short_Short_Integer;  -- ../include/FLAC/ordinals.h:41

   subtype FLAC_u_uint8 is
     Interfaces.Unsigned_8;  -- ../include/FLAC/ordinals.h:42

   subtype FLAC_u_int16 is Short_Integer;  -- ../include/FLAC/ordinals.h:44

   subtype FLAC_u_int32 is Integer;  -- ../include/FLAC/ordinals.h:45

   subtype FLAC_u_int64 is Long_Integer;  -- ../include/FLAC/ordinals.h:46

   subtype FLAC_u_uint16 is
     Interfaces.Unsigned_16;  -- ../include/FLAC/ordinals.h:47

   subtype FLAC_u_uint32 is
     Interfaces.Unsigned_32;  -- ../include/FLAC/ordinals.h:48

   subtype FLAC_u_uint64 is
     Interfaces.Unsigned_64;  -- ../include/FLAC/ordinals.h:49

   subtype FLAC_u_bool is Interfaces.C.int;  -- ../include/FLAC/ordinals.h:51
    FLAC_False : constant FLAC_u_bool := 0;
    FLAC_True : constant FLAC_u_bool := 1;
   subtype FLAC_u_byte is
     Interfaces.Unsigned_8;  -- ../include/FLAC/ordinals.h:53

   package uint64_text_io is new Ada.Text_Io.Modular_Io( FLAC_u_uint64 );
end Adaflac.ordinals;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
