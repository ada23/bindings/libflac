pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

package Adaflac.export is

   FLAC_API_VERSION_CURRENT  : constant := 14;  --  ../include/FLAC/export.h:98
   FLAC_API_VERSION_REVISION : constant := 0;  --  ../include/FLAC/export.h:99
   FLAC_API_VERSION_AGE      : constant := 2;  --  ../include/FLAC/export.h:100

   FLAC_API_SUPPORTS_OGG_FLAC : aliased int  -- ../include/FLAC/export.h:107
   with
      Import        => True,
      Convention    => C,
      External_Name => "FLAC_API_SUPPORTS_OGG_FLAC";

end Adaflac.export;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
