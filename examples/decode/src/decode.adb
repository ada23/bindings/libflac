with System ;
with Ada.Text_IO;            use Ada.Text_IO;
-- with Ada.Text_IO.C_Streams;
with Ada.Streams.Stream_Io ;

with Ada.Command_Line;       use Ada.Command_Line;
with Interfaces.C.Strings ; use Interfaces.C.Strings;

with GNAT.Source_Info; use GNAT.Source_Info;

with Adaflac.stream_decoder ; use Adaflac.stream_decoder ;
with Adaflac.ordinals;

with callbacks ;

procedure Decode is
   use Interfaces.C ;

   verbose : constant boolean := True ;
   name : constant String := gnat.Source_Info.enclosing_entity ;
   procedure T1 is
      myname : constant String := gnat.Source_Info.enclosing_entity ;
      --logfile : File_Type ;
      decoder : access adaflac.stream_decoder.FLAC_u_StreamDecoder
        := adaflac.stream_decoder.Cnew;
      init_decoder : adaflac.stream_decoder.FLAC_u_StreamDecoderInitStatus ;
      bstatus : adaflac.ordinals.FLAC_u_bool ;

   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;

      callbacks.Open_Output( Argument(2));
      bstatus := adaflac.stream_decoder.set_md5_checking(decoder, adaflac.ordinals.FLAC_True);

      init_decoder := adaflac.stream_decoder.init_file(decoder
                                                       , New_String( Argument(1) )
                                                       , callbacks.Writecallback'Access
                                                       , callbacks.MetadataCallback'access
                                                       , callbacks.ErrorCallback'access
                                                       , System.Null_Address ) ;
      if init_decoder /= adaflac.stream_decoder.FLAC_u_STREAM_DECODER_INIT_STATUS_OK
      then
         Put("Error initializing ");
         Put(adaflac.stream_decoder.InitStatus(init_decoder));
         New_Line;
         return ;
      end if ;

      bstatus := adaflac.stream_decoder.process_until_end_of_stream(decoder) ;
      if bstatus = adaflac.ordinals.Flac_True
      then
         Put_Line("Finished Decoding");
         bstatus := callbacks.EofCallback(decoder,System.Null_Address);
      else
         Put_Line("Decoding Failed ");
         Put_Line(adaflac.stream_decoder.State(adaflac.stream_decoder.get_state(decoder)));
      end if ;

      callbacks.Close ;
      adaflac.stream_decoder.delete(decoder);
   end T1 ;
begin
   if verbose
   then
      Put(name); Put(" ");
      Put(Compilation_ISO_Date); Put(" ");
      Put(Compilation_Time);
      New_Line;
   end if ;
   T1 ;
end Decode;
