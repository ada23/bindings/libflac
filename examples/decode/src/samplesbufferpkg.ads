With System ;
with Interfaces ; use Interfaces;
generic
   type DataType is range <> ;
   Def_MaxSamples : Integer := 4096 ;
   Def_Channels : Integer := 2 ;
package SamplesBufferPkg is
   function Get(from : System.Address ;
                Channel : Integer ;
                SampleNo : Integer ;
                NumChannels : Integer := Def_Channels ;
                MaxSamples : Integer := Def_MaxSamples ) return DataType;
end SamplesBufferPkg ;
