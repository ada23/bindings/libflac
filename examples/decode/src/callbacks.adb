with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Streams.Stream_Io ;

with Interfaces ; use Interfaces ;
with gnat.Source_Info ; use gnat.Source_Info;
with hex ;
with System.Aux_DEC ;

with SamplesBufferPkg ;

package body callbacks is
   
   current_metadata : adaflac.format.FLAC_u_StreamMetadata ;
   wavFile : aliased Ada.Streams.Stream_Io.File_Type ;
   wavStream : Ada.Streams.Stream_Io.Stream_Access ;
   
   function Get is
      new System.Aux_DEC.Fetch_From_Address( Ada.Streams.Stream_Io.Stream_Access );
   --:= Ada.Streams.Stream_Io.Stream(wavFile) ;
   
   package ByteSamplesPkg is new SamplesBufferPkg( Short_Short_Integer ) ;
   package WordSamplesPkg is new SamplesBufferPkg( Short_Integer ) ;
   function ReadCallback
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : access Interfaces.C.unsigned_char;
         userdata : access Interfaces.C.unsigned_long;
         arg4 : System.Address) return FLAC_u_StreamDecoderReadStatus is
    begin
        return FLAC_u_STREAM_DECODER_READ_STATUS_CONTINUE ;
    end ReadCallback;



   function SeekCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : adaflac.ordinals.FLAC_u_uint64;
         userdata : System.Address) return FLAC_u_StreamDecoderSeekStatus is
    begin
        return FLAC_u_STREAM_DECODER_SEEK_STATUS_OK ;
    end SeekCallback;


   function TellCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : access Interfaces.C.unsigned_long_long;
         userdata : System.Address) return FLAC_u_StreamDecoderTellStatus is
    begin
        return FLAC_u_STREAM_DECODER_TELL_STATUS_OK ;
    end TellCallback;


   function LengthCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : access Interfaces.C.unsigned_long_long;
         userdata : System.Address) return FLAC_u_StreamDecoderLengthStatus is
    begin
        return FLAC_u_STREAM_DECODER_LENGTH_STATUS_OK ;
    end LengthCallback;
    

   function EofCallback (decoder : access constant FLAC_u_StreamDecoder; arg2 : System.Address) 
            return adaflac.ordinals.FLAC_u_bool is
   begin
      Put(Enclosing_Entity); Put(" > "); 
      return adaflac.ordinals.FLAC_True ;
    end EofCallback;


   function WriteCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         frame : access constant adaflac.format.FLAC_u_Frame;
         buffer : System.Address;
         userdata : System.Address) return FLAC_u_StreamDecoderWriteStatus is
      sample_u8 : Short_Short_Integer ;
      sample_u16 : Short_Integer ;

      totalSize : Unsigned_32 ;
      hdr : WAVHeaderType ;

      
   begin
      if Verbose
      then
         Put("Frame: channels "); Put(Integer(frame.header.channels)) ;
         Put(" "); Put( adaflac.format.ChannelAssignment(frame.header.channel_assignment) ) ;
         Put(" blocksize "); Put(Integer(frame.header.blocksize)); 
         Put(" bits/sample "); Put(Integer(frame.header.bits_per_sample));
         Put(" No "); Put(Integer(frame.header.number.sample_number));
         Put(" crc : "); Put(Integer(frame.footer.crc),base=>16);
         New_Line;
      end if ;
      
      if frame.header.number.sample_number = 0
      then
         Put("Creating ");
         Put_Line(Ada.Streams.Stream_Io.Name(wavFile));
         totalSize := Unsigned_32(current_metadata.data.stream_info.total_samples) *
           current_metadata.data.stream_info.channels *
             current_metadata.data.stream_info.bits_per_sample / 8 ;
         hdr.TotalSize := totalSize + 36 ;
         hdr.numChannels := Unsigned_16(frame.header.channels) ;
         hdr.samplesSizeSec := current_metadata.data.stream_info.sample_rate * frame.header.Bits_Per_Sample * frame.header.Channels / 8 ;
         hdr.sampleSize := Unsigned_16(frame.header.Bits_Per_Sample * frame.header.Channels / 8)  ;
         hdr.bitsPerSample := Unsigned_16(frame.header.Bits_Per_Sample) ;
         hdr.fileSize := totalSize ;
         WAVHeaderType'Write(wavStream,hdr);         
      end if ;
      for s in 1..frame.header.blocksize
      loop 
         for c in 1..frame.header.channels
         loop
            case frame.header.bits_per_sample is
            when 8 =>
               sample_u8 := ByteSamplesPkg.Get(buffer,Integer(c),Integer(s),Integer(frame.header.channels),Integer(frame.header.blocksize)) ;
               Short_Short_Integer'Write(wavStream,sample_u8);                      
            when 16 => 
               sample_u16 := WordSamplesPkg.Get(buffer,Integer(c),Integer(s),Integer(frame.header.channels),Integer(frame.header.blocksize)) ;
               Short_Integer'Write(wavStream,sample_u16);
            when others =>
               Put(Integer(frame.header.bits_per_sample)); Put(" is not a supported sample size"); New_Line ;
               return FLAC_u_STREAM_DECODER_WRITE_STATUS_ABORT ;
            end case ;
         end loop ;
      end loop ;
      New_Line ;
      return FLAC_u_STREAM_DECODER_WRITE_STATUS_CONTINUE ;
   end WriteCallback;


   procedure MetadataCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         metadata : access constant adaflac.format.FLAC_u_StreamMetadata;
         userdata : System.Address) is
      use Interfaces.C ;
   begin
      Put(Enclosing_Entity); Put(" > "); 
      if metadata.c_type = adaflac.format.FLAC_u_METADATA_TYPE_STREAMINFO
      then
     
         Put("sample rate : "); Put(Integer(metadata.data.stream_info.sample_rate)); Put(" Hz ");
         Put(" channels : "); Put(Integer(metadata.data.stream_info.channels));
         Put(" bits per sample : "); Put(Integer(metadata.data.stream_info.bits_per_sample));
         Put(" total samples : "); Put(Integer(metadata.data.stream_info.total_samples));
         New_Line;
         current_metadata := metadata.all ;
      end if ;

    end MetadataCallback;

   procedure ErrorCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         status : FLAC_u_StreamDecoderErrorStatus;
         userdata : System.Address) is
    begin
      Put(Enclosing_Entity); Put(" > ");
      Put( adaflac.stream_decoder.ErrorStatus(status) );
      New_Line;
    end ErrorCallback;

   

   procedure Open_Output( fn : String ) is
   begin
      Ada.Streams.Stream_Io.Create(wavFile,Ada.Streams.Stream_Io.Out_File,fn);
      wavStream := Ada.Streams.Stream_Io.Stream(wavFile);
   end Open_Output ;
   
   
   procedure Close is
   begin
      Ada.Streams.Stream_Io.Close( wavFile ) ;
   end Close ;
   

end callbacks ;
