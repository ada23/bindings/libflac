with System;
with Interfaces; use Interfaces;
with Interfaces.C ;
with adaflac.ordinals ;
with adaflac.format;
with adaflac.stream_decoder ; use adaflac.stream_decoder;

package callbacks is
   verbose : Boolean := true ;
   -- Ref: https://docs.fileformat.com/audio/wav/
   type WAVHeaderType is
      record
         riff : String(1..4) := "RIFF" ;
         totalSize : Unsigned_32 := 0;
         fileType : String(1..4) := "WAVE" ;
         fmtChunk : String(1..4) := "fmt " ;
         lengthFormatData : Unsigned_32 := 16 ;
         formatType : Unsigned_16 := 1 ; 
         numChannels : Unsigned_16 := 0 ;
         sampleRate : Unsigned_32 := 44100 ;
         samplesSizeSec : Unsigned_32 := 0 ;
         sampleSize : Unsigned_16 := 0 ;
         bitsperSample : Unsigned_16 := 0 ;
         dataChunk : String(1..4) := "data" ;
         fileSize : Unsigned_32 := 0 ;
      end record
     with Convention => C;
   
   function ReadCallback
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : access Interfaces.C.unsigned_char;
         userdata : access Interfaces.C.unsigned_long;
         arg4 : System.Address) return FLAC_u_StreamDecoderReadStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:524

   function SeekCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : adaflac.ordinals.FLAC_u_uint64;
         userdata : System.Address) return FLAC_u_StreamDecoderSeekStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:559

   function TellCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : access Interfaces.C.unsigned_long_long;
         userdata : System.Address) return FLAC_u_StreamDecoderTellStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:597

   function LengthCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         arg2 : access Interfaces.C.unsigned_long_long;
         userdata : System.Address) return FLAC_u_StreamDecoderLengthStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:635

   function EofCallback (decoder : access constant FLAC_u_StreamDecoder; arg2 : System.Address) return adaflac.ordinals.FLAC_u_bool
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:662

   function WriteCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         frame : access constant adaflac.format.FLAC_u_Frame;
         buffer : System.Address;
         userdata : System.Address) return FLAC_u_StreamDecoderWriteStatus
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:690

   procedure MetadataCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         metadata : access constant adaflac.format.FLAC_u_StreamMetadata;
         userdata : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:717

   procedure ErrorCallback 
        (decoder : access constant FLAC_u_StreamDecoder;
         status : FLAC_u_StreamDecoderErrorStatus;
         userdata : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_decoder.h:734


   procedure Open_Output( fn : String ) ;
   procedure Close ;
   
end callbacks;
