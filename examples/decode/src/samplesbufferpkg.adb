with Interfaces ;
package body SamplesBufferPkg is

   type SamplesType is array (integer range <>) of Integer ;

   function Get(from : System.Address ;
                Channel : Integer ;
                SampleNo : Integer ;
                NumChannels : Integer := Def_Channels ;
                MaxSamples : Integer := Def_MaxSamples ) return DataType is
      PtrTable : array (1..NumChannels) of System.Address ;
      for PtrTable'Address use from ;
      Samples : SamplesType(1..MaxSamples) ;
      for Samples'Address use PtrTable(Channel);
      d : DataType ;
   begin

      d := DataType(Samples(SampleNo)) ;

      return d ;
   end Get ;

end SamplesBufferPkg ;
