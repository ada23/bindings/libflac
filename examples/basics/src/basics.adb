with Ada.Text_Io; use Ada.Text_Io;

with adaflac.format ;

procedure Basics is
begin
   Put("VERSION "); Put( adaflac.format.VERSION_STRING) ; New_Line ;
   Put("VENDOR "); Put( adaflac.format.VENDOR_STRING ) ; New_Line ;
end Basics;
