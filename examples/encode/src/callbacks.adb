with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Interfaces.C.Extensions;
with adaflac.ordinals ; use adaflac.ordinals ;

package body callbacks is
   package ICX renames Interfaces.C.Extensions;
   function ReadCallback
     (encoder : access constant FLAC_u_StreamEncoder;
      buffer : System.Address;
      bytes    : access Interfaces.C.size_t ;
      client_data : System.Address)
      return FLAC_u_StreamEncoderReadStatus
   is
   begin
      return FLAC_u_StreamEncoderReadStatus'First;
   end ReadCallback;

   function WriteCallback
     (encoder : access constant FLAC_u_StreamEncoder; buffer : System.Address;
      bytes    : Interfaces.C.size_t;
      samples : Unsigned_32;
      current_frame : Unsigned_32;
      client_data    : System.Address) return FLAC_u_StreamEncoderWriteStatus
   is
   begin
      return FLAC_u_StreamEncoderWriteStatus'First;
   end WriteCallback;

   function SeekCallback
     (encoder : access constant FLAC_u_StreamEncoder;
      absolute_byte_offset    : adaflac.ordinals.FLAC_u_uint64;
      client_data : System.Address)
      return FLAC_u_StreamEncoderSeekStatus
   is
   begin
      return FLAC_u_StreamEncoderSeekStatus'First;
   end SeekCallback;

   function TellCallback
     (encoder : access constant FLAC_u_StreamEncoder;
      absolute_byte_offset : access Interfaces.C.Extensions.unsigned_long_long;
      client_data : System.Address)
      return FLAC_u_StreamEncoderTellStatus
   is
   begin
      return FLAC_u_StreamEncoderTellStatus'First;
   end TellCallback;

   procedure MetadataCallback
          (encoder  : access constant FLAC_u_StreamEncoder;
           metadata : access constant adaflac.format.FLAC_u_StreamMetadata;
           client_data     : System.Address)
   is
   begin
      null;
   end MetadataCallback;

   procedure ProgressCallback
     (encoder : access constant FLAC_u_StreamEncoder;
         bytes_written : adaflac.ordinals.FLAC_u_uint64;
         samples_written : adaflac.ordinals.FLAC_u_uint64;
         frames_written :  Interfaces.C.Extensions.Unsigned_32;
         total_frames_estimate :  Interfaces.C.Extensions.Unsigned_32;
         client_data : System.Address)
   is
      use adaflac.ordinals.uint64_text_io ;
      use adaflac.Extunsigned_32_text_Io ;
   begin
      if Verbose
      then
         Put("Bytes written "); Put(bytes_written) ; Put(" ");
         Put("Samples written "); Put(samples_written); Put(" ");
         Put("Frames written "); Put(frames_written); Put(" ");
         Put("total_frames_estimate "); Put(total_frames_estimate) ; Put(" ");
         New_Line;
      end if ;
   end ProgressCallback;

   SAMPLES_IN_BLOCK : constant := 1024 ;
   procedure Copy( wavFile : Ada.Streams.Stream_Io.File_Type ;
                   wavStream : Ada.Streams.Stream_Io.Stream_Access ;
                   hdr : WAVHeaderType ;
                   encoder : access FLAC_u_StreamEncoder) is
      pcm : array(1..Integer(SAMPLES_IN_BLOCK*hdr.numChannels)) of Integer ;
      pcmsize : Integer := 0;
      bstatus : FLAC_u_bool;
      blocknum : Integer := 0 ;
      use Ada.Streams ;
   begin
      if verbose
      then
         Put("Num Channels "); Put(Integer(hdr.numChannels)); Put(" ");
         Put(" Bits per sample "); Put(Integer(hdr.bitsPerSample));  Put(" ");
         Put(" Sample Rate "); Put(Integer(hdr.SampleRate)); Put(" ");
         New_Line ;
      end if ;

      loop
         case hdr.bitsPerSample is
           when 8 =>
               declare
                  buffer : Stream_Element_Array(1..Stream_Element_Offset(SAMPLES_IN_BLOCK*hdr.numChannels)) ;
                  bufwords : Array (1..Integer(SAMPLES_IN_BLOCK*hdr.numChannels)) of Short_Short_Integer ;
                  for bufwords'Address use buffer'Address ;
                  bufsize : Stream_ELement_Offset ;

               begin
                  if Stream_Io.End_Of_File(wavFile)
                  then
                     exit ;
                  end if ;

                  Ada.Streams.Stream_Io.Read(wavFile,buffer,bufsize);
                  blocknum := blocknum + 1 ;
                  if Verbose
                  then
                     Put("Block "); Put(blocknum); Put(" ");
                     Put("Size "); Put(Integer(bufsize)); Put(" Bytes"); New_Line;
                  end if ;

                  pcm := (others => 0);

                  for s in 1..bufsize
                  loop
                     pcm(Integer(s)) := Integer(bufwords(Integer(s))/2);
                  end loop ;
                  bstatus := adaflac.stream_encoder.process_interleaved(encoder,
                                                            pcm(pcm'First)'Address,
                                                            ICX.Unsigned_32(Integer(bufsize)/Integer(hdr.numChannels)));
               exception
                  when Ada.Streams.Stream_Io.End_Error => exit ;
               end ;

            when 16 =>
               declare
                  buffer : Stream_Element_Array(1..Stream_Element_Offset(SAMPLES_IN_BLOCK*hdr.numChannels*2)) ;
                  bufwords : Array (1..Integer(SAMPLES_IN_BLOCK*hdr.numChannels)) of Short_Integer ;
                  for bufwords'Address use buffer'Address ;
                  bufsize : Stream_ELement_Offset ;

               begin
                  if Stream_Io.End_Of_File(wavFile)
                  then
                     exit ;
                  end if ;

                  Ada.Streams.Stream_Io.Read(wavFile,buffer,bufsize);
                  Put("Block "); Put(Integer(bufsize)); Put(" Words"); New_Line;
                  pcm := (others => 0);

                  for s in 1..bufsize/2
                  loop
                     pcm(Integer(s)) := Integer(bufwords(Integer(s)));
                  end loop ;
                  bstatus := adaflac.stream_encoder.process_interleaved(encoder,
                                                            pcm(pcm'First)'Address,
                                                            ICX.Unsigned_32(Integer(bufsize)/Integer(hdr.numChannels*2)));
               exception
                  when Ada.Streams.Stream_Io.End_Error => exit ;
               end ;

            when others => Put("Unsupport bits per sample "); Put(Integer(hdr.bitsPerSample)) ; New_Line ; return ;
         end case ;
      end loop ;
   end Copy ;

end callbacks;
