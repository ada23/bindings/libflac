
with System ;
with Ada.Text_IO;      use Ada.Text_IO;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Streams.Stream_Io;
with Interfaces.C;
with Interfaces.C.Strings;
WITH Interfaces.C.Extensions;

with GNAT.Source_Info; use GNAT.Source_Info;

with Adaflac.stream_encoder; use Adaflac.stream_encoder;
with Adaflac.ordinals;       use Adaflac.ordinals;
with adaflac.format;
with adaflac.metadata;

with callbacks;

procedure Encode is
   use Interfaces.C;
   package ICX renames Interfaces.C.Extensions;

   verbose : constant Boolean := True;

   name : constant String := GNAT.Source_Info.Enclosing_Entity;

   procedure T1 is
      myname : constant String := GNAT.Source_Info.Enclosing_Entity;
   begin
      if verbose then
         Put_Line (myname);
      end if;
      Put_Line ("FLAC_u_StreamEncoderState");
      for st in adaflac.stream_encoder.FLAC_u_StreamEncoderState'Range loop
         Put_Line (adaflac.stream_encoder.State (st));
      end loop;
      New_Line;
      Put_Line ("FLAC_u_StreamEncoderInitStatus");
      for st in adaflac.stream_encoder.FLAC_u_StreamEncoderInitStatus'Range
      loop
         Put_Line (InitStatus (st));
      end loop;
      New_Line;
      Put_Line ("FLAC_u_StreamEncoderReadStatus");
      for st in FLAC_u_StreamEncoderReadStatus'Range loop
         Put_Line (ReadStatus (st));
      end loop;
      New_Line;

      Put_Line ("FLAC_u_StreamEncoderWriteStatus");
      for st in FLAC_u_StreamEncoderWriteStatus'Range loop
         Put_Line (WriteStatus (st));
      end loop;
      New_Line;

      Put_Line ("FLAC_u_StreamEncoderSeekStatus");
      for st in FLAC_u_StreamEncoderSeekStatus'Range loop
         Put_Line (SeekStatus (st));
      end loop;
      New_Line;

      Put_Line ("FLAC_u_StreamEncoderTellStatus");
      for st in FLAC_u_StreamEncoderTellStatus'Range loop
         Put_Line (TellStatus (st));
      end loop;
      New_Line;

   end T1;

   procedure T2 is
      myname  : constant String := GNAT.Source_Info.Enclosing_Entity;
      wavFilenm : constant String := Argument(1) ;
      flacFile : constant String := Argument(2) ;
      bstatus : FLAC_u_bool;
      encoder : constant access FLAC_u_StreamEncoder := Cnew;
      estatus : FLAC_u_StreamEncoderInitStatus ;
      use Interfaces.C.Strings ;
      metadata : aliased array(1..2) of access ADAFLAC.FORMAT.FLAC_u_StreamMetadata;
      ventry : aliased adaflac.format.FLAC_u_StreamMetadata_VorbisComment_Entry ;

      wavFile : Ada.Streams.Stream_Io.File_Type;
      wavStream : Ada.Streams.Stream_Io.Stream_Access;
      hdr : callbacks.WAVHeaderType ;
      procedure CheckStatus is
      begin
         if bstatus /= adaflac.ordinals.FLAC_True then raise Program_Error ; end if ;
      end CheckStatus ;

      use Interfaces;
   begin
      if verbose then
         Put_Line (myname);
      end if;
      Ada.Streams.Stream_Io.Open(wavFile,Ada.Streams.Stream_Io.In_File,wavFilenm);
      wavStream := Ada.Streams.Stream_Io.Stream(wavFile);
      callbacks.WAVHeaderType'Read( wavStream, hdr );
      if hdr.riff /= "RIFF" or hdr.fileType /= "WAVE" or hdr.fmtChunk /= "fmt " or hdr.dataChunk /= "data"
      then
         Put_Line("WAV file header ill formed");
         Ada.Streams.Stream_Io.Close(wavFile);
         return ;
      end if ;

      --if hdr.bitsPerSample /= 16
      --then
      --   Put_Line("Only 16 bit sample WAV files is supported");
      --end if ;


      bstatus := set_verify (encoder, FLAC_True); CheckStatus ;
      bstatus := set_compression_level (encoder, 5); CheckStatus ;
      bstatus := set_channels(encoder,ICX.Unsigned_32(hdr.numChannels)); CheckStatus ;
      bstatus := set_bits_per_sample(encoder,ICX.Unsigned_32(hdr.bitsPerSample)); CheckStatus ;
      bstatus := set_sample_rate(encoder,ICX.Unsigned_32(hdr.SampleRate)); CheckStatus ;
      bstatus := set_total_samples_estimate(encoder,Unsigned_64(hdr.totalSize/Unsigned_32((hdr.numChannels*hdr.bitsperSample/8)))); CheckStatus ;
      metadata(1) := adaflac.metadata.object_new(adaflac.format.FLAC_u_METADATA_TYPE_VORBIS_COMMENT);
      bstatus := adaflac.metadata.object_vorbiscomment_entry_from_name_value_pair( ventry'access , New_String("ARTIST") , New_String("Srini") ) ; CheckStatus ;
      bstatus := adaflac.metadata.object_vorbiscomment_append_comment(metadata(1),ventry,adaflac.ordinals.FLAC_false); CheckStatus ;

      bstatus := adaflac.metadata.object_vorbiscomment_entry_from_name_value_pair( ventry'access , New_String("YEAR") , New_String("2024") ) ; CheckStatus ;
      bstatus := adaflac.metadata.object_vorbiscomment_append_comment(metadata(1),ventry,adaflac.ordinals.FLAC_false); CheckStatus ;


      metadata(2) := adaflac.metadata.object_new(adaflac.format.FLAC_u_METADATA_TYPE_PADDING);
      metadata(2).length := 1024;

      bstatus := set_metadata(encoder,metadata(1)'Address,metadata'length); CheckStatus ;
      estatus := adaflac.stream_encoder.init_file(encoder, New_String(flacFile), callbacks.ProgressCallback'access, System.Null_Address);
      if estatus /= adaflac.stream_encoder.FLAC_u_STREAM_ENCODER_INIT_STATUS_OK
      then
         Put("Error initializing ");
         Put(adaflac.stream_encoder.InitStatus(estatus));
         New_Line;
         return ;
      end if ;
      callbacks.Copy( wavFile , wavStream , hdr , encoder ) ;
      bstatus := finish(encoder);
      Put("Encoder state "); Put(State(get_state(encoder))); New_Line ;
      Ada.Streams.Stream_Io.Close(wavFile);
      delete (encoder);
   end T2;

begin
   if verbose then
      Put (name);
      Put (" ");
      Put (Compilation_ISO_Date);
      Put (" ");
      Put (Compilation_Time);
      New_Line;
   end if;
   T1;
   T2;
end Encode;
