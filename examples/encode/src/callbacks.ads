with System;
with Interfaces; use Interfaces;
with Interfaces.C;
with Interfaces.C.Extensions;
with Ada.Streams.Stream_Io;

with adaflac.ordinals;
with adaflac.format;
with adaflac.stream_encoder; use adaflac.stream_encoder;

package callbacks is
   verbose : Boolean := True;
   -- Ref: https://docs.fileformat.com/audio/wav/
   type WAVHeaderType is record
      riff             : String (1 .. 4) := "RIFF";
      totalSize        : Unsigned_32     := 0;
      fileType         : String (1 .. 4) := "WAVE";
      fmtChunk         : String (1 .. 4) := "fmt ";
      lengthFormatData : Unsigned_32     := 16;
      formatType       : Unsigned_16     := 1;
      numChannels      : Unsigned_16     := 0;
      sampleRate       : Unsigned_32     := 44_100;
      samplesSizeSec   : Unsigned_32     := 0;
      sampleSize       : Unsigned_16     := 0;
      bitsperSample    : Unsigned_16     := 0;
      dataChunk        : String (1 .. 4) := "data";
      fileSize         : Unsigned_32     := 0;
   end record with
      Convention => C;

   function ReadCallback
     (encoder : access constant FLAC_u_StreamEncoder; buffer : System.Address;
      bytes    : access Interfaces.C.size_t ;
      client_data : System.Address)
      return FLAC_u_StreamEncoderReadStatus with
      Convention => C;  -- ../include/FLAC/stream_encoder.h:523

   function WriteCallback
     (encoder : access constant FLAC_u_StreamEncoder; buffer : System.Address;
      bytes    : Interfaces.C.size_t;
      samples : Unsigned_32;
      current_frame : Unsigned_32;
      client_data    : System.Address)
      return FLAC_u_StreamEncoderWriteStatus with
      Convention => C;  -- ../include/FLAC/stream_encoder.h:561

   function SeekCallback
     (encoder : access constant FLAC_u_StreamEncoder;
      absolute_byte_offset    : adaflac.ordinals.FLAC_u_uint64;
      client_data : System.Address)
      return FLAC_u_StreamEncoderSeekStatus with
      Convention => C;  -- ../include/FLAC/stream_encoder.h:595

   function TellCallback
     (encoder : access constant FLAC_u_StreamEncoder;
         absolute_byte_offset : access Interfaces.C.Extensions.unsigned_long_long;
         client_data : System.Address)
      return FLAC_u_StreamEncoderTellStatus with
      Convention => C;  -- ../include/FLAC/stream_encoder.h:640

   procedure MetadataCallback
     (encoder  : access constant FLAC_u_StreamEncoder;
      metadata : access constant adaflac.format.FLAC_u_StreamMetadata;
      client_data     : System.Address) with
      Convention => C;  -- ../include/FLAC/stream_encoder.h:659


   procedure ProgressCallback
      (encoder : access constant FLAC_u_StreamEncoder;
         bytes_written : adaflac.ordinals.FLAC_u_uint64;
         samples_written : adaflac.ordinals.FLAC_u_uint64;
         frames_written : Interfaces.C.Extensions.Unsigned_32;
         total_frames_estimate :  Interfaces.C.Extensions.Unsigned_32;
         client_data : System.Address) with
     Convention => C;  -- ../include/FLAC/stream_encoder.h:682

   procedure Copy( wavFile : Ada.Streams.Stream_Io.File_Type ;
                   wavStream : Ada.Streams.Stream_Io.Stream_Access ;
                   hdr : WAVHeaderType ;
                   encoder : access FLAC_u_StreamEncoder) ;

end callbacks;
