pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_iosfwd is

   type ios_base is null record;   -- incomplete struct

   package basic_ios_wchar_t_Class_char_traits.char_traits is
      type basic_ios is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ios_wchar_t_Class_char_traits.char_traits;

   package basic_ios_char_Class_char_traits.char_traits is
      type basic_ios is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ios_char_Class_char_traits.char_traits;



   package basic_streambuf_wchar_t_Class_char_traits.char_traits is
      type basic_streambuf is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_streambuf_wchar_t_Class_char_traits.char_traits;

   package basic_streambuf_char_Class_char_traits.char_traits is
      type basic_streambuf is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_streambuf_char_Class_char_traits.char_traits;



   package basic_istream_wchar_t_Class_char_traits.char_traits is
      type basic_istream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_istream_wchar_t_Class_char_traits.char_traits;

   package basic_istream_char_Class_char_traits.char_traits is
      type basic_istream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_istream_char_Class_char_traits.char_traits;



   package basic_ostream_wchar_t_Class_char_traits.char_traits is
      type basic_ostream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ostream_wchar_t_Class_char_traits.char_traits;

   package basic_ostream_char_Class_char_traits.char_traits is
      type basic_ostream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ostream_char_Class_char_traits.char_traits;



   package basic_iostream_wchar_t_Class_char_traits.char_traits is
      type basic_iostream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_iostream_wchar_t_Class_char_traits.char_traits;

   package basic_iostream_char_Class_char_traits.char_traits is
      type basic_iostream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_iostream_char_Class_char_traits.char_traits;



   package basic_stringbuf_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_stringbuf is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_stringbuf_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator;

   package basic_stringbuf_char_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_stringbuf is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_stringbuf_char_Class_char_traits.char_traits_Class_allocator.allocator;



   package basic_istringstream_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_istringstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_istringstream_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator;

   package basic_istringstream_char_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_istringstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_istringstream_char_Class_char_traits.char_traits_Class_allocator.allocator;



   package basic_ostringstream_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_ostringstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ostringstream_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator;

   package basic_ostringstream_char_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_ostringstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ostringstream_char_Class_char_traits.char_traits_Class_allocator.allocator;



   package basic_stringstream_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_stringstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_stringstream_wchar_t_Class_char_traits.char_traits_Class_allocator.allocator;

   package basic_stringstream_char_Class_char_traits.char_traits_Class_allocator.allocator is
      type basic_stringstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_stringstream_char_Class_char_traits.char_traits_Class_allocator.allocator;



   package basic_filebuf_wchar_t_Class_char_traits.char_traits is
      type basic_filebuf is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_filebuf_wchar_t_Class_char_traits.char_traits;

   package basic_filebuf_char_Class_char_traits.char_traits is
      type basic_filebuf is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_filebuf_char_Class_char_traits.char_traits;



   package basic_ifstream_wchar_t_Class_char_traits.char_traits is
      type basic_ifstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ifstream_wchar_t_Class_char_traits.char_traits;

   package basic_ifstream_char_Class_char_traits.char_traits is
      type basic_ifstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ifstream_char_Class_char_traits.char_traits;



   package basic_ofstream_wchar_t_Class_char_traits.char_traits is
      type basic_ofstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ofstream_wchar_t_Class_char_traits.char_traits;

   package basic_ofstream_char_Class_char_traits.char_traits is
      type basic_ofstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_ofstream_char_Class_char_traits.char_traits;



   package basic_fstream_wchar_t_Class_char_traits.char_traits is
      type basic_fstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_fstream_wchar_t_Class_char_traits.char_traits;

   package basic_fstream_char_Class_char_traits.char_traits is
      type basic_fstream is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_fstream_char_Class_char_traits.char_traits;



   subtype ios is basic_ios;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:132

   subtype streambuf is basic_streambuf;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:135

   subtype istream is basic_istream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:138

   subtype ostream is basic_ostream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:141

   subtype iostream is basic_iostream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:144

   subtype stringbuf is basic_stringbuf;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:147

   subtype istringstream is basic_istringstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:150

   subtype ostringstream is basic_ostringstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:153

   subtype stringstream is basic_stringstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:156

   subtype filebuf is basic_filebuf;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:159

   subtype ifstream is basic_ifstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:162

   subtype ofstream is basic_ofstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:165

   subtype fstream is basic_fstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:168

   subtype wios is basic_ios;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:172

   subtype wstreambuf is basic_streambuf;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:175

   subtype wistream is basic_istream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:178

   subtype wostream is basic_ostream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:181

   subtype wiostream is basic_iostream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:184

   subtype wstringbuf is basic_stringbuf;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:187

   subtype wistringstream is basic_istringstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:190

   subtype wostringstream is basic_ostringstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:193

   subtype wstringstream is basic_stringstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:196

   subtype wfilebuf is basic_filebuf;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:199

   subtype wifstream is basic_ifstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:202

   subtype wofstream is basic_ofstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:205

   subtype wfstream is basic_fstream;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/iosfwd:208

end cpp_12_1_0_iosfwd;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
