pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;
with System;

package malloc_umalloc_h is

   function malloc (uu_size : sys_utypes_usize_t_h.size_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "malloc";

   function calloc (uu_count : sys_utypes_usize_t_h.size_t; uu_size : sys_utypes_usize_t_h.size_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:41
   with Import => True, 
        Convention => C, 
        External_Name => "calloc";

   procedure free (arg1 : System.Address)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "free";

   function realloc (uu_ptr : System.Address; uu_size : sys_utypes_usize_t_h.size_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:43
   with Import => True, 
        Convention => C, 
        External_Name => "realloc";

   function valloc (arg1 : sys_utypes_usize_t_h.size_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:45
   with Import => True, 
        Convention => C, 
        External_Name => "valloc";

   function aligned_alloc (uu_alignment : sys_utypes_usize_t_h.size_t; uu_size : sys_utypes_usize_t_h.size_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:50
   with Import => True, 
        Convention => C, 
        External_Name => "aligned_alloc";

   function posix_memalign
     (uu_memptr : System.Address;
      uu_alignment : sys_utypes_usize_t_h.size_t;
      uu_size : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "posix_memalign";

end malloc_umalloc_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
