pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_ptr_traits_h is

   type uu_undefined is null record;   -- incomplete struct

end cpp_12_1_0_bits_ptr_traits_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
