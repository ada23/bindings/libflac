pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package uctype_h is

   --  skipped func ___runetype

   --  skipped func ___tolower

   --  skipped func ___toupper

   function isascii (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:135
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isasciii";

   --  skipped func __maskrune

   --  skipped func __istype

   --  skipped func __isctype

   --  skipped func __toupper

   --  skipped func __tolower

   --  skipped func __wcwidth

   function isalnum (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:212
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isalnumi";

   function isalpha (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:218
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isalphai";

   function isblank (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:224
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isblanki";

   function iscntrl (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:230
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7iscntrli";

   function isdigit (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:237
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isdigiti";

   function isgraph (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:243
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isgraphi";

   function islower (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:249
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isloweri";

   function isprint (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:255
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isprinti";

   function ispunct (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:261
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7ispuncti";

   function isspace (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:267
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isspacei";

   function isupper (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:273
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7isupperi";

   function isxdigit (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:280
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8isxdigiti";

   function toascii (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:286
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7toasciii";

   function tolower (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:292
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7toloweri";

   function toupper (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:298
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z7toupperi";

   function digittoint (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:305
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z10digittointi";

   function ishexnumber (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:311
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z11ishexnumberi";

   function isideogram (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:317
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z10isideogrami";

   function isnumber (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:323
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8isnumberi";

   function isphonogram (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:329
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z11isphonogrami";

   function isrune (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:335
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z6isrunei";

   function isspecial (u_c : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_ctype.h:341
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z9isspeciali";

end uctype_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
