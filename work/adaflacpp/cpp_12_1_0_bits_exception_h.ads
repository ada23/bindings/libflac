pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package cpp_12_1_0_bits_exception_h is

   package Class_c_exception is
      type c_exception is tagged limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_c_exception return c_exception;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/exception.h:64
      pragma CPP_Constructor (New_c_exception, "_ZNSt9exceptionC1Ev");

      procedure Delete_c_exception (this : access c_exception)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/exception.h:65
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9exceptionD1Ev";

      procedure Delete_And_Free_c_exception (this : access c_exception)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/exception.h:65
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9exceptionD0Ev";

      function Assign_c_exception (this : access c_exception'Class; arg2 : access constant c_exception'Class) return access c_exception  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/exception.h:68
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9exceptionaSERKS_";

      function Assign_c_exception (this : access c_exception'Class; arg2 : access c_exception'Class) return access c_exception  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/exception.h:70
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9exceptionaSEOS_";

      function what (this : access constant c_exception) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/exception.h:76
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt9exception4whatEv";
   end;
   use Class_c_exception;
end cpp_12_1_0_bits_exception_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
