pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package sys_utypes_uint16_t_h is

   subtype int16_t is short;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_int16_t.h:30

end sys_utypes_uint16_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
