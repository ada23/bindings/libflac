pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;

package cpp_12_1_0_cwchar is

   function wcschr (uu_p : access wchar_t; uu_c : wchar_t) return access wchar_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/cwchar:214
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6wcschrPww";

   function wcspbrk (uu_s1 : access wchar_t; uu_s2 : access wchar_t) return access wchar_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/cwchar:218
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7wcspbrkPwPKw";

   function wcsrchr (uu_p : access wchar_t; uu_c : wchar_t) return access wchar_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/cwchar:222
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7wcsrchrPww";

   function wcsstr (uu_s1 : access wchar_t; uu_s2 : access wchar_t) return access wchar_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/cwchar:226
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6wcsstrPwPKw";

   function wmemchr
     (uu_p : access wchar_t;
      uu_c : wchar_t;
      uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return access wchar_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/cwchar:230
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7wmemchrPwwm";

end cpp_12_1_0_cwchar;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
