pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
with sys_utypes_usize_t_h;
with FLAC_ordinals_h;

package FLAC_callback_h is

   type FLAC_u_IOHandle is new System.Address;  -- ../include/FLAC/callback.h:89

   type FLAC_u_IOCallback_Read is access function
        (arg1 : System.Address;
         arg2 : sys_utypes_usize_t_h.size_t;
         arg3 : sys_utypes_usize_t_h.size_t;
         arg4 : FLAC_u_IOHandle) return sys_utypes_usize_t_h.size_t
   with Convention => C;  -- ../include/FLAC/callback.h:104

   type FLAC_u_IOCallback_Write is access function
        (arg1 : System.Address;
         arg2 : sys_utypes_usize_t_h.size_t;
         arg3 : sys_utypes_usize_t_h.size_t;
         arg4 : FLAC_u_IOHandle) return sys_utypes_usize_t_h.size_t
   with Convention => C;  -- ../include/FLAC/callback.h:117

   type FLAC_u_IOCallback_Seek is access function
        (arg1 : FLAC_u_IOHandle;
         arg2 : FLAC_ordinals_h.FLAC_u_int64;
         arg3 : int) return int
   with Convention => C;  -- ../include/FLAC/callback.h:130

   type FLAC_u_IOCallback_Tell is access function (arg1 : FLAC_u_IOHandle) return FLAC_ordinals_h.FLAC_u_int64
   with Convention => C;  -- ../include/FLAC/callback.h:141

   type FLAC_u_IOCallback_Eof is access function (arg1 : FLAC_u_IOHandle) return int
   with Convention => C;  -- ../include/FLAC/callback.h:152

   type FLAC_u_IOCallback_Close is access function (arg1 : FLAC_u_IOHandle) return int
   with Convention => C;  -- ../include/FLAC/callback.h:162

   type FLAC_u_IOCallbacks is record
      read : FLAC_u_IOCallback_Read;  -- ../include/FLAC/callback.h:176
      write : FLAC_u_IOCallback_Write;  -- ../include/FLAC/callback.h:177
      seek : FLAC_u_IOCallback_Seek;  -- ../include/FLAC/callback.h:178
      tell : FLAC_u_IOCallback_Tell;  -- ../include/FLAC/callback.h:179
      eof : FLAC_u_IOCallback_Eof;  -- ../include/FLAC/callback.h:180
      close : FLAC_u_IOCallback_Close;  -- ../include/FLAC/callback.h:181
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/callback.h:182

end FLAC_callback_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
