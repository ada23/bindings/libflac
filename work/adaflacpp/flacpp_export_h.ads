pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package FLACpp_export_h is

   FLACPP_API_VERSION_CURRENT : constant := 11;  --  ../include/FLAC++/export.h:94
   FLACPP_API_VERSION_REVISION : constant := 0;  --  ../include/FLAC++/export.h:95
   FLACPP_API_VERSION_AGE : constant := 0;  --  ../include/FLAC++/export.h:96

end FLACpp_export_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
