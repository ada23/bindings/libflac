pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;

package sys_utypes_ufd_def_h is

   type anon_array1739 is array (0 .. 31) of aliased arm_utypes_h.uu_int32_t;
   type fd_set is record
      fds_bits : aliased anon_array1739;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_fd_def.h:51
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_fd_def.h:50

   --  skipped func __darwin_check_fd_set_overflow

   --  skipped func __darwin_check_fd_set

   --  skipped func __darwin_fd_isset

   --  skipped func __darwin_fd_set

   --  skipped func __darwin_fd_clr

end sys_utypes_ufd_def_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
