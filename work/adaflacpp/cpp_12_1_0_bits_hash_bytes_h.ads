pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_hash_bytes_h is

   --  skipped func _Hash_bytes

   --  skipped func _Fnv_hash_bytes

end cpp_12_1_0_bits_hash_bytes_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
