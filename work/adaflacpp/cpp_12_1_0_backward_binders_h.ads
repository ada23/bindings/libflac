pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_backward_binders_h is

end cpp_12_1_0_backward_binders_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
