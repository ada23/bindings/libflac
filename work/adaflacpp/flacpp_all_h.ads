pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package FLACpp_all_h is

end FLACpp_all_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
