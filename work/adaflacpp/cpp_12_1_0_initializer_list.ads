pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_initializer_list is

   package initializer_list_char32_t is
      type initializer_list is limited record
         u_M_array : iterator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:58
         u_M_len : aliased size_type;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:59
      end record
      with Import => True,
           Convention => CPP;

      function New_initializer_list (uu_a : const_iterator; uu_l : size_type) return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:62
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIDiEC1EPKDim");

      function New_initializer_list return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:66
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIDiEC1Ev");

      function size (this : access constant initializer_list) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:71
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIDiE4sizeEv";

      function c_begin (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:75
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIDiE5beginEv";

      function c_end (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIDiE3endEv";

   end;
   use initializer_list_char32_t;

   package initializer_list_char16_t is
      type initializer_list is limited record
         u_M_array : iterator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:58
         u_M_len : aliased size_type;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:59
      end record
      with Import => True,
           Convention => CPP;

      function New_initializer_list (uu_a : const_iterator; uu_l : size_type) return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:62
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIDsEC1EPKDsm");

      function New_initializer_list return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:66
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIDsEC1Ev");

      function size (this : access constant initializer_list) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:71
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIDsE4sizeEv";

      function c_begin (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:75
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIDsE5beginEv";

      function c_end (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIDsE3endEv";

   end;
   use initializer_list_char16_t;

   package initializer_list_wchar_t is
      type initializer_list is limited record
         u_M_array : iterator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:58
         u_M_len : aliased size_type;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:59
      end record
      with Import => True,
           Convention => CPP;

      function New_initializer_list (uu_a : const_iterator; uu_l : size_type) return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:62
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIwEC1EPKwm");

      function New_initializer_list return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:66
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIwEC1Ev");

      function size (this : access constant initializer_list) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:71
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIwE4sizeEv";

      function c_begin (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:75
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIwE5beginEv";

      function c_end (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIwE3endEv";

   end;
   use initializer_list_wchar_t;

   package initializer_list_char is
      type initializer_list is limited record
         u_M_array : iterator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:58
         u_M_len : aliased size_type;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:59
      end record
      with Import => True,
           Convention => CPP;

      function New_initializer_list (uu_a : const_iterator; uu_l : size_type) return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:62
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIcEC1EPKcm");

      function New_initializer_list return initializer_list;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:66
      pragma CPP_Constructor (New_initializer_list, "_ZNSt16initializer_listIcEC1Ev");

      function size (this : access constant initializer_list) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:71
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIcE4sizeEv";

      function c_begin (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:75
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIcE5beginEv";

      function c_end (this : access constant initializer_list) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/initializer_list:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt16initializer_listIcE3endEv";

   end;
   use initializer_list_char;



end cpp_12_1_0_initializer_list;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
