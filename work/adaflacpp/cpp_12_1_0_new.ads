pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_bits_exception_h;
with Interfaces.C.Strings;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;
with System;

package cpp_12_1_0_new is

   package Class_bad_alloc is
      type bad_alloc is limited new cpp_12_1_0_bits_exception_h.Class_c_exception.c_exception with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_bad_alloc return bad_alloc;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:58
      pragma CPP_Constructor (New_bad_alloc, "_ZNSt9bad_allocC1Ev");

      function Assign_bad_alloc (this : access bad_alloc'Class; arg2 : access constant bad_alloc'Class) return access bad_alloc  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:62
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9bad_allocaSERKS_";

      procedure Delete_bad_alloc (this : access bad_alloc)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:67
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9bad_allocD1Ev";

      procedure Delete_And_Free_bad_alloc (this : access bad_alloc)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:67
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt9bad_allocD0Ev";

      function what (this : access constant bad_alloc) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:70
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt9bad_alloc4whatEv";
   end;
   use Class_bad_alloc;
   package Class_bad_array_new_length is
      type bad_array_new_length is limited new bad_alloc with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_bad_array_new_length return bad_array_new_length;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:77
      pragma CPP_Constructor (New_bad_array_new_length, "_ZNSt20bad_array_new_lengthC1Ev");

      procedure Delete_bad_array_new_length (this : access bad_array_new_length)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:81
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt20bad_array_new_lengthD1Ev";

      procedure Delete_And_Free_bad_array_new_length (this : access bad_array_new_length)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:81
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt20bad_array_new_lengthD0Ev";

      function what (this : access constant bad_array_new_length) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:84
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt20bad_array_new_length4whatEv";
   end;
   use Class_bad_array_new_length;
   type align_val_t is 
     ()
   with Convention => C;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:89

   package Class_nothrow_t is
      type nothrow_t is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_nothrow_t return nothrow_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:95
      pragma CPP_Constructor (New_nothrow_t, "_ZNSt9nothrow_tC1Ev");
   end;
   use Class_nothrow_t;
   nothrow : aliased constant nothrow_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:99
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7nothrow";

   type new_handler is access procedure
   with Convention => C;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:103

   function set_new_handler (arg1 : new_handler) return new_handler  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:107
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt15set_new_handlerPFvvE";

   function get_new_handler return new_handler  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:111
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt15get_new_handlerv";

   function operator_new (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:126
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Znwm";

   function operator_new_ob (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:128
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Znam";

   procedure operator_delete (arg1 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:130
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPv";

   procedure operator_delete_ob (arg1 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:132
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPv";

   procedure operator_delete (arg1 : System.Address; arg2 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:135
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPvm";

   procedure operator_delete_ob (arg1 : System.Address; arg2 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:137
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPvm";

   function operator_new (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t; arg2 : access constant nothrow_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:140
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnwmRKSt9nothrow_t";

   function operator_new_ob (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t; arg2 : access constant nothrow_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:142
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnamRKSt9nothrow_t";

   procedure operator_delete (arg1 : System.Address; arg2 : access constant nothrow_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:144
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPvRKSt9nothrow_t";

   procedure operator_delete_ob (arg1 : System.Address; arg2 : access constant nothrow_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:146
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPvRKSt9nothrow_t";

   function operator_new (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t; arg2 : align_val_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:149
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnwmSt11align_val_t";

   function operator_new
     (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
      arg2 : align_val_t;
      arg3 : access constant nothrow_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:151
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnwmSt11align_val_tRKSt9nothrow_t";

   procedure operator_delete (arg1 : System.Address; arg2 : align_val_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:153
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPvSt11align_val_t";

   procedure operator_delete
     (arg1 : System.Address;
      arg2 : align_val_t;
      arg3 : access constant nothrow_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:155
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPvSt11align_val_tRKSt9nothrow_t";

   function operator_new_ob (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t; arg2 : align_val_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:157
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnamSt11align_val_t";

   function operator_new_ob
     (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
      arg2 : align_val_t;
      arg3 : access constant nothrow_t) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:159
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnamSt11align_val_tRKSt9nothrow_t";

   procedure operator_delete_ob (arg1 : System.Address; arg2 : align_val_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:161
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPvSt11align_val_t";

   procedure operator_delete_ob
     (arg1 : System.Address;
      arg2 : align_val_t;
      arg3 : access constant nothrow_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:163
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPvSt11align_val_tRKSt9nothrow_t";

   procedure operator_delete
     (arg1 : System.Address;
      arg2 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
      arg3 : align_val_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:166
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPvmSt11align_val_t";

   procedure operator_delete_ob
     (arg1 : System.Address;
      arg2 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
      arg3 : align_val_t)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:168
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPvmSt11align_val_t";

   function operator_new (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t; uu_p : System.Address) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:174
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnwmPv";

   function operator_new_ob (arg1 : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t; uu_p : System.Address) return System.Address  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:176
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZnamPv";

   procedure operator_delete (arg1 : System.Address; arg2 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:180
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdlPvS_";

   procedure operator_delete_ob (arg1 : System.Address; arg2 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:181
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZdaPvS_";

   procedure launder (arg1 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:204
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7launderPv";

   procedure launder (arg1 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:205
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7launderPKv";

   procedure launder (arg1 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:206
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7launderPVv";

   procedure launder (arg1 : System.Address)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:207
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7launderPVKv";

   hardware_destructive_interference_size : aliased constant cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:212
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt38hardware_destructive_interference_size";

   hardware_constructive_interference_size : aliased constant cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/new:213
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt39hardware_constructive_interference_size";

end cpp_12_1_0_new;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
