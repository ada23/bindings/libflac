pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with cpp_12_1_0_string;
limited with cpp_12_1_0_bits_basic_string_h;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;
limited with cpp_12_1_0_string_view;
with System;
with cpp_12_1_0_type_traits;

package cpp_12_1_0_bits_functional_hash_h is

   package uu_hash_base_unsigned_long_basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_basic_string;

   package uu_hash_base_unsigned_long_basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_basic_string;

   package uu_hash_base_unsigned_long_basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_basic_string;

   package uu_hash_base_unsigned_long_basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_basic_string;

   package uu_hash_base_unsigned_long_Class_basic_string.basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string.basic_string;

   package uu_hash_base_unsigned_long_Class_basic_string.basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string.basic_string;

   package uu_hash_base_unsigned_long_Class_basic_string.basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string.basic_string;

   package uu_hash_base_unsigned_long_Class_basic_string.basic_string is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string.basic_string;

   package uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view;

   package uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view;

   package uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view;

   package uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Class_basic_string_view.basic_string_view;

   package uu_hash_base_unsigned_long_address is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_address;

   package uu_hash_base_unsigned_long_long_double is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_long_double;

   package uu_hash_base_unsigned_long_double is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_double;

   package uu_hash_base_unsigned_long_float is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_float;

   package uu_hash_base_unsigned_long_uu_int128_unsigned is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_uu_int128_unsigned;

   package uu_hash_base_unsigned_long_Extensions.Signed_128 is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Extensions.Signed_128;

   package uu_hash_base_unsigned_long_unsigned_long_long is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_unsigned_long_long;

   package uu_hash_base_unsigned_long_unsigned_long is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_unsigned_long;

   package uu_hash_base_unsigned_long_unsigned is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_unsigned;

   package uu_hash_base_unsigned_long_unsigned_short is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_unsigned_short;

   package uu_hash_base_unsigned_long_Long_Long_Integer is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_Long_Long_Integer;

   package uu_hash_base_unsigned_long_long is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_long;

   package uu_hash_base_unsigned_long_int is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_int;

   package uu_hash_base_unsigned_long_short is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_short;

   package uu_hash_base_unsigned_long_char32_t is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_char32_t;

   package uu_hash_base_unsigned_long_char16_t is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_char16_t;

   package uu_hash_base_unsigned_long_wchar_t is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_wchar_t;

   package uu_hash_base_unsigned_long_unsigned_char is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_unsigned_char;

   package uu_hash_base_unsigned_long_signed_char is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_signed_char;

   package uu_hash_base_unsigned_long_char is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_char;

   package uu_hash_base_unsigned_long_bool is
      type uu_hash_base is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_hash_base_unsigned_long_bool;



   package hash_basic_string is
      type hash is limited record
         parent : aliased cpp_12_1_0_string.Class_uu_hash_string_base.uu_hash_string_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use hash_basic_string;

   package hash_basic_string is
      type hash is limited record
         parent : aliased cpp_12_1_0_string.Class_uu_hash_string_base.uu_hash_string_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use hash_basic_string;

   package hash_basic_string is
      type hash is limited record
         parent : aliased cpp_12_1_0_string.Class_uu_hash_string_base.uu_hash_string_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use hash_basic_string;

   package hash_basic_string is
      type hash is limited record
         parent : aliased cpp_12_1_0_string.Class_uu_hash_string_base.uu_hash_string_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use hash_basic_string;

   package Class_hash.hash_Class_basic_string.basic_string is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_bits_basic_string_h.Class_hash.hash; uu_s : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4301
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashINSt7__cxx1112basic_stringIDiSt11char_traitsIDiESaIDiEEEEclERKS5_";

   end;
   use Class_hash.hash_Class_basic_string.basic_string;

   package Class_hash.hash_Class_basic_string.basic_string is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_bits_basic_string_h.Class_hash.hash; uu_s : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4286
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashINSt7__cxx1112basic_stringIDsSt11char_traitsIDsESaIDsEEEEclERKS5_";

   end;
   use Class_hash.hash_Class_basic_string.basic_string;

   package Class_hash.hash_Class_basic_string.basic_string is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_bits_basic_string_h.Class_hash.hash; uu_s : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4253
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashINSt7__cxx1112basic_stringIwSt11char_traitsIwESaIwEEEEclERKS5_";

   end;
   use Class_hash.hash_Class_basic_string.basic_string;

   package Class_hash.hash_Class_basic_string.basic_string is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_bits_basic_string_h.Class_hash.hash; uu_s : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4239
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_";

   end;
   use Class_hash.hash_Class_basic_string.basic_string;

   package Class_hash.hash_Class_basic_string_view.basic_string_view is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_string_view.Class_hash.hash; uu_s : access constant cpp_12_1_0_string_view.Class_basic_string_view.basic_string_view) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:753
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashISt17basic_string_viewIDiSt11char_traitsIDiEEEclERKS3_";

   end;
   use Class_hash.hash_Class_basic_string_view.basic_string_view;

   package Class_hash.hash_Class_basic_string_view.basic_string_view is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_string_view.Class_hash.hash; uu_s : access constant cpp_12_1_0_string_view.Class_basic_string_view.basic_string_view) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:739
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashISt17basic_string_viewIDsSt11char_traitsIDsEEEclERKS3_";

   end;
   use Class_hash.hash_Class_basic_string_view.basic_string_view;

   package Class_hash.hash_Class_basic_string_view.basic_string_view is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_string_view.Class_hash.hash; uu_s : access constant cpp_12_1_0_string_view.Class_basic_string_view.basic_string_view) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:710
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashISt17basic_string_viewIwSt11char_traitsIwEEEclERKS3_";

   end;
   use Class_hash.hash_Class_basic_string_view.basic_string_view;

   package Class_hash.hash_Class_basic_string_view.basic_string_view is
      type Class_hash.hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant cpp_12_1_0_string_view.Class_hash.hash; uu_str : access constant cpp_12_1_0_string_view.Class_basic_string_view.basic_string_view) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:697
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashISt17basic_string_viewIcSt11char_traitsIcEEEclERKS3_";

   end;
   use Class_hash.hash_Class_basic_string_view.basic_string_view;

   package hash_address is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; arg2 : System.Address) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:276
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIDnEclEDn";

   end;
   use hash_address;

   package hash_long_double is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : long_double) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:268
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIeEclEe";

   end;
   use hash_long_double;

   package hash_double is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : double) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:255
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIdEclEd";

   end;
   use hash_double;

   package hash_float is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : float) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:243
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIfEclEf";

   end;
   use hash_float;

   package hash_uu_int128_unsigned is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : uu_int128_unsigned) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:178
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIoEclEo";

   end;
   use hash_uu_int128_unsigned;

   package hash_Extensions.Signed_128 is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : Extensions.Signed_128) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:176
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashInEclEn";

   end;
   use hash_Extensions.Signed_128;

   package hash_unsigned_long_long is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : Extensions.unsigned_long_long) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:172
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIyEclEy";

   end;
   use hash_unsigned_long_long;

   package hash_unsigned_long is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : unsigned_long) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:169
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashImEclEm";

   end;
   use hash_unsigned_long;

   package hash_unsigned is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : unsigned) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:166
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIjEclEj";

   end;
   use hash_unsigned;

   package hash_unsigned_short is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : unsigned_short) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:163
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashItEclEt";

   end;
   use hash_unsigned_short;

   package hash_Long_Long_Integer is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : Long_Long_Integer) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:160
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIxEclEx";

   end;
   use hash_Long_Long_Integer;

   package hash_long is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : long) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:157
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIlEclEl";

   end;
   use hash_long;

   package hash_int is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : int) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:154
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIiEclEi";

   end;
   use hash_int;

   package hash_short is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : short) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:151
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIsEclEs";

   end;
   use hash_short;

   package hash_char32_t is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : char32_t) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:148
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIDiEclEDi";

   end;
   use hash_char32_t;

   package hash_char16_t is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : char16_t) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:145
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIDsEclEDs";

   end;
   use hash_char16_t;

   package hash_wchar_t is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : wchar_t) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:137
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIwEclEw";

   end;
   use hash_wchar_t;

   package hash_unsigned_char is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : unsigned_char) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:134
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIhEclEh";

   end;
   use hash_unsigned_char;

   package hash_signed_char is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : signed_char) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:131
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIaEclEa";

   end;
   use hash_signed_char;

   package hash_char is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : char) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:128
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIcEclEc";

   end;
   use hash_char;

   package hash_bool is
      type hash is limited record
         parent : aliased uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant hash; uu_val : Extensions.bool) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:125
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4hashIbEclEb";

   end;
   use hash_bool;



   package Class_u_Hash_impl is
      type u_Hash_impl is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function hash
        (uu_ptr : System.Address;
         uu_clength : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
         uu_seed : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:204
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt10_Hash_impl4hashEPKvmm";
   end;
   use Class_u_Hash_impl;
   package Class_u_Fnv_hash_impl is
      type u_Fnv_hash_impl is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function hash
        (uu_ptr : System.Address;
         uu_clength : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
         uu_seed : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/functional_hash.h:223
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt14_Fnv_hash_impl4hashEPKvmm";
   end;
   use Class_u_Fnv_hash_impl;
   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_Class_hash.hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_Class_hash.hash;

   package uu_is_fast_hash_hash is
      type uu_is_fast_hash is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_fast_hash_hash;



end cpp_12_1_0_bits_functional_hash_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
