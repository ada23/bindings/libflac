pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with cpp_12_1_0_bits_stringfwd_h;
with Interfaces.C.Strings;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;

package cpp_12_1_0_bits_basic_string_h is

   function stoi
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:3971
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stoiERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi";

   function stol
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:3976
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stolERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi";

   function stoul
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return unsigned_long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:3981
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx115stoulERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi";

   function stoll
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return Long_Long_Integer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:3986
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx115stollERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi";

   function stoull
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return Extensions.unsigned_long_long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:3991
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx116stoullERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPmi";

   function stof (uu_str : access constant basic_string; uu_idx : access unsigned_long) return float  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:3997
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stofERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPm";

   function stod (uu_str : access constant basic_string; uu_idx : access unsigned_long) return double  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4001
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stodERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPm";

   function stold (uu_str : access constant basic_string; uu_idx : access unsigned_long) return long_double  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4005
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx115stoldERKNS_12basic_stringIcSt11char_traitsIcESaIcEEEPm";

   function to_string (uu_val : int) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4012
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEi";

   function to_string (uu_val : unsigned) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4026
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEj";

   function to_string (uu_val : long) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4037
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEl";

   function to_string (uu_val : unsigned_long) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4051
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEm";

   function to_string (uu_val : Long_Long_Integer) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4062
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEx";

   function to_string (uu_val : Extensions.unsigned_long_long) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4074
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEy";

   function to_string (uu_val : float) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4085
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEf";

   function to_string (uu_val : double) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4094
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEd";

   function to_string (uu_val : long_double) return cpp_12_1_0_bits_stringfwd_h.Class_string.string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4103
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx119to_stringEe";

   function stoi
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4114
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stoiERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPmi";

   function stol
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4119
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stolERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPmi";

   function stoul
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return unsigned_long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4124
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx115stoulERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPmi";

   function stoll
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return Long_Long_Integer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4129
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx115stollERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPmi";

   function stoull
     (uu_str : access constant basic_string;
      uu_idx : access unsigned_long;
      uu_base : int) return Extensions.unsigned_long_long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4134
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx116stoullERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPmi";

   function stof (uu_str : access constant basic_string; uu_idx : access unsigned_long) return float  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4140
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stofERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPm";

   function stod (uu_str : access constant basic_string; uu_idx : access unsigned_long) return double  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4144
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx114stodERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPm";

   function stold (uu_str : access constant basic_string; uu_idx : access unsigned_long) return long_double  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4148
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx115stoldERKNS_12basic_stringIwSt11char_traitsIwESaIwEEEPm";

   function to_wstring (uu_val : int) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4154
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEi";

   function to_wstring (uu_val : unsigned) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4159
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEj";

   function to_wstring (uu_val : long) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4165
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEl";

   function to_wstring (uu_val : unsigned_long) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4170
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEm";

   function to_wstring (uu_val : Long_Long_Integer) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4176
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEx";

   function to_wstring (uu_val : Extensions.unsigned_long_long) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4182
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEy";

   function to_wstring (uu_val : float) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4188
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEf";

   function to_wstring (uu_val : double) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4197
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEd";

   function to_wstring (uu_val : long_double) return cpp_12_1_0_bits_stringfwd_h.Class_wstring.wstring  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4206
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt7__cxx1110to_wstringEe";

   type basic_string;
   function operator""s (uu_str : Interfaces.C.Strings.chars_ptr; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4329
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals15string_literalsli1sB5cxx11EPKcm";

   type basic_string;
   function operator""s (uu_str : access wchar_t; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4334
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals15string_literalsli1sB5cxx11EPKwm";

   type basic_string;
   function operator""s (uu_str : access char16_t; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4346
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals15string_literalsli1sB5cxx11EPKDsm";

   type basic_string;
   function operator""s (uu_str : access char32_t; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/basic_string.h:4351
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals15string_literalsli1sB5cxx11EPKDim";

end cpp_12_1_0_bits_basic_string_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
