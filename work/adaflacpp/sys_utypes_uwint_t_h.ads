pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;

package sys_utypes_uwint_t_h is

   subtype wint_t is arm_utypes_h.uu_darwin_wint_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_wint_t.h:32

end sys_utypes_uwint_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
