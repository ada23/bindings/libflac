pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
with arm_utypes_h;

package stdarg_h is

   --  arg-macro: procedure va_start (v, l)
   --    __builtin_va_start(v,l)
   --  arg-macro: procedure va_end (v)
   --    __builtin_va_end(v)
   --  arg-macro: procedure va_arg (v, l)
   --    __builtin_va_arg(v,l)
   --  arg-macro: procedure va_copy (d, s)
   --    __builtin_va_copy(d,s)
   subtype uu_gnuc_va_list is System.Address;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stdarg.h:40

   subtype va_list is arm_utypes_h.uu_darwin_va_list;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stdarg.h:99

end stdarg_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
