pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_aarch64_apple_darwin21_bits_atomic_word_h is

   subtype u_Atomic_word is int;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/atomic_word.h:32

end cpp_12_1_0_aarch64_apple_darwin21_bits_atomic_word_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
