pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;

package cpp_12_1_0_bits_postypes_h is

   subtype streamoff is Long_Long_Integer;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/postypes.h:62

   subtype streamsize is cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.ptrdiff_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/postypes.h:68

   package fpos_uu_mbstate_t is
      type fpos is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use fpos_uu_mbstate_t;



   subtype streampos is fpos;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/postypes.h:204

   subtype wstreampos is fpos;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/postypes.h:206

   subtype u16streampos is fpos;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/postypes.h:215

   subtype u32streampos is fpos;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/postypes.h:217

end cpp_12_1_0_bits_postypes_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
