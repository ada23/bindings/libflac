pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_aarch64_apple_darwin21_bits_cpplocale_h is

   type uu_c_locale is access all int;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/c++locale.h:49

   --  skipped func __convert_from_v

end cpp_12_1_0_aarch64_apple_darwin21_bits_cpplocale_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
