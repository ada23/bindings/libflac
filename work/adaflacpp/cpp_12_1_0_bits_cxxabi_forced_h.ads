pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_cxxabi_forced_h is

   package Class_uu_forced_unwind is
      type uu_forced_unwind is abstract tagged limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      procedure Delete_uu_forced_unwind (this : access uu_forced_unwind)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/cxxabi_forced.h:50
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN10__cxxabiv115__forced_unwindD1Ev";

      procedure Delete_And_Free_uu_forced_unwind (this : access uu_forced_unwind)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/cxxabi_forced.h:50
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN10__cxxabiv115__forced_unwindD0Ev";

      procedure uu_pure_dummy (this : access uu_forced_unwind) is abstract;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/cxxabi_forced.h:53
   end;
   use Class_uu_forced_unwind;
end cpp_12_1_0_bits_cxxabi_forced_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
