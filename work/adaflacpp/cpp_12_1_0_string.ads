pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_bits_functional_hash_h;
limited with cpp_12_1_0_bits_basic_string_h;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;

package cpp_12_1_0_string is

   package polymorphic_allocator_wchar_t is
      type polymorphic_allocator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use polymorphic_allocator_wchar_t;

   package polymorphic_allocator_char32_t is
      type polymorphic_allocator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use polymorphic_allocator_char32_t;

   package polymorphic_allocator_char16_t is
      type polymorphic_allocator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use polymorphic_allocator_char16_t;

   package polymorphic_allocator_char is
      type polymorphic_allocator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use polymorphic_allocator_char;



   package basic_string_wchar_t_Class_char_traits.char_traits is
      type basic_string is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_string_wchar_t_Class_char_traits.char_traits;

   package basic_string_char32_t_Class_char_traits.char_traits is
      type basic_string is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_string_char32_t_Class_char_traits.char_traits;

   package basic_string_char16_t_Class_char_traits.char_traits is
      type basic_string is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_string_char16_t_Class_char_traits.char_traits;

   package basic_string_char_Class_char_traits.char_traits is
      type basic_string is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use basic_string_char_Class_char_traits.char_traits;



   subtype string is basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:65

   subtype u16string is basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:69

   subtype u32string is basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:70

   subtype wstring is basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:71

   package uu_hash_string_base_basic_string is
      type uu_hash_string_base is limited record
         parent : aliased cpp_12_1_0_bits_functional_hash_h.uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant uu_hash_string_base; uu_s : access constant cpp_12_1_0_bits_basic_string_h.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt18__hash_string_baseINSt7__cxx1112basic_stringIwSt11char_traitsIwENSt3pmr21polymorphic_allocatorIwEEEEEclERKS7_";

   end;
   use uu_hash_string_base_basic_string;

   package uu_hash_string_base_basic_string is
      type uu_hash_string_base is limited record
         parent : aliased cpp_12_1_0_bits_functional_hash_h.uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant uu_hash_string_base; uu_s : access constant cpp_12_1_0_bits_basic_string_h.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt18__hash_string_baseINSt7__cxx1112basic_stringIDiSt11char_traitsIDiENSt3pmr21polymorphic_allocatorIDiEEEEEclERKS7_";

   end;
   use uu_hash_string_base_basic_string;

   package uu_hash_string_base_basic_string is
      type uu_hash_string_base is limited record
         parent : aliased cpp_12_1_0_bits_functional_hash_h.uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant uu_hash_string_base; uu_s : access constant cpp_12_1_0_bits_basic_string_h.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt18__hash_string_baseINSt7__cxx1112basic_stringIDsSt11char_traitsIDsENSt3pmr21polymorphic_allocatorIDsEEEEEclERKS7_";

   end;
   use uu_hash_string_base_basic_string;

   package uu_hash_string_base_basic_string is
      type uu_hash_string_base is limited record
         parent : aliased cpp_12_1_0_bits_functional_hash_h.uu_hash_base;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op (this : access constant uu_hash_string_base; uu_s : access constant cpp_12_1_0_bits_basic_string_h.basic_string) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string:79
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt18__hash_string_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcENSt3pmr21polymorphic_allocatorIcEEEEEclERKS7_";

   end;
   use uu_hash_string_base_basic_string;



end cpp_12_1_0_string;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
