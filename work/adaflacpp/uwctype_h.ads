pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_uwint_t_h;
with utypes_uwctype_t_h;
with Interfaces.C.Strings;

package uwctype_h is

   function iswalnum (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:51
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswalnumi";

   function iswalpha (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:57
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswalphai";

   function iswcntrl (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:63
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswcntrli";

   function iswctype (u_wc : sys_utypes_uwint_t_h.wint_t; u_charclass : utypes_uwctype_t_h.wctype_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:69
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswctypeij";

   function iswdigit (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:75
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswdigiti";

   function iswgraph (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:81
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswgraphi";

   function iswlower (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:87
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswloweri";

   function iswprint (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:93
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswprinti";

   function iswpunct (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:99
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswpuncti";

   function iswspace (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:105
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswspacei";

   function iswupper (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:111
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8iswupperi";

   function iswxdigit (u_wc : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:117
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z9iswxdigiti";

   function towlower (u_wc : sys_utypes_uwint_t_h.wint_t) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:123
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8towloweri";

   function towupper (u_wc : sys_utypes_uwint_t_h.wint_t) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:129
   with Import => True, 
        Convention => CPP, 
        External_Name => "_Z8towupperi";

   function wctype (arg1 : Interfaces.C.Strings.chars_ptr) return utypes_uwctype_t_h.wctype_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_wctype.h:157
   with Import => True, 
        Convention => C, 
        External_Name => "wctype";

end uwctype_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
