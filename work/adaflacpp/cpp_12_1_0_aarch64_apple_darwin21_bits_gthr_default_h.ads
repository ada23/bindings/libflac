pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_upthread_upthread_t_h;
with sys_upthread_upthread_key_t_h;
with sys_upthread_upthread_once_t_h;
with sys_upthread_upthread_mutex_t_h;
with sys_upthread_upthread_cond_t_h;
with sys_utypes_utimespec_h;

package cpp_12_1_0_aarch64_apple_darwin21_bits_gthr_default_h is

   subtype uu_gthread_t is sys_upthread_upthread_t_h.pthread_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:47

   subtype uu_gthread_key_t is sys_upthread_upthread_key_t_h.pthread_key_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:48

   subtype uu_gthread_once_t is sys_upthread_upthread_once_t_h.pthread_once_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:49

   subtype uu_gthread_mutex_t is sys_upthread_upthread_mutex_t_h.pthread_mutex_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:50

   subtype uu_gthread_recursive_mutex_t is sys_upthread_upthread_mutex_t_h.pthread_mutex_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:51

   subtype uu_gthread_cond_t is sys_upthread_upthread_cond_t_h.pthread_cond_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:52

   subtype uu_gthread_time_t is sys_utypes_utimespec_h.timespec;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/aarch64-apple-darwin21/bits/gthr-default.h:53

   --  skipped func __gthread_active_p

   --  skipped func __gthread_create

   --  skipped func __gthread_join

   --  skipped func __gthread_detach

   --  skipped func __gthread_equal

   --  skipped func __gthread_self

   --  skipped func __gthread_yield

   --  skipped func __gthread_once

   --  skipped func __gthread_key_create

   --  skipped func __gthread_key_delete

   --  skipped func __gthread_getspecific

   --  skipped func __gthread_setspecific

   --  skipped func __gthread_mutex_init_function

   --  skipped func __gthread_mutex_destroy

   --  skipped func __gthread_mutex_lock

   --  skipped func __gthread_mutex_trylock

   --  skipped func __gthread_mutex_unlock

   --  skipped func __gthread_recursive_mutex_init_function

   --  skipped func __gthread_recursive_mutex_lock

   --  skipped func __gthread_recursive_mutex_trylock

   --  skipped func __gthread_recursive_mutex_unlock

   --  skipped func __gthread_recursive_mutex_destroy

   --  skipped func __gthread_cond_broadcast

   --  skipped func __gthread_cond_signal

   --  skipped func __gthread_cond_wait

   --  skipped func __gthread_cond_timedwait

   --  skipped func __gthread_cond_wait_recursive

   --  skipped func __gthread_cond_destroy

end cpp_12_1_0_aarch64_apple_darwin21_bits_gthr_default_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
