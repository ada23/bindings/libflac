pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;
with Interfaces.C.Extensions;
with Interfaces.C.Strings;

package cpp_12_1_0_string_view is

   --  skipped func __sv_check

   --  skipped func __sv_limit

   package basic_string_view_char32_t_Class_char_traits.char_traits is
      type basic_string_view is limited record
         u_M_len : aliased cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:516
         u_M_str : access char32_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:517
      end record
      with Import => True,
           Convention => CPP;

      function New_basic_string_view return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:126
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEEC1Ev");

      function New_basic_string_view (uu_str : access char32_t) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:133
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEEC1EPKDi");

      function New_basic_string_view (uu_str : access char32_t; uu_len : size_type) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:139
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEEC1EPKDim");

      function Assign_basic_string_view (this : access basic_string_view; arg2 : access constant basic_string_view) return access basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:177
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEEaSERKS2_";

      function c_begin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5beginEv";

      function c_end (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:186
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE3endEv";

      function cbegin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:190
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE6cbeginEv";

      function cend (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:194
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4cendEv";

      function rbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:198
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE6rbeginEv";

      function rend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:202
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4rendEv";

      function crbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:206
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7crbeginEv";

      function crend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5crendEv";

      function size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:216
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4sizeEv";

      function length (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:220
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE6lengthEv";

      function max_size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:224
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE8max_sizeEv";

      function empty (this : access constant basic_string_view) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:231
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5emptyEv";

      function operator_ob (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:237
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEEixEm";

      function c_at (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:244
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE2atEm";

      function front (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:254
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5frontEv";

      function back (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:261
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4backEv";

      function data (this : access constant basic_string_view) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:268
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4dataEv";

      procedure remove_prefix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:274
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEE13remove_prefixEm";

      procedure remove_suffix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:282
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEE13remove_suffixEm";

      procedure swap (this : access basic_string_view; uu_sv : access basic_string_view)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:286
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEE4swapERS2_";

      function copy
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_n : size_type;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:297
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4copyEPDimm";

      function substr
        (this : access constant basic_string_view;
         uu_pos : size_type;
         uu_n : size_type) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:309
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE6substrEmm";

      function compare (this : access constant basic_string_view; uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:317
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7compareES2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:327
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7compareEmmS2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view;
         uu_pos2 : size_type;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:331
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7compareEmmS2_mm";

      function compare (this : access constant basic_string_view; uu_str : access char32_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:338
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7compareEPKDi";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : access char32_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:342
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7compareEmmPKDi";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : access char32_t;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:346
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE7compareEmmPKDim";

      function find
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:403
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4findES2_m";

      function find
        (this : access constant basic_string_view;
         uu_c : char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:407
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4findEDim";

      function find
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:410
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4findEPKDimm";

      function find
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:413
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE4findEPKDim";

      function rfind
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:417
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5rfindES2_m";

      function rfind
        (this : access constant basic_string_view;
         uu_c : char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:421
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5rfindEDim";

      function rfind
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:424
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5rfindEPKDimm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:427
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE5rfindEPKDim";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:431
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE13find_first_ofES2_m";

      function find_first_of
        (this : access constant basic_string_view;
         uu_c : char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:435
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE13find_first_ofEDim";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:439
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE13find_first_ofEPKDimm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:443
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE13find_first_ofEPKDim";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:447
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE12find_last_ofES2_m";

      function find_last_of
        (this : access constant basic_string_view;
         uu_c : char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:452
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE12find_last_ofEDim";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:456
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE12find_last_ofEPKDimm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:460
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE12find_last_ofEPKDim";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:464
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE17find_first_not_ofES2_m";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_c : char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:469
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE17find_first_not_ofEDim";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:472
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE17find_first_not_ofEPKDimm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:476
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE17find_first_not_ofEPKDim";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:483
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE16find_last_not_ofES2_m";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_c : char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:488
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE16find_last_not_ofEDim";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:491
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE16find_last_not_ofEPKDimm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : access char32_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:495
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDiSt11char_traitsIDiEE16find_last_not_ofEPKDim";

      --  skipped func _S_compare

      npos : aliased constant size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDiSt11char_traitsIDiEE4nposE";

   end;
   use basic_string_view_char32_t_Class_char_traits.char_traits;

   package basic_string_view_char16_t_Class_char_traits.char_traits is
      type basic_string_view is limited record
         u_M_len : aliased cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:516
         u_M_str : access char16_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:517
      end record
      with Import => True,
           Convention => CPP;

      function New_basic_string_view return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:126
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEEC1Ev");

      function New_basic_string_view (uu_str : access char16_t) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:133
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEEC1EPKDs");

      function New_basic_string_view (uu_str : access char16_t; uu_len : size_type) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:139
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEEC1EPKDsm");

      function Assign_basic_string_view (this : access basic_string_view; arg2 : access constant basic_string_view) return access basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:177
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEEaSERKS2_";

      function c_begin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5beginEv";

      function c_end (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:186
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE3endEv";

      function cbegin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:190
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE6cbeginEv";

      function cend (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:194
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4cendEv";

      function rbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:198
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE6rbeginEv";

      function rend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:202
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4rendEv";

      function crbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:206
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7crbeginEv";

      function crend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5crendEv";

      function size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:216
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4sizeEv";

      function length (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:220
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE6lengthEv";

      function max_size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:224
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE8max_sizeEv";

      function empty (this : access constant basic_string_view) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:231
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5emptyEv";

      function operator_ob (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:237
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEEixEm";

      function c_at (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:244
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE2atEm";

      function front (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:254
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5frontEv";

      function back (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:261
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4backEv";

      function data (this : access constant basic_string_view) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:268
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4dataEv";

      procedure remove_prefix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:274
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEE13remove_prefixEm";

      procedure remove_suffix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:282
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEE13remove_suffixEm";

      procedure swap (this : access basic_string_view; uu_sv : access basic_string_view)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:286
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEE4swapERS2_";

      function copy
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_n : size_type;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:297
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4copyEPDsmm";

      function substr
        (this : access constant basic_string_view;
         uu_pos : size_type;
         uu_n : size_type) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:309
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE6substrEmm";

      function compare (this : access constant basic_string_view; uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:317
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7compareES2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:327
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7compareEmmS2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view;
         uu_pos2 : size_type;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:331
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7compareEmmS2_mm";

      function compare (this : access constant basic_string_view; uu_str : access char16_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:338
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7compareEPKDs";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : access char16_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:342
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7compareEmmPKDs";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : access char16_t;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:346
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE7compareEmmPKDsm";

      function find
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:403
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4findES2_m";

      function find
        (this : access constant basic_string_view;
         uu_c : char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:407
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4findEDsm";

      function find
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:410
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4findEPKDsmm";

      function find
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:413
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE4findEPKDsm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:417
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5rfindES2_m";

      function rfind
        (this : access constant basic_string_view;
         uu_c : char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:421
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5rfindEDsm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:424
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5rfindEPKDsmm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:427
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE5rfindEPKDsm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:431
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE13find_first_ofES2_m";

      function find_first_of
        (this : access constant basic_string_view;
         uu_c : char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:435
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE13find_first_ofEDsm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:439
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE13find_first_ofEPKDsmm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:443
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE13find_first_ofEPKDsm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:447
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE12find_last_ofES2_m";

      function find_last_of
        (this : access constant basic_string_view;
         uu_c : char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:452
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE12find_last_ofEDsm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:456
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE12find_last_ofEPKDsmm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:460
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE12find_last_ofEPKDsm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:464
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE17find_first_not_ofES2_m";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_c : char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:469
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE17find_first_not_ofEDsm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:472
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE17find_first_not_ofEPKDsmm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:476
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE17find_first_not_ofEPKDsm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:483
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE16find_last_not_ofES2_m";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_c : char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:488
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE16find_last_not_ofEDsm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:491
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE16find_last_not_ofEPKDsmm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : access char16_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:495
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIDsSt11char_traitsIDsEE16find_last_not_ofEPKDsm";

      --  skipped func _S_compare

      npos : aliased constant size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIDsSt11char_traitsIDsEE4nposE";

   end;
   use basic_string_view_char16_t_Class_char_traits.char_traits;

   package basic_string_view_wchar_t_Class_char_traits.char_traits is
      type basic_string_view is limited record
         u_M_len : aliased cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:516
         u_M_str : access wchar_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:517
      end record
      with Import => True,
           Convention => CPP;

      function New_basic_string_view return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:126
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIwSt11char_traitsIwEEC1Ev");

      function New_basic_string_view (uu_str : access wchar_t) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:133
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIwSt11char_traitsIwEEC1EPKw");

      function New_basic_string_view (uu_str : access wchar_t; uu_len : size_type) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:139
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIwSt11char_traitsIwEEC1EPKwm");

      function Assign_basic_string_view (this : access basic_string_view; arg2 : access constant basic_string_view) return access basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:177
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIwSt11char_traitsIwEEaSERKS2_";

      function c_begin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5beginEv";

      function c_end (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:186
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE3endEv";

      function cbegin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:190
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE6cbeginEv";

      function cend (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:194
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4cendEv";

      function rbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:198
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE6rbeginEv";

      function rend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:202
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4rendEv";

      function crbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:206
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7crbeginEv";

      function crend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5crendEv";

      function size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:216
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4sizeEv";

      function length (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:220
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE6lengthEv";

      function max_size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:224
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE8max_sizeEv";

      function empty (this : access constant basic_string_view) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:231
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5emptyEv";

      function operator_ob (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:237
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEEixEm";

      function c_at (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:244
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE2atEm";

      function front (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:254
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5frontEv";

      function back (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:261
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4backEv";

      function data (this : access constant basic_string_view) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:268
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4dataEv";

      procedure remove_prefix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:274
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIwSt11char_traitsIwEE13remove_prefixEm";

      procedure remove_suffix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:282
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIwSt11char_traitsIwEE13remove_suffixEm";

      procedure swap (this : access basic_string_view; uu_sv : access basic_string_view)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:286
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIwSt11char_traitsIwEE4swapERS2_";

      function copy
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_n : size_type;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:297
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4copyEPwmm";

      function substr
        (this : access constant basic_string_view;
         uu_pos : size_type;
         uu_n : size_type) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:309
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE6substrEmm";

      function compare (this : access constant basic_string_view; uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:317
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7compareES2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:327
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7compareEmmS2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view;
         uu_pos2 : size_type;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:331
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7compareEmmS2_mm";

      function compare (this : access constant basic_string_view; uu_str : access wchar_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:338
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7compareEPKw";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : access wchar_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:342
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7compareEmmPKw";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : access wchar_t;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:346
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE7compareEmmPKwm";

      function find
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:403
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4findES2_m";

      function find
        (this : access constant basic_string_view;
         uu_c : wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:407
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4findEwm";

      function find
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:410
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4findEPKwmm";

      function find
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:413
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE4findEPKwm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:417
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5rfindES2_m";

      function rfind
        (this : access constant basic_string_view;
         uu_c : wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:421
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5rfindEwm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:424
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5rfindEPKwmm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:427
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE5rfindEPKwm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:431
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE13find_first_ofES2_m";

      function find_first_of
        (this : access constant basic_string_view;
         uu_c : wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:435
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE13find_first_ofEwm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:439
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE13find_first_ofEPKwmm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:443
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE13find_first_ofEPKwm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:447
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE12find_last_ofES2_m";

      function find_last_of
        (this : access constant basic_string_view;
         uu_c : wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:452
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE12find_last_ofEwm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:456
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE12find_last_ofEPKwmm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:460
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE12find_last_ofEPKwm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:464
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE17find_first_not_ofES2_m";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_c : wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:469
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE17find_first_not_ofEwm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:472
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE17find_first_not_ofEPKwmm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:476
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE17find_first_not_ofEPKwm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:483
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE16find_last_not_ofES2_m";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_c : wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:488
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE16find_last_not_ofEwm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:491
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE16find_last_not_ofEPKwmm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : access wchar_t;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:495
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIwSt11char_traitsIwEE16find_last_not_ofEPKwm";

      --  skipped func _S_compare

      npos : aliased constant size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIwSt11char_traitsIwEE4nposE";

   end;
   use basic_string_view_wchar_t_Class_char_traits.char_traits;

   package basic_string_view_char_Class_char_traits.char_traits is
      type basic_string_view is limited record
         u_M_len : aliased cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:516
         u_M_str : Interfaces.C.Strings.chars_ptr;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:517
      end record
      with Import => True,
           Convention => CPP;

      function New_basic_string_view return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:126
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIcSt11char_traitsIcEEC1Ev");

      function New_basic_string_view (uu_str : Interfaces.C.Strings.chars_ptr) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:133
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIcSt11char_traitsIcEEC1EPKc");

      function New_basic_string_view (uu_str : Interfaces.C.Strings.chars_ptr; uu_len : size_type) return basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:139
      pragma CPP_Constructor (New_basic_string_view, "_ZNSt17basic_string_viewIcSt11char_traitsIcEEC1EPKcm");

      function Assign_basic_string_view (this : access basic_string_view; arg2 : access constant basic_string_view) return access basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:177
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIcSt11char_traitsIcEEaSERKS2_";

      function c_begin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5beginEv";

      function c_end (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:186
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE3endEv";

      function cbegin (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:190
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE6cbeginEv";

      function cend (this : access constant basic_string_view) return const_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:194
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4cendEv";

      function rbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:198
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE6rbeginEv";

      function rend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:202
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4rendEv";

      function crbegin (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:206
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7crbeginEv";

      function crend (this : access constant basic_string_view) return const_reverse_iterator  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5crendEv";

      function size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:216
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4sizeEv";

      function length (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:220
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE6lengthEv";

      function max_size (this : access constant basic_string_view) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:224
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE8max_sizeEv";

      function empty (this : access constant basic_string_view) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:231
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5emptyEv";

      function operator_ob (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:237
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEEixEm";

      function c_at (this : access constant basic_string_view; uu_pos : size_type) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:244
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE2atEm";

      function front (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:254
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5frontEv";

      function back (this : access constant basic_string_view) return const_reference  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:261
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4backEv";

      function data (this : access constant basic_string_view) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:268
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4dataEv";

      procedure remove_prefix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:274
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIcSt11char_traitsIcEE13remove_prefixEm";

      procedure remove_suffix (this : access basic_string_view; uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:282
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIcSt11char_traitsIcEE13remove_suffixEm";

      procedure swap (this : access basic_string_view; uu_sv : access basic_string_view)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:286
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIcSt11char_traitsIcEE4swapERS2_";

      function copy
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_n : size_type;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:297
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4copyEPcmm";

      function substr
        (this : access constant basic_string_view;
         uu_pos : size_type;
         uu_n : size_type) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:309
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE6substrEmm";

      function compare (this : access constant basic_string_view; uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:317
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7compareES2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:327
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7compareEmmS2_";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : basic_string_view;
         uu_pos2 : size_type;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:331
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7compareEmmS2_mm";

      function compare (this : access constant basic_string_view; uu_str : Interfaces.C.Strings.chars_ptr) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:338
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7compareEPKc";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : Interfaces.C.Strings.chars_ptr) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:342
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7compareEmmPKc";

      function compare
        (this : access constant basic_string_view;
         uu_pos1 : size_type;
         uu_n1 : size_type;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_n2 : size_type) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:346
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE7compareEmmPKcm";

      function find
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:403
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4findES2_m";

      function find
        (this : access constant basic_string_view;
         uu_c : char;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:407
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4findEcm";

      function find
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:410
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4findEPKcmm";

      function find
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:413
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE4findEPKcm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:417
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5rfindES2_m";

      function rfind
        (this : access constant basic_string_view;
         uu_c : char;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:421
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5rfindEcm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:424
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5rfindEPKcmm";

      function rfind
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:427
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE5rfindEPKcm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:431
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE13find_first_ofES2_m";

      function find_first_of
        (this : access constant basic_string_view;
         uu_c : char;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:435
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE13find_first_ofEcm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:439
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE13find_first_ofEPKcmm";

      function find_first_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:443
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE13find_first_ofEPKcm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:447
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE12find_last_ofES2_m";

      function find_last_of
        (this : access constant basic_string_view;
         uu_c : char;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:452
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE12find_last_ofEcm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:456
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE12find_last_ofEPKcmm";

      function find_last_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:460
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE12find_last_ofEPKcm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:464
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE17find_first_not_ofES2_m";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_c : char;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:469
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE17find_first_not_ofEcm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:472
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE17find_first_not_ofEPKcmm";

      function find_first_not_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:476
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE17find_first_not_ofEPKcm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : basic_string_view;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:483
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE16find_last_not_ofES2_m";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_c : char;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:488
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE16find_last_not_ofEcm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type;
         uu_n : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:491
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE16find_last_not_ofEPKcmm";

      function find_last_not_of
        (this : access constant basic_string_view;
         uu_str : Interfaces.C.Strings.chars_ptr;
         uu_pos : size_type) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:495
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17basic_string_viewIcSt11char_traitsIcEE16find_last_not_ofEPKcm";

      --  skipped func _S_compare

      npos : aliased constant size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17basic_string_viewIcSt11char_traitsIcEE4nposE";

   end;
   use basic_string_view_char_Class_char_traits.char_traits;



   subtype string_view is basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:679

   subtype wstring_view is basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:680

   subtype u16string_view is basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:684

   subtype u32string_view is basic_string_view;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:685

   function operator""sv (uu_str : Interfaces.C.Strings.chars_ptr; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:769
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals20string_view_literalsli2svEPKcm";

   function operator""sv (uu_str : access wchar_t; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:773
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals20string_view_literalsli2svEPKwm";

   function operator""sv (uu_str : access char16_t; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:783
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals20string_view_literalsli2svEPKDsm";

   function operator""sv (uu_str : access char32_t; uu_len : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return basic_string_view  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/string_view:787
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZNSt8literals20string_view_literalsli2svEPKDim";

end cpp_12_1_0_string_view;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
