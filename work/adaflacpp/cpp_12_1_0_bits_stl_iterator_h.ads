pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_stl_iterator_h is

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_uu_normal_iterator is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_uu_normal_iterator;

   package reverse_iterator_accesschar32_t is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_accesschar32_t;

   package reverse_iterator_accesschar16_t is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_accesschar16_t;

   package reverse_iterator_accesswchar_t is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_accesswchar_t;

   package reverse_iterator_chars_ptr is
      type reverse_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use reverse_iterator_chars_ptr;



   package uu_normal_iterator_accesschar32_t_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_accesschar32_t_Class_basic_string.basic_string;

   package uu_normal_iterator_accesschar32_t_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_accesschar32_t_Class_basic_string.basic_string;

   package uu_normal_iterator_accesschar16_t_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_accesschar16_t_Class_basic_string.basic_string;

   package uu_normal_iterator_accesschar16_t_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_accesschar16_t_Class_basic_string.basic_string;

   package uu_normal_iterator_accesswchar_t_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_accesswchar_t_Class_basic_string.basic_string;

   package uu_normal_iterator_accesswchar_t_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_accesswchar_t_Class_basic_string.basic_string;

   package uu_normal_iterator_chars_ptr_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_chars_ptr_Class_basic_string.basic_string;

   package uu_normal_iterator_chars_ptr_Class_basic_string.basic_string is
      type uu_normal_iterator is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_normal_iterator_chars_ptr_Class_basic_string.basic_string;



end cpp_12_1_0_bits_stl_iterator_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
