pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with sys_upthread_upthread_types_h;
with sys_qos_h;
with sys_upthread_upthread_t_h;

package pthread_qos_h is

   function pthread_attr_set_qos_class_np
     (uu_attr : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t;
      uu_qos_class : sys_qos_h.qos_class_t;
      uu_relative_priority : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:83
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_set_qos_class_np";

   function pthread_attr_get_qos_class_np
     (uu_attr : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t;
      uu_qos_class : access unsigned;
      uu_relative_priority : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:114
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_get_qos_class_np";

   function pthread_set_qos_class_self_np (uu_qos_class : sys_qos_h.qos_class_t; uu_relative_priority : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:155
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_set_qos_class_self_np";

   function pthread_get_qos_class_np
     (uu_pthread : sys_upthread_upthread_t_h.pthread_t;
      uu_qos_class : access unsigned;
      uu_relative_priority : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:186
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_get_qos_class_np";

   type pthread_override_s is null record;   -- incomplete struct

   type pthread_override_t is access all pthread_override_s;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:211

   function pthread_override_qos_class_start_np
     (uu_pthread : sys_upthread_upthread_t_h.pthread_t;
      uu_qos_class : sys_qos_h.qos_class_t;
      uu_relative_priority : int) return pthread_override_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:265
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_override_qos_class_start_np";

   function pthread_override_qos_class_end_np (uu_override : pthread_override_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/qos.h:293
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_override_qos_class_end_np";

end pthread_qos_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
