pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_bits_cpp_type_traits_h;

package cpp_12_1_0_ext_numeric_traits_h is

   package uu_is_integer_nonstrict_int is
      type uu_is_integer_nonstrict is limited record
         parent : aliased cpp_12_1_0_bits_cpp_type_traits_h.uu_is_integer;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integer_nonstrict_int;



   package uu_numeric_traits_integer_int is
      type uu_numeric_traits_integer is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy;









   end;
   use uu_numeric_traits_integer_int;



   package uu_int_traits_int is
      type uu_int_traits is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy;









   end;
   use uu_int_traits_int;



   package uu_numeric_traits_floating_long_double is
      type uu_numeric_traits_floating is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy;









   end;
   use uu_numeric_traits_floating_long_double;

   package uu_numeric_traits_floating_double is
      type uu_numeric_traits_floating is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy;









   end;
   use uu_numeric_traits_floating_double;

   package uu_numeric_traits_floating_float is
      type uu_numeric_traits_floating is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy;









   end;
   use uu_numeric_traits_floating_float;



   package uu_numeric_traits_int is
      type uu_numeric_traits is limited record
         parent : aliased uu_numeric_traits_integer;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_numeric_traits_int;

   package uu_numeric_traits_long_double is
      type uu_numeric_traits is limited record
         parent : aliased uu_numeric_traits_floating;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_numeric_traits_long_double;

   package uu_numeric_traits_double is
      type uu_numeric_traits is limited record
         parent : aliased uu_numeric_traits_floating;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_numeric_traits_double;

   package uu_numeric_traits_float is
      type uu_numeric_traits is limited record
         parent : aliased uu_numeric_traits_floating;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_numeric_traits_float;



end cpp_12_1_0_ext_numeric_traits_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
