pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with FLAC_stream_encoder_h;
with Interfaces.C.Extensions;
with utypes_uuint32_t_h;
with Interfaces.C.Strings;
with FLAC_ordinals_h;
with System;
with FLACpp_decoder_h;
with sys_utypes_usize_t_h;
limited with FLAC_format_h;
limited with ustdio_h;
limited with cpp_12_1_0_bits_basic_string_h;

package FLACpp_encoder_h is

   package Class_Stream is
      type State is record
         state_u : aliased FLAC_stream_encoder_h.FLAC_u_StreamEncoderState;  -- ../include/FLAC++/encoder.h:113
      end record
      with Convention => C_Pass_By_Copy;
      type Stream is abstract tagged limited record
         encoder_u : access FLAC_stream_encoder_h.FLAC_u_StreamEncoder;  -- ../include/FLAC++/encoder.h:204
      end record
      with Import => True,
           Convention => CPP;

      function New_Stream return Stream is abstract;  -- ../include/FLAC++/encoder.h:116
      pragma CPP_Constructor (New_Stream, "_ZN4FLAC7Encoder6StreamC1Ev");

      procedure Delete_Stream (this : access Stream)  -- ../include/FLAC++/encoder.h:117
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6StreamD1Ev";

      procedure Delete_And_Free_Stream (this : access Stream)  -- ../include/FLAC++/encoder.h:117
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6StreamD0Ev";

      function is_valid (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:124
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream8is_validEv";

      --  skipped func __conv_op 

      function set_ogg_serial_number (this : access Stream; value : long) return Extensions.bool  -- ../include/FLAC++/encoder.h:128
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream21set_ogg_serial_numberEl";

      function set_verify (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:129
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream10set_verifyEb";

      function set_streamable_subset (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:130
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream21set_streamable_subsetEb";

      function set_channels (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:131
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream12set_channelsEj";

      function set_bits_per_sample (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:132
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream19set_bits_per_sampleEj";

      function set_sample_rate (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:133
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream15set_sample_rateEj";

      function set_compression_level (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:134
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream21set_compression_levelEj";

      function set_blocksize (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:135
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream13set_blocksizeEj";

      function set_do_mid_side_stereo (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:136
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream22set_do_mid_side_stereoEb";

      function set_loose_mid_side_stereo (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:137
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream25set_loose_mid_side_stereoEb";

      function set_apodization (this : access Stream; specification : Interfaces.C.Strings.chars_ptr) return Extensions.bool  -- ../include/FLAC++/encoder.h:138
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream15set_apodizationEPKc";

      function set_max_lpc_order (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:139
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream17set_max_lpc_orderEj";

      function set_qlp_coeff_precision (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:140
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream23set_qlp_coeff_precisionEj";

      function set_do_qlp_coeff_prec_search (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:141
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream28set_do_qlp_coeff_prec_searchEb";

      function set_do_escape_coding (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream20set_do_escape_codingEb";

      function set_do_exhaustive_model_search (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:143
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream30set_do_exhaustive_model_searchEb";

      function set_min_residual_partition_order (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:144
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream32set_min_residual_partition_orderEj";

      function set_max_residual_partition_order (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:145
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream32set_max_residual_partition_orderEj";

      function set_rice_parameter_search_dist (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:146
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream30set_rice_parameter_search_distEj";

      function set_total_samples_estimate (this : access Stream; value : FLAC_ordinals_h.FLAC_u_uint64) return Extensions.bool  -- ../include/FLAC++/encoder.h:147
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream26set_total_samples_estimateEy";

      function set_metadata
        (this : access Stream;
         metadata : System.Address;
         num_blocks : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:148
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream12set_metadataEPP20FLAC__StreamMetadataj";

      function set_metadata
        (this : access Stream;
         metadata : System.Address;
         num_blocks : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:149
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream12set_metadataEPPNS_8Metadata9PrototypeEj";

      function set_limit_min_bitrate (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/encoder.h:150
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream21set_limit_min_bitrateEb";

      function set_num_threads (this : access Stream; value : utypes_uuint32_t_h.uint32_t) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:151
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream15set_num_threadsEj";

      function get_state (this : access constant Stream'Class) return State  -- ../include/FLAC++/encoder.h:154
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream9get_stateEv";

      function get_verify_decoder_state (this : access constant Stream) return FLACpp_decoder_h.Class_State.State  -- ../include/FLAC++/encoder.h:155
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream24get_verify_decoder_stateEv";

      procedure get_verify_decoder_error_stats
        (this : access Stream;
         absolute_sample : access Extensions.unsigned_long_long;
         frame_number : access unsigned;
         channel : access unsigned;
         sample : access unsigned;
         expected : access int;
         got : access int)  -- ../include/FLAC++/encoder.h:156
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream30get_verify_decoder_error_statsEPyPjS3_S3_PiS4_";

      function get_verify (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:157
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream10get_verifyEv";

      function get_streamable_subset (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:158
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream21get_streamable_subsetEv";

      function get_do_mid_side_stereo (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:159
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream22get_do_mid_side_stereoEv";

      function get_loose_mid_side_stereo (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:160
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream25get_loose_mid_side_stereoEv";

      function get_channels (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:161
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream12get_channelsEv";

      function get_bits_per_sample (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:162
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream19get_bits_per_sampleEv";

      function get_sample_rate (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:163
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream15get_sample_rateEv";

      function get_blocksize (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:164
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream13get_blocksizeEv";

      function get_max_lpc_order (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:165
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream17get_max_lpc_orderEv";

      function get_qlp_coeff_precision (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:166
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream23get_qlp_coeff_precisionEv";

      function get_do_qlp_coeff_prec_search (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:167
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream28get_do_qlp_coeff_prec_searchEv";

      function get_do_escape_coding (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:168
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream20get_do_escape_codingEv";

      function get_do_exhaustive_model_search (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:169
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream30get_do_exhaustive_model_searchEv";

      function get_min_residual_partition_order (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:170
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream32get_min_residual_partition_orderEv";

      function get_max_residual_partition_order (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:171
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream32get_max_residual_partition_orderEv";

      function get_rice_parameter_search_dist (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:172
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream30get_rice_parameter_search_distEv";

      function get_total_samples_estimate (this : access constant Stream) return FLAC_ordinals_h.FLAC_u_uint64  -- ../include/FLAC++/encoder.h:173
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream26get_total_samples_estimateEv";

      function get_limit_min_bitrate (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:174
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream21get_limit_min_bitrateEv";

      function get_num_threads (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/encoder.h:175
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Encoder6Stream15get_num_threadsEv";

      function init (this : access Stream) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:177
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream4initEv";

      function init_ogg (this : access Stream) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:178
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream8init_oggEv";

      function finish (this : access Stream) return Extensions.bool  -- ../include/FLAC++/encoder.h:180
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream6finishEv";

      function process
        (this : access Stream;
         buffer : System.Address;
         samples : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream7processEPKPKij";

      function process_interleaved
        (this : access Stream;
         buffer : access int;
         samples : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/encoder.h:183
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream19process_interleavedEPKij";

      function read_callback
        (this : access Stream;
         buffer : access unsigned_char;
         bytes : access unsigned_long) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderReadStatus  -- ../include/FLAC++/encoder.h:186
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream13read_callbackEPhPm";

      function write_callback
        (this : access Stream;
         buffer : access unsigned_char;
         bytes : sys_utypes_usize_t_h.size_t;
         samples : utypes_uuint32_t_h.uint32_t;
         current_frame : utypes_uuint32_t_h.uint32_t) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderWriteStatus is abstract;  -- ../include/FLAC++/encoder.h:189

      function seek_callback (this : access Stream; absolute_byte_offset : FLAC_ordinals_h.FLAC_u_uint64) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderSeekStatus  -- ../include/FLAC++/encoder.h:192
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream13seek_callbackEy";

      function tell_callback (this : access Stream; absolute_byte_offset : access Extensions.unsigned_long_long) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderTellStatus  -- ../include/FLAC++/encoder.h:195
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream13tell_callbackEPy";

      procedure metadata_callback (this : access Stream; metadata : access constant FLAC_format_h.FLAC_u_StreamMetadata)  -- ../include/FLAC++/encoder.h:198
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream17metadata_callbackEPK20FLAC__StreamMetadata";

      function read_callback_u
        (encoder : access constant FLAC_stream_encoder_h.FLAC_u_StreamEncoder;
         buffer : access unsigned_char;
         bytes : access unsigned_long;
         client_data : System.Address) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderReadStatus  -- ../include/FLAC++/encoder.h:206
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream14read_callback_EPK19FLAC__StreamEncoderPhPmPv";

      function write_callback_u
        (encoder : access constant FLAC_stream_encoder_h.FLAC_u_StreamEncoder;
         buffer : access unsigned_char;
         bytes : sys_utypes_usize_t_h.size_t;
         samples : utypes_uuint32_t_h.uint32_t;
         current_frame : utypes_uuint32_t_h.uint32_t;
         client_data : System.Address) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderWriteStatus  -- ../include/FLAC++/encoder.h:207
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream15write_callback_EPK19FLAC__StreamEncoderPKhmjjPv";

      function seek_callback_u
        (encoder : access constant FLAC_stream_encoder_h.FLAC_u_StreamEncoder;
         absolute_byte_offset : FLAC_ordinals_h.FLAC_u_uint64;
         client_data : System.Address) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderSeekStatus  -- ../include/FLAC++/encoder.h:208
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream14seek_callback_EPK19FLAC__StreamEncoderyPv";

      function tell_callback_u
        (encoder : access constant FLAC_stream_encoder_h.FLAC_u_StreamEncoder;
         absolute_byte_offset : access Extensions.unsigned_long_long;
         client_data : System.Address) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderTellStatus  -- ../include/FLAC++/encoder.h:209
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream14tell_callback_EPK19FLAC__StreamEncoderPyPv";

      procedure metadata_callback_u
        (encoder : access constant FLAC_stream_encoder_h.FLAC_u_StreamEncoder;
         metadata : access constant FLAC_format_h.FLAC_u_StreamMetadata;
         client_data : System.Address)  -- ../include/FLAC++/encoder.h:210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6Stream18metadata_callback_EPK19FLAC__StreamEncoderPK20FLAC__StreamMetadataPv";

      procedure Assign_Stream (this : access Stream'Class; arg2 : access constant Stream'Class)  -- ../include/FLAC++/encoder.h:214
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder6StreamaSERKS1_";
   end;
   use Class_Stream;
   package Class_File is
      type File is limited new Stream with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_File return File;  -- ../include/FLAC++/encoder.h:239
      pragma CPP_Constructor (New_File, "_ZN4FLAC7Encoder4FileC1Ev");

      procedure Delete_File (this : access File)  -- ../include/FLAC++/encoder.h:240
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4FileD1Ev";

      procedure Delete_And_Free_File (this : access File)  -- ../include/FLAC++/encoder.h:240
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4FileD0Ev";

      function init (this : access File; the_file : access ustdio_h.uu_sFILE) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:243
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File4initEP7__sFILE";

      function init (this : access File; filename : Interfaces.C.Strings.chars_ptr) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:244
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File4initEPKc";

      function init (this : access File; filename : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:245
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File4initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE";

      function init_ogg (this : access File; the_file : access ustdio_h.uu_sFILE) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:247
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File8init_oggEP7__sFILE";

      function init_ogg (this : access File; filename : Interfaces.C.Strings.chars_ptr) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:248
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File8init_oggEPKc";

      function init_ogg (this : access File; filename : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC++/encoder.h:249
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File8init_oggERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE";

      procedure progress_callback
        (this : access File;
         bytes_written : FLAC_ordinals_h.FLAC_u_uint64;
         samples_written : FLAC_ordinals_h.FLAC_u_uint64;
         frames_written : utypes_uuint32_t_h.uint32_t;
         total_frames_estimate : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/encoder.h:252
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File17progress_callbackEyyjj";

      function write_callback
        (this : access File;
         buffer : access unsigned_char;
         bytes : sys_utypes_usize_t_h.size_t;
         samples : utypes_uuint32_t_h.uint32_t;
         current_frame : utypes_uuint32_t_h.uint32_t) return FLAC_stream_encoder_h.FLAC_u_StreamEncoderWriteStatus  -- ../include/FLAC++/encoder.h:255
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File14write_callbackEPKhmjj";

      procedure progress_callback_u
        (encoder : access constant FLAC_stream_encoder_h.FLAC_u_StreamEncoder;
         bytes_written : FLAC_ordinals_h.FLAC_u_uint64;
         samples_written : FLAC_ordinals_h.FLAC_u_uint64;
         frames_written : utypes_uuint32_t_h.uint32_t;
         total_frames_estimate : utypes_uuint32_t_h.uint32_t;
         client_data : System.Address)  -- ../include/FLAC++/encoder.h:257
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4File18progress_callback_EPK19FLAC__StreamEncoderyyjjPv";

      function New_File (arg1 : access constant Stream) return File;  -- ../include/FLAC++/encoder.h:260
      pragma CPP_Constructor (New_File, "_ZN4FLAC7Encoder4FileC1ERKNS0_6StreamE");

      procedure Assign_File (this : access File'Class; arg2 : access constant Stream'Class)  -- ../include/FLAC++/encoder.h:261
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Encoder4FileaSERKNS0_6StreamE";
   end;
   use Class_File;
end FLACpp_encoder_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
