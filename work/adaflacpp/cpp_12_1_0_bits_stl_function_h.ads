pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
with Interfaces.C.Extensions;

package cpp_12_1_0_bits_stl_function_h is

   package binary_function_address_address_bool is
      type binary_function is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use binary_function_address_address_bool;



   type uu_is_transparent is null record;   -- incomplete struct

   package plus_address is
      type plus is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use plus_address;



   package minus_address is
      type minus is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use minus_address;



   package multiplies_address is
      type multiplies is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use multiplies_address;



   package divides_address is
      type divides is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use divides_address;



   package modulus_address is
      type modulus is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use modulus_address;



   package negate_address is
      type negate is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use negate_address;



   package equal_to_address is
      type equal_to is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use equal_to_address;



   package not_equal_to_address is
      type not_equal_to is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use not_equal_to_address;



   package greater_address is
      type greater is limited record
         parent : aliased binary_function;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op
        (this : access constant greater;
         uu_x : System.Address;
         uu_y : System.Address) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stl_function.h:436
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt7greaterIPVKvEclES1_S1_";

   end;
   use greater_address;

   package greater_address is
      type greater is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use greater_address;



   package less_address is
      type less is limited record
         parent : aliased binary_function;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op
        (this : access constant less;
         uu_x : System.Address;
         uu_y : System.Address) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stl_function.h:451
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt4lessIPVKvEclES1_S1_";

   end;
   use less_address;

   package less_address is
      type less is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use less_address;



   package greater_equal_address is
      type greater_equal is limited record
         parent : aliased binary_function;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op
        (this : access constant greater_equal;
         uu_x : System.Address;
         uu_y : System.Address) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stl_function.h:466
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt13greater_equalIPVKvEclES1_S1_";

   end;
   use greater_equal_address;

   package greater_equal_address is
      type greater_equal is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use greater_equal_address;



   package less_equal_address is
      type less_equal is limited record
         parent : aliased binary_function;
      end record
      with Import => True,
           Convention => CPP;

      function operator_op
        (this : access constant less_equal;
         uu_x : System.Address;
         uu_y : System.Address) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stl_function.h:481
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt10less_equalIPVKvEclES1_S1_";

   end;
   use less_equal_address;

   package less_equal_address is
      type less_equal is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use less_equal_address;



   package logical_and_address is
      type logical_and is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use logical_and_address;



   package logical_or_address is
      type logical_or is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use logical_or_address;



   package logical_not_address is
      type logical_not is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use logical_not_address;



   package bit_and_address is
      type bit_and is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use bit_and_address;



   package bit_or_address is
      type bit_or is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use bit_or_address;



   package bit_xor_address is
      type bit_xor is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use bit_xor_address;



   package bit_not_address is
      type bit_not is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use bit_not_address;



end cpp_12_1_0_bits_stl_function_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
