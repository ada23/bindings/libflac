pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_bits_basic_string_h;

package cpp_12_1_0_bits_stringfwd_h is

   subtype string is cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stringfwd.h:77

   subtype wstring is cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stringfwd.h:80

   subtype u16string is cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stringfwd.h:89

   subtype u32string is cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stringfwd.h:92

end cpp_12_1_0_bits_stringfwd_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
