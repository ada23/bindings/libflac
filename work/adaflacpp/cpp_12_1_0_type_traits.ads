pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package cpp_12_1_0_type_traits is

   package integral_constant_unsigned_long_2 is
      type integral_constant is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func __conv_op 

      function operator_op (this : access constant integral_constant) return value_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:72
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17integral_constantImLm2EEclEv";

      value : aliased constant unsigned_long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:64
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17integral_constantImLm2EE5valueE";

   end;
   use integral_constant_unsigned_long_2;

   package integral_constant_unsigned_long_0 is
      type integral_constant is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func __conv_op 

      function operator_op (this : access constant integral_constant) return value_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:72
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17integral_constantImLm0EEclEv";

      value : aliased constant unsigned_long  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:64
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17integral_constantImLm0EE5valueE";

   end;
   use integral_constant_unsigned_long_0;

   package integral_constant_bool_0 is
      type integral_constant is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func __conv_op 

      function operator_op (this : access constant integral_constant) return value_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:72
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17integral_constantIbLb0EEclEv";

      value : aliased constant Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:64
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17integral_constantIbLb0EE5valueE";

   end;
   use integral_constant_bool_0;

   package integral_constant_bool_1 is
      type integral_constant is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func __conv_op 

      function operator_op (this : access constant integral_constant) return value_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:72
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt17integral_constantIbLb1EEclEv";

      value : aliased constant Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:64
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt17integral_constantIbLb1EE5valueE";

   end;
   use integral_constant_bool_1;



   subtype true_type is integral_constant;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:82

   subtype false_type is integral_constant;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:85

   package uu_conditional_0 is
      type uu_conditional is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_conditional_0;



   package uu_type_identity_char32_t is
      type uu_type_identity is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_type_identity_char32_t;

   package uu_type_identity_char16_t is
      type uu_type_identity is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_type_identity_char16_t;

   package uu_type_identity_wchar_t is
      type uu_type_identity is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_type_identity_wchar_t;

   package uu_type_identity_char is
      type uu_type_identity is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_type_identity_char;



   package uu_or_u_unknown163840 is
      type uu_or_u is limited record
         parent : aliased is_array;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown163840;

   package uu_or_u_unknown112920 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown112920;

   package uu_or_u_unknown206184 is
      type uu_or_u is limited record
         parent : aliased uu_is_array_unknown_bounds;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown206184;

   package uu_or_u_unknown204000 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown204000;

   package uu_or_u_unknown201144 is
      type uu_or_u is limited record
         parent : aliased is_rvalue_reference;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown201144;

   package uu_or_u_unknown199800 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown199800;

   package uu_or_u_unknown8736 is
      type uu_or_u is limited record
         parent : aliased uu_is_array_unknown_bounds;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown8736;

   package uu_or_u_unknown6552 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown6552;

   package uu_or_u_unknown3696 is
      type uu_or_u is limited record
         parent : aliased is_rvalue_reference;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown3696;

   package uu_or_u_unknown2352 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown2352;

   package uu_or_u_unknown57048 is
      type uu_or_u is limited record
         parent : aliased uu_is_array_unknown_bounds;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown57048;

   package uu_or_u_unknown54864 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown54864;

   package uu_or_u_unknown52008 is
      type uu_or_u is limited record
         parent : aliased is_rvalue_reference;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown52008;

   package uu_or_u_unknown50664 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown50664;

   package uu_or_u_unknown102672 is
      type uu_or_u is limited record
         parent : aliased uu_is_array_unknown_bounds;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown102672;

   package uu_or_u_unknown100320 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown100320;

   package uu_or_u_unknown4872 is
      type uu_or_u is limited record
         parent : aliased is_rvalue_reference;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown4872;

   package uu_or_u_unknown504 is
      type uu_or_u is limited record
         parent : aliased uu_or_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown504;

   package uu_or_u_unknown237776 is
      type uu_or_u is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_or_u_unknown237776;



   package uu_and_u_unknown175096 is
      type uu_and_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown175096;

   package uu_and_u_unknown111408 is
      type uu_and_u is limited record
         parent : aliased is_convertible;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown111408;

   package uu_and_u_unknown208872 is
      type uu_and_u is limited record
         parent : aliased is_array;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown208872;

   package uu_and_u_unknown11424 is
      type uu_and_u is limited record
         parent : aliased is_array;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown11424;

   package uu_and_u_unknown59736 is
      type uu_and_u is limited record
         parent : aliased is_array;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown59736;

   package uu_and_u_unknown106032 is
      type uu_and_u is limited record
         parent : aliased is_array;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown106032;

   package uu_and_u_unknown17056 is
      type uu_and_u is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_and_u_unknown17056;



   package uu_not_u_is_convertible is
      type uu_not_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_not_u_is_convertible;

   package uu_not_u_is_convertible is
      type uu_not_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_not_u_is_convertible;

   package uu_not_u_extent is
      type uu_not_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_not_u_extent;

   package uu_not_u_extent is
      type uu_not_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_not_u_extent;

   package uu_not_u_extent is
      type uu_not_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_not_u_extent;

   package uu_not_u_extent is
      type uu_not_u is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_not_u_extent;



   package is_reference_char32_t is
      type is_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_reference_char32_t;

   package is_reference_char16_t is
      type is_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_reference_char16_t;

   package is_reference_wchar_t is
      type is_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_reference_wchar_t;

   package is_reference_char is
      type is_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_reference_char;



   package is_function_Class_basic_string_view.basic_string_view is
      type is_function is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_function_Class_basic_string_view.basic_string_view;

   package is_function_char32_t is
      type is_function is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_function_char32_t;

   package is_function_char16_t is
      type is_function is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_function_char16_t;

   package is_function_wchar_t is
      type is_function is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_function_wchar_t;

   package is_function_char is
      type is_function is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_function_char;



   package is_void_accessunsigned is
      type is_void is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_void_accessunsigned;

   package is_void_char32_t is
      type is_void is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_void_char32_t;

   package is_void_char16_t is
      type is_void is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_void_char16_t;

   package is_void_wchar_t is
      type is_void is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_void_wchar_t;

   package is_void_char is
      type is_void is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_void_char;

   package is_void_address is
      type is_void is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_void_address;



   package remove_cv_accessunsigned is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_accessunsigned;

   package remove_cv_char is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_char;

   package remove_cv_address is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_address;

   package remove_cv_char32_t is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_char32_t;

   package remove_cv_unsigned_short is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_unsigned_short;

   package remove_cv_char16_t is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_char16_t;

   package remove_cv_unsigned is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_unsigned;

   package remove_cv_wchar_t is
      type remove_cv is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use remove_cv_wchar_t;



   package is_const_Class_basic_string_view.basic_string_view is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_Class_basic_string_view.basic_string_view;

   package is_const_char32_t is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_char32_t;

   package is_const_char16_t is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_char16_t;

   package is_const_wchar_t is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_wchar_t;

   package is_const_char is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_char;

   package is_const_unsigned_short is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_unsigned_short;

   package is_const_unsigned is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_unsigned;

   package is_const_char32_t is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_char32_t;

   package is_const_char16_t is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_char16_t;

   package is_const_wchar_t is
      type is_const is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_const_wchar_t;



   package uu_is_array_unknown_bounds_char32_t is
      type uu_is_array_unknown_bounds is limited record
         parent : aliased uu_and_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_array_unknown_bounds_char32_t;

   package uu_is_array_unknown_bounds_char16_t is
      type uu_is_array_unknown_bounds is limited record
         parent : aliased uu_and_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_array_unknown_bounds_char16_t;

   package uu_is_array_unknown_bounds_wchar_t is
      type uu_is_array_unknown_bounds is limited record
         parent : aliased uu_and_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_array_unknown_bounds_wchar_t;

   package uu_is_array_unknown_bounds_char is
      type uu_is_array_unknown_bounds is limited record
         parent : aliased uu_and_u;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_array_unknown_bounds_char;



   type uu_failure_type is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:265

   package uu_is_void_helper_accessunsigned is
      type uu_is_void_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_void_helper_accessunsigned;

   package uu_is_void_helper_char32_t is
      type uu_is_void_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_void_helper_char32_t;

   package uu_is_void_helper_char16_t is
      type uu_is_void_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_void_helper_char16_t;

   package uu_is_void_helper_wchar_t is
      type uu_is_void_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_void_helper_wchar_t;

   package uu_is_void_helper_char is
      type uu_is_void_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_void_helper_char;

   package uu_is_void_helper_address is
      type uu_is_void_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_void_helper_address;



   package uu_is_integral_helper_uu_int128_unsigned is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_uu_int128_unsigned;

   package uu_is_integral_helper_Extensions.Signed_128 is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_Extensions.Signed_128;

   package uu_is_integral_helper_unsigned_long_long is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_unsigned_long_long;

   package uu_is_integral_helper_Long_Long_Integer is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_Long_Long_Integer;

   package uu_is_integral_helper_unsigned_long is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_unsigned_long;

   package uu_is_integral_helper_long is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_long;

   package uu_is_integral_helper_unsigned is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_unsigned;

   package uu_is_integral_helper_int is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_int;

   package uu_is_integral_helper_unsigned_short is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_unsigned_short;

   package uu_is_integral_helper_short is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_short;

   package uu_is_integral_helper_char32_t is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_char32_t;

   package uu_is_integral_helper_char16_t is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_char16_t;

   package uu_is_integral_helper_wchar_t is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_wchar_t;

   package uu_is_integral_helper_unsigned_char is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_unsigned_char;

   package uu_is_integral_helper_signed_char is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_signed_char;

   package uu_is_integral_helper_char is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_char;

   package uu_is_integral_helper_bool is
      type uu_is_integral_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_integral_helper_bool;



   package is_integral_char32_t is
      type is_integral is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_integral_char32_t;

   package is_integral_unsigned_short is
      type is_integral is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_integral_unsigned_short;

   package is_integral_char16_t is
      type is_integral is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_integral_char16_t;

   package is_integral_unsigned is
      type is_integral is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_integral_unsigned;

   package is_integral_wchar_t is
      type is_integral is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_integral_wchar_t;



   package uu_is_floating_point_helper_long_double is
      type uu_is_floating_point_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_floating_point_helper_long_double;

   package uu_is_floating_point_helper_double is
      type uu_is_floating_point_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_floating_point_helper_double;

   package uu_is_floating_point_helper_float is
      type uu_is_floating_point_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_floating_point_helper_float;



   package is_array_Class_basic_string_view.basic_string_view is
      type is_array is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_array_Class_basic_string_view.basic_string_view;

   package is_array_char32_t is
      type is_array is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_array_char32_t;

   package is_array_char16_t is
      type is_array is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_array_char16_t;

   package is_array_wchar_t is
      type is_array is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_array_wchar_t;

   package is_array_char is
      type is_array is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_array_char;



   package is_lvalue_reference_char32_t is
      type is_lvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_lvalue_reference_char32_t;

   package is_lvalue_reference_char16_t is
      type is_lvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_lvalue_reference_char16_t;

   package is_lvalue_reference_wchar_t is
      type is_lvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_lvalue_reference_wchar_t;

   package is_lvalue_reference_char is
      type is_lvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_lvalue_reference_char;



   package is_rvalue_reference_char32_t is
      type is_rvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_rvalue_reference_char32_t;

   package is_rvalue_reference_char16_t is
      type is_rvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_rvalue_reference_char16_t;

   package is_rvalue_reference_wchar_t is
      type is_rvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_rvalue_reference_wchar_t;

   package is_rvalue_reference_char is
      type is_rvalue_reference is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_rvalue_reference_char;



   package is_enum_char32_t is
      type is_enum is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_enum_char32_t;

   package is_enum_unsigned_short is
      type is_enum is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_enum_unsigned_short;

   package is_enum_char16_t is
      type is_enum is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_enum_char16_t;

   package is_enum_unsigned is
      type is_enum is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_enum_unsigned;

   package is_enum_wchar_t is
      type is_enum is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_enum_wchar_t;



   package uu_is_null_pointer_helper_address is
      type uu_is_null_pointer_helper is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_null_pointer_helper_address;



   package is_volatile_unsigned_short is
      type is_volatile is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_volatile_unsigned_short;

   package is_volatile_unsigned is
      type is_volatile is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_volatile_unsigned;

   package is_volatile_char32_t is
      type is_volatile is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_volatile_char32_t;

   package is_volatile_char16_t is
      type is_volatile is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_volatile_char16_t;

   package is_volatile_wchar_t is
      type is_volatile is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_volatile_wchar_t;



   package is_trivial_char32_t is
      type is_trivial is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_trivial_char32_t;

   package is_trivial_char16_t is
      type is_trivial is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_trivial_char16_t;

   package is_trivial_wchar_t is
      type is_trivial is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_trivial_wchar_t;

   package is_trivial_char is
      type is_trivial is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_trivial_char;



   package is_standard_layout_char32_t is
      type is_standard_layout is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_standard_layout_char32_t;

   package is_standard_layout_char16_t is
      type is_standard_layout is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_standard_layout_char16_t;

   package is_standard_layout_wchar_t is
      type is_standard_layout is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_standard_layout_wchar_t;

   package is_standard_layout_char is
      type is_standard_layout is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_standard_layout_char;



   package extent_char32_t_0 is
      type extent is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use extent_char32_t_0;

   package extent_char16_t_0 is
      type extent is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use extent_char16_t_0;

   package extent_wchar_t_0 is
      type extent is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use extent_wchar_t_0;

   package extent_char_0 is
      type extent is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use extent_char_0;



   type uu_do_is_destructible_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:867

   type uu_do_is_nt_destructible_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:920

   package Class_uu_do_is_implicitly_default_constructible_impl is
      type uu_do_is_implicitly_default_constructible_impl is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func __test
   end;
   use Class_uu_do_is_implicitly_default_constructible_impl;
   package uu_is_convertible_helper_accessunsigned_Class_basic_string_view.basic_string_view_0 is
      type uu_is_convertible_helper is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_convertible_helper_accessunsigned_Class_basic_string_view.basic_string_view_0;



   package is_convertible_accessunsigned_chars_ptr is
      type is_convertible is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_convertible_accessunsigned_chars_ptr;

   package is_convertible_accessunsigned_accessClass_basic_string.basic_string is
      type is_convertible is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_convertible_accessunsigned_accessClass_basic_string.basic_string;

   package is_convertible_accessunsigned_Class_basic_string_view.basic_string_view is
      type is_convertible is limited record
         parent : aliased integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use is_convertible_accessunsigned_Class_basic_string_view.basic_string_view;



   package uu_cv_selector_short_0_0 is
      type uu_cv_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_cv_selector_short_0_0;

   package uu_cv_selector_int_0_0 is
      type uu_cv_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_cv_selector_int_0_0;

   package uu_cv_selector_unsigned_short_0_0 is
      type uu_cv_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_cv_selector_unsigned_short_0_0;

   package uu_cv_selector_unsigned_0_0 is
      type uu_cv_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_cv_selector_unsigned_0_0;



   package uu_match_cv_qualifiers_unsigned_short_short_0_0 is
      type uu_match_cv_qualifiers is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_match_cv_qualifiers_unsigned_short_short_0_0;

   package uu_match_cv_qualifiers_unsigned_int_0_0 is
      type uu_match_cv_qualifiers is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_match_cv_qualifiers_unsigned_int_0_0;

   package uu_match_cv_qualifiers_char32_t_unsigned_0_0 is
      type uu_match_cv_qualifiers is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_match_cv_qualifiers_char32_t_unsigned_0_0;

   package uu_match_cv_qualifiers_char16_t_unsigned_short_0_0 is
      type uu_match_cv_qualifiers is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_match_cv_qualifiers_char16_t_unsigned_short_0_0;

   package uu_match_cv_qualifiers_wchar_t_unsigned_0_0 is
      type uu_match_cv_qualifiers is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_match_cv_qualifiers_wchar_t_unsigned_0_0;



   package uu_make_unsigned_char32_t is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_char32_t;

   package uu_make_unsigned_char16_t is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_char16_t;

   package uu_make_unsigned_wchar_t is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_wchar_t;

   package uu_make_unsigned_Extensions.Signed_128 is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_Extensions.Signed_128;

   package uu_make_unsigned_Long_Long_Integer is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_Long_Long_Integer;

   package uu_make_unsigned_long is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_long;

   package uu_make_unsigned_int is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_int;

   package uu_make_unsigned_short is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_short;

   package uu_make_unsigned_signed_char is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_signed_char;

   package uu_make_unsigned_char is
      type uu_make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_char;



   package uu_make_unsigned_selector_char32_t_1_0 is
      type uu_make_unsigned_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_selector_char32_t_1_0;

   package uu_make_unsigned_selector_char16_t_1_0 is
      type uu_make_unsigned_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_selector_char16_t_1_0;

   package uu_make_unsigned_selector_wchar_t_1_0 is
      type uu_make_unsigned_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_selector_wchar_t_1_0;

   package uu_make_unsigned_selector_char32_t_0_1 is
      type uu_make_unsigned_selector is limited record
         parent : aliased uu_make_unsigned_selector_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_selector_char32_t_0_1;

   package uu_make_unsigned_selector_char16_t_0_1 is
      type uu_make_unsigned_selector is limited record
         parent : aliased uu_make_unsigned_selector_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_selector_char16_t_0_1;

   package uu_make_unsigned_selector_wchar_t_0_1 is
      type uu_make_unsigned_selector is limited record
         parent : aliased uu_make_unsigned_selector_base;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_unsigned_selector_wchar_t_0_1;



   type uu_make_unsigned_selector_base is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:1787

   package make_unsigned_bool is
      type make_unsigned is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use make_unsigned_bool;



   package uu_make_signed_char32_t is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_char32_t;

   package uu_make_signed_char16_t is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_char16_t;

   package uu_make_signed_wchar_t is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_wchar_t;

   package uu_make_signed_uu_int128_unsigned is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_uu_int128_unsigned;

   package uu_make_signed_unsigned_long_long is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_unsigned_long_long;

   package uu_make_signed_unsigned_long is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_unsigned_long;

   package uu_make_signed_unsigned is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_unsigned;

   package uu_make_signed_unsigned_short is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_unsigned_short;

   package uu_make_signed_unsigned_char is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_unsigned_char;

   package uu_make_signed_char is
      type uu_make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_char;



   package uu_make_signed_selector_char32_t_0_1 is
      type uu_make_signed_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_selector_char32_t_0_1;

   package uu_make_signed_selector_unsigned_short_1_0 is
      type uu_make_signed_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_selector_unsigned_short_1_0;

   package uu_make_signed_selector_char16_t_0_1 is
      type uu_make_signed_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_selector_char16_t_0_1;

   package uu_make_signed_selector_unsigned_1_0 is
      type uu_make_signed_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_selector_unsigned_1_0;

   package uu_make_signed_selector_wchar_t_0_1 is
      type uu_make_signed_selector is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_make_signed_selector_wchar_t_0_1;



   package make_signed_bool is
      type make_signed is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use make_signed_bool;



   package enable_if_1_Class_allocator.allocator is
      type enable_if is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use enable_if_1_Class_allocator.allocator;

   package enable_if_1_Class_allocator.allocator is
      type enable_if is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use enable_if_1_Class_allocator.allocator;

   package enable_if_1_Class_allocator.allocator is
      type enable_if is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use enable_if_1_Class_allocator.allocator;

   package enable_if_0_address is
      type enable_if is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use enable_if_0_address;

   package enable_if_1_Class_allocator.allocator is
      type enable_if is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use enable_if_1_Class_allocator.allocator;



   package common_type_unknown66712 is
      type common_type is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use common_type_unknown66712;



   type uu_do_common_type_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2270

   type uu_invoke_memfun_ref is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2409

   type uu_invoke_memfun_deref is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2410

   type uu_invoke_memobj_ref is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2411

   type uu_invoke_memobj_deref is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2412

   type uu_invoke_other is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2413

   type uu_result_of_memfun_ref_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2421

   type uu_result_of_memfun_deref_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2440

   type uu_result_of_memobj_ref_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2459

   type uu_result_of_memobj_deref_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2478

   type uu_result_of_other_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2561

   type uu_do_is_swappable_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2722

   type uu_do_is_nothrow_swappable_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2732

   type uu_do_is_swappable_with_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2808

   type uu_do_is_nothrow_swappable_with_impl is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:2820

   type uu_nonesuchbase is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:3008

   package Class_uu_nonesuch is
      type uu_nonesuch is limited record
         parent : aliased uu_nonesuchbase;
      end record
      with Import => True,
           Convention => CPP;

      procedure Delete_uu_nonesuch (this : access uu_nonesuch)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:3010
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt10__nonesuchD1Ev";

      procedure Assign_uu_nonesuch (this : access uu_nonesuch; arg2 : access constant uu_nonesuch)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/type_traits:3012
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt10__nonesuchaSERKS_";
   end;
   use Class_uu_nonesuch;
end cpp_12_1_0_type_traits;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
