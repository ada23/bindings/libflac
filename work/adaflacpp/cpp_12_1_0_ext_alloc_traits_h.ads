pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_bits_alloc_traits_h;

package cpp_12_1_0_ext_alloc_traits_h is

   package uu_alloc_traits_Class_allocator.allocator_char32_t is
      type uu_alloc_traits is limited record
         parent : aliased cpp_12_1_0_bits_alloc_traits_h.Class_allocator_traits.allocator_traits;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func _S_select_on_copy

      --  skipped func _S_on_swap

      --  skipped func _S_propagate_on_copy_assign

      --  skipped func _S_propagate_on_move_assign

      --  skipped func _S_propagate_on_swap

      --  skipped func _S_always_equal

      --  skipped func _S_nothrow_move

   end;
   use uu_alloc_traits_Class_allocator.allocator_char32_t;

   package uu_alloc_traits_Class_allocator.allocator_char16_t is
      type uu_alloc_traits is limited record
         parent : aliased cpp_12_1_0_bits_alloc_traits_h.Class_allocator_traits.allocator_traits;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func _S_select_on_copy

      --  skipped func _S_on_swap

      --  skipped func _S_propagate_on_copy_assign

      --  skipped func _S_propagate_on_move_assign

      --  skipped func _S_propagate_on_swap

      --  skipped func _S_always_equal

      --  skipped func _S_nothrow_move

   end;
   use uu_alloc_traits_Class_allocator.allocator_char16_t;

   package uu_alloc_traits_Class_allocator.allocator_wchar_t is
      type uu_alloc_traits is limited record
         parent : aliased cpp_12_1_0_bits_alloc_traits_h.Class_allocator_traits.allocator_traits;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func _S_select_on_copy

      --  skipped func _S_on_swap

      --  skipped func _S_propagate_on_copy_assign

      --  skipped func _S_propagate_on_move_assign

      --  skipped func _S_propagate_on_swap

      --  skipped func _S_always_equal

      --  skipped func _S_nothrow_move

   end;
   use uu_alloc_traits_Class_allocator.allocator_wchar_t;

   package uu_alloc_traits_Class_allocator.allocator_char is
      type uu_alloc_traits is limited record
         parent : aliased cpp_12_1_0_bits_alloc_traits_h.Class_allocator_traits.allocator_traits;
      end record
      with Import => True,
           Convention => CPP;

      --  skipped func _S_select_on_copy

      --  skipped func _S_on_swap

      --  skipped func _S_propagate_on_copy_assign

      --  skipped func _S_propagate_on_move_assign

      --  skipped func _S_propagate_on_swap

      --  skipped func _S_always_equal

      --  skipped func _S_nothrow_move

   end;
   use uu_alloc_traits_Class_allocator.allocator_char;



end cpp_12_1_0_ext_alloc_traits_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
