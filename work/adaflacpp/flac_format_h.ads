pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with FLAC_ordinals_h;
with utypes_uuint32_t_h;
with Interfaces.C.Extensions;
with System;

package FLAC_format_h is

   FLAC_u_MAX_METADATA_TYPE_CODE : constant := (126);  --  ../include/FLAC/format.h:93

   FLAC_u_MIN_BLOCK_SIZE : constant := (16);  --  ../include/FLAC/format.h:96

   FLAC_u_MAX_BLOCK_SIZE : constant := (65535);  --  ../include/FLAC/format.h:99

   FLAC_u_SUBSET_MAX_BLOCK_SIZE_48000HZ : constant := (4608);  --  ../include/FLAC/format.h:103

   FLAC_u_MAX_CHANNELS : constant := (8);  --  ../include/FLAC/format.h:106

   FLAC_u_MIN_BITS_PER_SAMPLE : constant := (4);  --  ../include/FLAC/format.h:109

   FLAC_u_MAX_BITS_PER_SAMPLE : constant := (32);  --  ../include/FLAC/format.h:112

   FLAC_u_REFERENCE_CODEC_MAX_BITS_PER_SAMPLE : constant := (32);  --  ../include/FLAC/format.h:120

   FLAC_u_MAX_SAMPLE_RATE : constant := (1048575);  --  ../include/FLAC/format.h:125

   FLAC_u_MAX_LPC_ORDER : constant := (32);  --  ../include/FLAC/format.h:128

   FLAC_u_SUBSET_MAX_LPC_ORDER_48000HZ : constant := (12);  --  ../include/FLAC/format.h:132

   FLAC_u_MIN_QLP_COEFF_PRECISION : constant := (5);  --  ../include/FLAC/format.h:137

   FLAC_u_MAX_QLP_COEFF_PRECISION : constant := (15);  --  ../include/FLAC/format.h:142

   FLAC_u_MAX_FIXED_ORDER : constant := (4);  --  ../include/FLAC/format.h:145

   FLAC_u_MAX_RICE_PARTITION_ORDER : constant := (15);  --  ../include/FLAC/format.h:148

   FLAC_u_SUBSET_MAX_RICE_PARTITION_ORDER : constant := (8);  --  ../include/FLAC/format.h:151

   FLAC_u_STREAM_SYNC_LENGTH : constant := (4);  --  ../include/FLAC/format.h:179

   FLAC_u_STREAM_METADATA_STREAMINFO_LENGTH : constant := (34);  --  ../include/FLAC/format.h:557

   FLAC_u_STREAM_METADATA_SEEKPOINT_LENGTH : constant := (18);  --  ../include/FLAC/format.h:598

   FLAC_u_STREAM_METADATA_HEADER_LENGTH : constant := (4);  --  ../include/FLAC/format.h:872

   FLAC_u_VERSION_STRING : Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:159
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__VERSION_STRING";

   FLAC_u_VENDOR_STRING : Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:165
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__VENDOR_STRING";

   FLAC_u_STREAM_SYNC_STRING : aliased array (0 .. 3) of aliased FLAC_ordinals_h.FLAC_u_byte  -- ../include/FLAC/format.h:168
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_SYNC_STRING";

   FLAC_u_STREAM_SYNC : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:173
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_SYNC";

   FLAC_u_STREAM_SYNC_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:176
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_SYNC_LEN";

   type FLAC_u_EntropyCodingMethodType is 
     (FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE,
      FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE2)
   with Convention => C;  -- ../include/FLAC/format.h:199

   FLAC_u_EntropyCodingMethodTypeString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:206
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__EntropyCodingMethodTypeString";

   type FLAC_u_EntropyCodingMethod_PartitionedRiceContents is record
      parameters : access unsigned;  -- ../include/FLAC/format.h:213
      raw_bits : access unsigned;  -- ../include/FLAC/format.h:216
      capacity_by_order : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:221
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:226

   type FLAC_u_EntropyCodingMethod_PartitionedRice is record
      order : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:232
      contents : access constant FLAC_u_EntropyCodingMethod_PartitionedRiceContents;  -- ../include/FLAC/format.h:235
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:238

   FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE_ORDER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:240
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE_ORDER_LEN";

   FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE_PARAMETER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:241
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE_PARAMETER_LEN";

   FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE2_PARAMETER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:242
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE2_PARAMETER_LEN";

   FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE_RAW_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:243
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE_RAW_LEN";

   FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE_ESCAPE_PARAMETER : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:245
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE_ESCAPE_PARAMETER";

   FLAC_u_ENTROPY_CODING_METHOD_PARTITIONED_RICE2_ESCAPE_PARAMETER : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:247
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_PARTITIONED_RICE2_ESCAPE_PARAMETER";

   type anon_anon_13 (discr : unsigned := 0) is record
      case discr is
         when others =>
            partitioned_rice : aliased FLAC_u_EntropyCodingMethod_PartitionedRice;  -- ../include/FLAC/format.h:255
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type FLAC_u_EntropyCodingMethod is record
      c_type : aliased FLAC_u_EntropyCodingMethodType;  -- ../include/FLAC/format.h:253
      data : aliased anon_anon_13;  -- ../include/FLAC/format.h:256
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:257

   FLAC_u_ENTROPY_CODING_METHOD_TYPE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:259
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ENTROPY_CODING_METHOD_TYPE_LEN";

   type FLAC_u_SubframeType is 
     (FLAC_u_SUBFRAME_TYPE_CONSTANT,
      FLAC_u_SUBFRAME_TYPE_VERBATIM,
      FLAC_u_SUBFRAME_TYPE_FIXED,
      FLAC_u_SUBFRAME_TYPE_LPC)
   with Convention => C;  -- ../include/FLAC/format.h:269

   FLAC_u_SubframeTypeString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:276
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SubframeTypeString";

   type FLAC_u_Subframe_Constant is record
      value : aliased FLAC_ordinals_h.FLAC_u_int64;  -- ../include/FLAC/format.h:282
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:283

   type FLAC_u_VerbatimSubframeDataType is 
     (FLAC_u_VERBATIM_SUBFRAME_DATA_TYPE_INT32,
      FLAC_u_VERBATIM_SUBFRAME_DATA_TYPE_INT64)
   with Convention => C;  -- ../include/FLAC/format.h:289

   type anon_anon_18 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            int32 : access int;  -- ../include/FLAC/format.h:296
         when others =>
            int64 : access Long_Long_Integer;  -- ../include/FLAC/format.h:297
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type FLAC_u_Subframe_Verbatim is record
      data : aliased anon_anon_18;  -- ../include/FLAC/format.h:298
      data_type : aliased FLAC_u_VerbatimSubframeDataType;  -- ../include/FLAC/format.h:299
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:300

   type anon_array2177 is array (0 .. 3) of aliased FLAC_ordinals_h.FLAC_u_int64;
   type FLAC_u_Subframe_Fixed is record
      entropy_coding_method : aliased FLAC_u_EntropyCodingMethod;  -- ../include/FLAC/format.h:306
      order : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:309
      warmup : aliased anon_array2177;  -- ../include/FLAC/format.h:312
      residual : access int;  -- ../include/FLAC/format.h:315
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:317

   type anon_array2180 is array (0 .. 31) of aliased FLAC_ordinals_h.FLAC_u_int32;
   type anon_array2182 is array (0 .. 31) of aliased FLAC_ordinals_h.FLAC_u_int64;
   type FLAC_u_Subframe_LPC is record
      entropy_coding_method : aliased FLAC_u_EntropyCodingMethod;  -- ../include/FLAC/format.h:323
      order : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:326
      qlp_coeff_precision : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:329
      quantization_level : aliased int;  -- ../include/FLAC/format.h:332
      qlp_coeff : aliased anon_array2180;  -- ../include/FLAC/format.h:335
      warmup : aliased anon_array2182;  -- ../include/FLAC/format.h:338
      residual : access int;  -- ../include/FLAC/format.h:341
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:343

   FLAC_u_SUBFRAME_LPC_QLP_COEFF_PRECISION_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:345
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_LPC_QLP_COEFF_PRECISION_LEN";

   FLAC_u_SUBFRAME_LPC_QLP_SHIFT_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:346
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_LPC_QLP_SHIFT_LEN";

   type anon_anon_22 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            c_constant : aliased FLAC_u_Subframe_Constant;  -- ../include/FLAC/format.h:354
         when 1 =>
            fixed : aliased FLAC_u_Subframe_Fixed;  -- ../include/FLAC/format.h:355
         when 2 =>
            lpc : aliased FLAC_u_Subframe_LPC;  -- ../include/FLAC/format.h:356
         when others =>
            verbatim : aliased FLAC_u_Subframe_Verbatim;  -- ../include/FLAC/format.h:357
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type FLAC_u_Subframe is record
      c_type : aliased FLAC_u_SubframeType;  -- ../include/FLAC/format.h:352
      data : aliased anon_anon_22;  -- ../include/FLAC/format.h:358
      wasted_bits : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:359
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:360

   FLAC_u_SUBFRAME_ZERO_PAD_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:369
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_ZERO_PAD_LEN";

   FLAC_u_SUBFRAME_TYPE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:370
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_TYPE_LEN";

   FLAC_u_SUBFRAME_WASTED_BITS_FLAG_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:371
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_WASTED_BITS_FLAG_LEN";

   FLAC_u_SUBFRAME_TYPE_CONSTANT_BYTE_ALIGNED_MASK : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:373
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_TYPE_CONSTANT_BYTE_ALIGNED_MASK";

   FLAC_u_SUBFRAME_TYPE_VERBATIM_BYTE_ALIGNED_MASK : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:374
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_TYPE_VERBATIM_BYTE_ALIGNED_MASK";

   FLAC_u_SUBFRAME_TYPE_FIXED_BYTE_ALIGNED_MASK : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:375
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_TYPE_FIXED_BYTE_ALIGNED_MASK";

   FLAC_u_SUBFRAME_TYPE_LPC_BYTE_ALIGNED_MASK : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:376
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__SUBFRAME_TYPE_LPC_BYTE_ALIGNED_MASK";

   type FLAC_u_ChannelAssignment is 
     (FLAC_u_CHANNEL_ASSIGNMENT_INDEPENDENT,
      FLAC_u_CHANNEL_ASSIGNMENT_LEFT_SIDE,
      FLAC_u_CHANNEL_ASSIGNMENT_RIGHT_SIDE,
      FLAC_u_CHANNEL_ASSIGNMENT_MID_SIDE)
   with Convention => C;  -- ../include/FLAC/format.h:393

   FLAC_u_ChannelAssignmentString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:400
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__ChannelAssignmentString";

   type FLAC_u_FrameNumberType is 
     (FLAC_u_FRAME_NUMBER_TYPE_FRAME_NUMBER,
      FLAC_u_FRAME_NUMBER_TYPE_SAMPLE_NUMBER)
   with Convention => C;  -- ../include/FLAC/format.h:406

   FLAC_u_FrameNumberTypeString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:413
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FrameNumberTypeString";

   type anon_anon_26 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            frame_number : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:440
         when others =>
            sample_number : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:441
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type FLAC_u_FrameHeader is record
      blocksize : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:419
      sample_rate : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:422
      channels : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:425
      channel_assignment : aliased FLAC_u_ChannelAssignment;  -- ../include/FLAC/format.h:428
      bits_per_sample : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:431
      number_type : aliased FLAC_u_FrameNumberType;  -- ../include/FLAC/format.h:434
      number : aliased anon_anon_26;  -- ../include/FLAC/format.h:442
      crc : aliased FLAC_ordinals_h.FLAC_u_uint8;  -- ../include/FLAC/format.h:446
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:451

   FLAC_u_FRAME_HEADER_SYNC : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:453
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_SYNC";

   FLAC_u_FRAME_HEADER_SYNC_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:454
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_SYNC_LEN";

   FLAC_u_FRAME_HEADER_RESERVED_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:455
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_RESERVED_LEN";

   FLAC_u_FRAME_HEADER_BLOCKING_STRATEGY_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:456
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_BLOCKING_STRATEGY_LEN";

   FLAC_u_FRAME_HEADER_BLOCK_SIZE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:457
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_BLOCK_SIZE_LEN";

   FLAC_u_FRAME_HEADER_SAMPLE_RATE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:458
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_SAMPLE_RATE_LEN";

   FLAC_u_FRAME_HEADER_CHANNEL_ASSIGNMENT_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:459
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_CHANNEL_ASSIGNMENT_LEN";

   FLAC_u_FRAME_HEADER_BITS_PER_SAMPLE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:460
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_BITS_PER_SAMPLE_LEN";

   FLAC_u_FRAME_HEADER_ZERO_PAD_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:461
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_ZERO_PAD_LEN";

   FLAC_u_FRAME_HEADER_CRC_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:462
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_HEADER_CRC_LEN";

   type FLAC_u_FrameFooter is record
      crc : aliased FLAC_ordinals_h.FLAC_u_uint16;  -- ../include/FLAC/format.h:468
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:473

   FLAC_u_FRAME_FOOTER_CRC_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:475
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__FRAME_FOOTER_CRC_LEN";

   type anon_array2199 is array (0 .. 7) of aliased FLAC_u_Subframe;
   type FLAC_u_Frame is record
      header : aliased FLAC_u_FrameHeader;  -- ../include/FLAC/format.h:481
      subframes : aliased anon_array2199;  -- ../include/FLAC/format.h:482
      footer : aliased FLAC_u_FrameFooter;  -- ../include/FLAC/format.h:483
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:484

   subtype FLAC_u_MetadataType is unsigned;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_STREAMINFO : constant FLAC_u_MetadataType := 0;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_PADDING : constant FLAC_u_MetadataType := 1;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_APPLICATION : constant FLAC_u_MetadataType := 2;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_SEEKTABLE : constant FLAC_u_MetadataType := 3;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_VORBIS_COMMENT : constant FLAC_u_MetadataType := 4;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_CUESHEET : constant FLAC_u_MetadataType := 5;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_PICTURE : constant FLAC_u_MetadataType := 6;
   FLAC_u_MetadataType_FLAC_u_METADATA_TYPE_UNDEFINED : constant FLAC_u_MetadataType := 7;
   FLAC_u_MetadataType_FLAC_u_MAX_METADATA_TYPE : constant FLAC_u_MetadataType := 126;  -- ../include/FLAC/format.h:524

   FLAC_u_MetadataTypeString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:531
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__MetadataTypeString";

   type anon_array2204 is array (0 .. 15) of aliased FLAC_ordinals_h.FLAC_u_byte;
   type FLAC_u_StreamMetadata_StreamInfo is record
      min_blocksize : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:537
      max_blocksize : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:537
      min_framesize : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:538
      max_framesize : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:538
      sample_rate : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:539
      channels : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:540
      bits_per_sample : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:541
      total_samples : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:542
      md5sum : aliased anon_array2204;  -- ../include/FLAC/format.h:543
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:544

   FLAC_u_STREAM_METADATA_STREAMINFO_MIN_BLOCK_SIZE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:546
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_MIN_BLOCK_SIZE_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_MAX_BLOCK_SIZE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:547
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_MAX_BLOCK_SIZE_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_MIN_FRAME_SIZE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:548
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_MIN_FRAME_SIZE_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_MAX_FRAME_SIZE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:549
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_MAX_FRAME_SIZE_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_SAMPLE_RATE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:550
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_SAMPLE_RATE_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_CHANNELS_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:551
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_CHANNELS_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_BITS_PER_SAMPLE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:552
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_BITS_PER_SAMPLE_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_TOTAL_SAMPLES_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:553
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_TOTAL_SAMPLES_LEN";

   FLAC_u_STREAM_METADATA_STREAMINFO_MD5SUM_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:554
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_STREAMINFO_MD5SUM_LEN";

   type FLAC_u_StreamMetadata_Padding is record
      dummy : aliased int;  -- ../include/FLAC/format.h:562
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:567

   type anon_array2209 is array (0 .. 3) of aliased FLAC_ordinals_h.FLAC_u_byte;
   type FLAC_u_StreamMetadata_Application is record
      id : aliased anon_array2209;  -- ../include/FLAC/format.h:573
      data : access unsigned_char;  -- ../include/FLAC/format.h:574
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:575

   FLAC_u_STREAM_METADATA_APPLICATION_ID_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:577
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_APPLICATION_ID_LEN";

   type FLAC_u_StreamMetadata_SeekPoint is record
      sample_number : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:582
      stream_offset : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:585
      frame_samples : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:589
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:591

   FLAC_u_STREAM_METADATA_SEEKPOINT_SAMPLE_NUMBER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:593
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_SEEKPOINT_SAMPLE_NUMBER_LEN";

   FLAC_u_STREAM_METADATA_SEEKPOINT_STREAM_OFFSET_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:594
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_SEEKPOINT_STREAM_OFFSET_LEN";

   FLAC_u_STREAM_METADATA_SEEKPOINT_FRAME_SAMPLES_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:595
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_SEEKPOINT_FRAME_SAMPLES_LEN";

   FLAC_u_STREAM_METADATA_SEEKPOINT_PLACEHOLDER : aliased constant FLAC_ordinals_h.FLAC_u_uint64  -- ../include/FLAC/format.h:604
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_SEEKPOINT_PLACEHOLDER";

   type FLAC_u_StreamMetadata_SeekTable is record
      num_points : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:620
      points : access FLAC_u_StreamMetadata_SeekPoint;  -- ../include/FLAC/format.h:621
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:622

   type FLAC_u_StreamMetadata_VorbisComment_Entry is record
      length : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:632
      c_entry : access unsigned_char;  -- ../include/FLAC/format.h:633
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:634

   FLAC_u_STREAM_METADATA_VORBIS_COMMENT_ENTRY_LENGTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:636
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_VORBIS_COMMENT_ENTRY_LENGTH_LEN";

   type FLAC_u_StreamMetadata_VorbisComment is record
      vendor_string : aliased FLAC_u_StreamMetadata_VorbisComment_Entry;  -- ../include/FLAC/format.h:642
      num_comments : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:643
      comments : access FLAC_u_StreamMetadata_VorbisComment_Entry;  -- ../include/FLAC/format.h:644
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:645

   FLAC_u_STREAM_METADATA_VORBIS_COMMENT_NUM_COMMENTS_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:647
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_VORBIS_COMMENT_NUM_COMMENTS_LEN";

   type FLAC_u_StreamMetadata_CueSheet_Index is record
      offset : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:655
      number : aliased FLAC_ordinals_h.FLAC_u_byte;  -- ../include/FLAC/format.h:660
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:662

   FLAC_u_STREAM_METADATA_CUESHEET_INDEX_OFFSET_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:664
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_INDEX_OFFSET_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_INDEX_NUMBER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:665
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_INDEX_NUMBER_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_INDEX_RESERVED_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:666
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_INDEX_RESERVED_LEN";

   subtype anon_array2227 is Interfaces.C.char_array (0 .. 12);
   type FLAC_u_StreamMetadata_CueSheet_Track is record
      offset : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:674
      number : aliased FLAC_ordinals_h.FLAC_u_byte;  -- ../include/FLAC/format.h:677
      isrc : aliased anon_array2227;  -- ../include/FLAC/format.h:680
      c_type : Extensions.Unsigned_1;  -- ../include/FLAC/format.h:683
      pre_emphasis : Extensions.Unsigned_1;  -- ../include/FLAC/format.h:686
      num_indices : aliased FLAC_ordinals_h.FLAC_u_byte;  -- ../include/FLAC/format.h:689
      indices : access FLAC_u_StreamMetadata_CueSheet_Index;  -- ../include/FLAC/format.h:692
   end record
   with Convention => C_Pass_By_Copy,
        Pack => True,
        Alignment => 8;  -- ../include/FLAC/format.h:695

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_OFFSET_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:697
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_OFFSET_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_NUMBER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:698
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_NUMBER_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_ISRC_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:699
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_ISRC_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_TYPE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:700
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_TYPE_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_PRE_EMPHASIS_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:701
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_PRE_EMPHASIS_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_RESERVED_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:702
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_RESERVED_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_TRACK_NUM_INDICES_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:703
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_TRACK_NUM_INDICES_LEN";

   subtype anon_array2232 is Interfaces.C.char_array (0 .. 128);
   type FLAC_u_StreamMetadata_CueSheet is record
      media_catalog_number : aliased anon_array2232;  -- ../include/FLAC/format.h:711
      lead_in : aliased FLAC_ordinals_h.FLAC_u_uint64;  -- ../include/FLAC/format.h:717
      is_cd : aliased FLAC_ordinals_h.FLAC_u_bool;  -- ../include/FLAC/format.h:720
      num_tracks : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:723
      tracks : access FLAC_u_StreamMetadata_CueSheet_Track;  -- ../include/FLAC/format.h:726
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:729

   FLAC_u_STREAM_METADATA_CUESHEET_MEDIA_CATALOG_NUMBER_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:731
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_MEDIA_CATALOG_NUMBER_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_LEAD_IN_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:732
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_LEAD_IN_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_IS_CD_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:733
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_IS_CD_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_RESERVED_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:734
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_RESERVED_LEN";

   FLAC_u_STREAM_METADATA_CUESHEET_NUM_TRACKS_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:735
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_CUESHEET_NUM_TRACKS_LEN";

   type FLAC_u_StreamMetadata_Picture_Type is 
     (FLAC_u_STREAM_METADATA_PICTURE_TYPE_OTHER,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_FILE_ICON_STANDARD,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_FILE_ICON,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_FRONT_COVER,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_BACK_COVER,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_LEAFLET_PAGE,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_MEDIA,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_LEAD_ARTIST,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_ARTIST,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_CONDUCTOR,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_BAND,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_COMPOSER,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_LYRICIST,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_RECORDING_LOCATION,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_DURING_RECORDING,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_DURING_PERFORMANCE,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_VIDEO_SCREEN_CAPTURE,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_FISH,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_ILLUSTRATION,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_BAND_LOGOTYPE,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_PUBLISHER_LOGOTYPE,
      FLAC_u_STREAM_METADATA_PICTURE_TYPE_UNDEFINED)
   with Convention => C;  -- ../include/FLAC/format.h:762

   FLAC_u_StreamMetadata_Picture_TypeString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/format.h:770
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamMetadata_Picture_TypeString";

   type FLAC_u_StreamMetadata_Picture is record
      c_type : aliased FLAC_u_StreamMetadata_Picture_Type;  -- ../include/FLAC/format.h:777
      mime_type : Interfaces.C.Strings.chars_ptr;  -- ../include/FLAC/format.h:780
      description : access unsigned_char;  -- ../include/FLAC/format.h:791
      width : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:798
      height : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:801
      depth : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:804
      colors : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:807
      data_length : aliased FLAC_ordinals_h.FLAC_u_uint32;  -- ../include/FLAC/format.h:812
      data : access unsigned_char;  -- ../include/FLAC/format.h:815
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:818

   FLAC_u_STREAM_METADATA_PICTURE_TYPE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:820
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_TYPE_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_MIME_TYPE_LENGTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:821
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_MIME_TYPE_LENGTH_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_DESCRIPTION_LENGTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:822
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_DESCRIPTION_LENGTH_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_WIDTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:823
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_WIDTH_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_HEIGHT_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:824
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_HEIGHT_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_DEPTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:825
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_DEPTH_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_COLORS_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:826
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_COLORS_LEN";

   FLAC_u_STREAM_METADATA_PICTURE_DATA_LENGTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:827
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_PICTURE_DATA_LENGTH_LEN";

   type FLAC_u_StreamMetadata_Unknown is record
      data : access unsigned_char;  -- ../include/FLAC/format.h:835
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:836

   type anon_anon_43 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            stream_info : aliased FLAC_u_StreamMetadata_StreamInfo;  -- ../include/FLAC/format.h:854
         when 1 =>
            padding : aliased FLAC_u_StreamMetadata_Padding;  -- ../include/FLAC/format.h:855
         when 2 =>
            application : aliased FLAC_u_StreamMetadata_Application;  -- ../include/FLAC/format.h:856
         when 3 =>
            seek_table : aliased FLAC_u_StreamMetadata_SeekTable;  -- ../include/FLAC/format.h:857
         when 4 =>
            vorbis_comment : aliased FLAC_u_StreamMetadata_VorbisComment;  -- ../include/FLAC/format.h:858
         when 5 =>
            cue_sheet : aliased FLAC_u_StreamMetadata_CueSheet;  -- ../include/FLAC/format.h:859
         when 6 =>
            picture : aliased FLAC_u_StreamMetadata_Picture;  -- ../include/FLAC/format.h:860
         when others =>
            unknown : aliased FLAC_u_StreamMetadata_Unknown;  -- ../include/FLAC/format.h:861
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type FLAC_u_StreamMetadata is record
      c_type : aliased FLAC_u_MetadataType;  -- ../include/FLAC/format.h:842
      is_last : aliased FLAC_ordinals_h.FLAC_u_bool;  -- ../include/FLAC/format.h:847
      length : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/format.h:850
      data : aliased anon_anon_43;  -- ../include/FLAC/format.h:862
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/format.h:841

   FLAC_u_STREAM_METADATA_IS_LAST_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:867
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_IS_LAST_LEN";

   FLAC_u_STREAM_METADATA_TYPE_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:868
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_TYPE_LEN";

   FLAC_u_STREAM_METADATA_LENGTH_LEN : aliased constant utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:869
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__STREAM_METADATA_LENGTH_LEN";

   function FLAC_u_format_sample_rate_is_valid (sample_rate : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:890
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_sample_rate_is_valid";

   function FLAC_u_format_blocksize_is_subset (blocksize : utypes_uuint32_t_h.uint32_t; sample_rate : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:902
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_blocksize_is_subset";

   function FLAC_u_format_sample_rate_is_subset (sample_rate : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:913
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_sample_rate_is_subset";

   function FLAC_u_format_vorbiscomment_entry_name_is_legal (name : Interfaces.C.Strings.chars_ptr) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:927
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_vorbiscomment_entry_name_is_legal";

   function FLAC_u_format_vorbiscomment_entry_value_is_legal (value : access unsigned_char; length : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:943
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_vorbiscomment_entry_value_is_legal";

   function FLAC_u_format_vorbiscomment_entry_is_legal (c_entry : access unsigned_char; length : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:960
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_vorbiscomment_entry_is_legal";

   function FLAC_u_format_seektable_is_legal (seek_table : access constant FLAC_u_StreamMetadata_SeekTable) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:972
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_seektable_is_legal";

   function FLAC_u_format_seektable_sort (seek_table : access FLAC_u_StreamMetadata_SeekTable) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/format.h:986
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_seektable_sort";

   function FLAC_u_format_cuesheet_is_legal
     (cue_sheet : access constant FLAC_u_StreamMetadata_CueSheet;
      check_cd_da_subset : FLAC_ordinals_h.FLAC_u_bool;
      violation : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:1006
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_cuesheet_is_legal";

   function FLAC_u_format_picture_is_legal (picture : access constant FLAC_u_StreamMetadata_Picture; violation : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/format.h:1024
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__format_picture_is_legal";

end FLAC_format_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
