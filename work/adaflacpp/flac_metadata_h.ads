pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with FLAC_format_h;
with FLAC_ordinals_h;
with System;
with utypes_uuint32_t_h;
with sys_utypes_uoff_t_h;
with FLAC_callback_h;
with Interfaces.C.Extensions;

package FLAC_metadata_h is

   function FLAC_u_metadata_get_streaminfo (filename : Interfaces.C.Strings.chars_ptr; streaminfo : access FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:164
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_get_streaminfo";

   function FLAC_u_metadata_get_tags (filename : Interfaces.C.Strings.chars_ptr; tags : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:183
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_get_tags";

   function FLAC_u_metadata_get_cuesheet (filename : Interfaces.C.Strings.chars_ptr; cuesheet : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:202
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_get_cuesheet";

   function FLAC_u_metadata_get_picture
     (filename : Interfaces.C.Strings.chars_ptr;
      picture : System.Address;
      c_type : FLAC_format_h.FLAC_u_StreamMetadata_Picture_Type;
      mime_type : Interfaces.C.Strings.chars_ptr;
      description : access unsigned_char;
      max_width : utypes_uuint32_t_h.uint32_t;
      max_height : utypes_uuint32_t_h.uint32_t;
      max_depth : utypes_uuint32_t_h.uint32_t;
      max_colors : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:242
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_get_picture";

   type FLAC_u_Metadata_SimpleIterator is null record;   -- incomplete struct

   type FLAC_u_Metadata_SimpleIteratorStatus is 
     (FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_OK,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_ILLEGAL_INPUT,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_ERROR_OPENING_FILE,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_NOT_A_FLAC_FILE,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_NOT_WRITABLE,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_BAD_METADATA,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_READ_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_SEEK_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_WRITE_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_RENAME_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_UNLINK_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_MEMORY_ALLOCATION_ERROR,
      FLAC_u_METADATA_SIMPLE_ITERATOR_STATUS_INTERNAL_ERROR)
   with Convention => C;  -- ../include/FLAC/metadata.h:355

   FLAC_u_Metadata_SimpleIteratorStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/metadata.h:362
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__Metadata_SimpleIteratorStatusString";

   function FLAC_u_metadata_simple_iterator_new return access FLAC_u_Metadata_SimpleIterator  -- ../include/FLAC/metadata.h:370
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_new";

   procedure FLAC_u_metadata_simple_iterator_delete (iterator : access FLAC_u_Metadata_SimpleIterator)  -- ../include/FLAC/metadata.h:378
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_delete";

   function FLAC_u_metadata_simple_iterator_status (iterator : access FLAC_u_Metadata_SimpleIterator) return FLAC_u_Metadata_SimpleIteratorStatus  -- ../include/FLAC/metadata.h:390
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_status";

   function FLAC_u_metadata_simple_iterator_init
     (iterator : access FLAC_u_Metadata_SimpleIterator;
      filename : Interfaces.C.Strings.chars_ptr;
      read_only : FLAC_ordinals_h.FLAC_u_bool;
      preserve_file_stats : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:416
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_init";

   function FLAC_u_metadata_simple_iterator_is_writable (iterator : access constant FLAC_u_Metadata_SimpleIterator) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:428
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_is_writable";

   function FLAC_u_metadata_simple_iterator_next (iterator : access FLAC_u_Metadata_SimpleIterator) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:442
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_next";

   function FLAC_u_metadata_simple_iterator_prev (iterator : access FLAC_u_Metadata_SimpleIterator) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:456
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_prev";

   function FLAC_u_metadata_simple_iterator_is_last (iterator : access constant FLAC_u_Metadata_SimpleIterator) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:469
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_is_last";

   function FLAC_u_metadata_simple_iterator_get_block_offset (iterator : access constant FLAC_u_Metadata_SimpleIterator) return sys_utypes_uoff_t_h.off_t  -- ../include/FLAC/metadata.h:485
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_get_block_offset";

   function FLAC_u_metadata_simple_iterator_get_block_type (iterator : access constant FLAC_u_Metadata_SimpleIterator) return FLAC_format_h.FLAC_u_MetadataType  -- ../include/FLAC/metadata.h:499
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_get_block_type";

   function FLAC_u_metadata_simple_iterator_get_block_length (iterator : access constant FLAC_u_Metadata_SimpleIterator) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/metadata.h:516
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_get_block_length";

   function FLAC_u_metadata_simple_iterator_get_application_id (iterator : access FLAC_u_Metadata_SimpleIterator; id : access unsigned_char) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:541
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_get_application_id";

   function FLAC_u_metadata_simple_iterator_get_block (iterator : access FLAC_u_Metadata_SimpleIterator) return access FLAC_format_h.FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:559
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_get_block";

   function FLAC_u_metadata_simple_iterator_set_block
     (iterator : access FLAC_u_Metadata_SimpleIterator;
      block : access FLAC_format_h.FLAC_u_StreamMetadata;
      use_padding : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:615
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_set_block";

   function FLAC_u_metadata_simple_iterator_insert_block_after
     (iterator : access FLAC_u_Metadata_SimpleIterator;
      block : access FLAC_format_h.FLAC_u_StreamMetadata;
      use_padding : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:640
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_insert_block_after";

   function FLAC_u_metadata_simple_iterator_delete_block (iterator : access FLAC_u_Metadata_SimpleIterator; use_padding : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:659
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_simple_iterator_delete_block";

   type FLAC_u_Metadata_Chain is null record;   -- incomplete struct

   type FLAC_u_Metadata_Iterator is null record;   -- incomplete struct

   type FLAC_u_Metadata_ChainStatus is 
     (FLAC_u_METADATA_CHAIN_STATUS_OK,
      FLAC_u_METADATA_CHAIN_STATUS_ILLEGAL_INPUT,
      FLAC_u_METADATA_CHAIN_STATUS_ERROR_OPENING_FILE,
      FLAC_u_METADATA_CHAIN_STATUS_NOT_A_FLAC_FILE,
      FLAC_u_METADATA_CHAIN_STATUS_NOT_WRITABLE,
      FLAC_u_METADATA_CHAIN_STATUS_BAD_METADATA,
      FLAC_u_METADATA_CHAIN_STATUS_READ_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_SEEK_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_WRITE_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_RENAME_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_UNLINK_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_MEMORY_ALLOCATION_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_INTERNAL_ERROR,
      FLAC_u_METADATA_CHAIN_STATUS_INVALID_CALLBACKS,
      FLAC_u_METADATA_CHAIN_STATUS_READ_WRITE_MISMATCH,
      FLAC_u_METADATA_CHAIN_STATUS_WRONG_WRITE_CALL)
   with Convention => C;  -- ../include/FLAC/metadata.h:793

   FLAC_u_Metadata_ChainStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/metadata.h:800
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__Metadata_ChainStatusString";

   function FLAC_u_metadata_chain_new return access FLAC_u_Metadata_Chain  -- ../include/FLAC/metadata.h:809
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_new";

   procedure FLAC_u_metadata_chain_delete (chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:817
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_delete";

   function FLAC_u_metadata_chain_status (chain : access FLAC_u_Metadata_Chain) return FLAC_u_Metadata_ChainStatus  -- ../include/FLAC/metadata.h:829
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_status";

   function FLAC_u_metadata_chain_read (chain : access FLAC_u_Metadata_Chain; filename : Interfaces.C.Strings.chars_ptr) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:848
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_read";

   function FLAC_u_metadata_chain_read_ogg (chain : access FLAC_u_Metadata_Chain; filename : Interfaces.C.Strings.chars_ptr) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:870
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_read_ogg";

   function FLAC_u_metadata_chain_read_with_callbacks
     (chain : access FLAC_u_Metadata_Chain;
      handle : FLAC_callback_h.FLAC_u_IOHandle;
      callbacks : FLAC_callback_h.FLAC_u_IOCallbacks) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:892
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_read_with_callbacks";

   function FLAC_u_metadata_chain_read_ogg_with_callbacks
     (chain : access FLAC_u_Metadata_Chain;
      handle : FLAC_callback_h.FLAC_u_IOHandle;
      callbacks : FLAC_callback_h.FLAC_u_IOCallbacks) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:917
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_read_ogg_with_callbacks";

   function FLAC_u_metadata_chain_check_if_tempfile_needed (chain : access FLAC_u_Metadata_Chain; use_padding : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:945
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_check_if_tempfile_needed";

   function FLAC_u_metadata_chain_write
     (chain : access FLAC_u_Metadata_Chain;
      use_padding : FLAC_ordinals_h.FLAC_u_bool;
      preserve_file_stats : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:991
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_write";

   function FLAC_u_metadata_chain_write_with_callbacks
     (chain : access FLAC_u_Metadata_Chain;
      use_padding : FLAC_ordinals_h.FLAC_u_bool;
      handle : FLAC_callback_h.FLAC_u_IOHandle;
      callbacks : FLAC_callback_h.FLAC_u_IOCallbacks) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1021
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_write_with_callbacks";

   function FLAC_u_metadata_chain_write_with_callbacks_and_tempfile
     (chain : access FLAC_u_Metadata_Chain;
      use_padding : FLAC_ordinals_h.FLAC_u_bool;
      handle : FLAC_callback_h.FLAC_u_IOHandle;
      callbacks : FLAC_callback_h.FLAC_u_IOCallbacks;
      temp_handle : FLAC_callback_h.FLAC_u_IOHandle;
      temp_callbacks : FLAC_callback_h.FLAC_u_IOCallbacks) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1072
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_write_with_callbacks_and_tempfile";

   procedure FLAC_u_metadata_chain_merge_padding (chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:1086
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_merge_padding";

   procedure FLAC_u_metadata_chain_sort_padding (chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:1101
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_chain_sort_padding";

   function FLAC_u_metadata_iterator_new return access FLAC_u_Metadata_Iterator  -- ../include/FLAC/metadata.h:1111
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_new";

   procedure FLAC_u_metadata_iterator_delete (iterator : access FLAC_u_Metadata_Iterator)  -- ../include/FLAC/metadata.h:1119
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_delete";

   procedure FLAC_u_metadata_iterator_init (iterator : access FLAC_u_Metadata_Iterator; chain : access FLAC_u_Metadata_Chain)  -- ../include/FLAC/metadata.h:1130
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_init";

   function FLAC_u_metadata_iterator_next (iterator : access FLAC_u_Metadata_Iterator) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1144
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_next";

   function FLAC_u_metadata_iterator_prev (iterator : access FLAC_u_Metadata_Iterator) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1158
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_prev";

   function FLAC_u_metadata_iterator_get_block_type (iterator : access constant FLAC_u_Metadata_Iterator) return FLAC_format_h.FLAC_u_MetadataType  -- ../include/FLAC/metadata.h:1170
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_get_block_type";

   function FLAC_u_metadata_iterator_get_block (iterator : access FLAC_u_Metadata_Iterator) return access FLAC_format_h.FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:1191
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_get_block";

   function FLAC_u_metadata_iterator_set_block (iterator : access FLAC_u_Metadata_Iterator; block : access FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1208
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_set_block";

   function FLAC_u_metadata_iterator_delete_block (iterator : access FLAC_u_Metadata_Iterator; replace_with_padding : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1226
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_delete_block";

   function FLAC_u_metadata_iterator_insert_block_before (iterator : access FLAC_u_Metadata_Iterator; block : access FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1245
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_insert_block_before";

   function FLAC_u_metadata_iterator_insert_block_after (iterator : access FLAC_u_Metadata_Iterator; block : access FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1263
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_iterator_insert_block_after";

   function FLAC_u_metadata_object_new (c_type : FLAC_format_h.FLAC_u_MetadataType) return access FLAC_format_h.FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:1333
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_new";

   function FLAC_u_metadata_object_clone (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access FLAC_format_h.FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:1347
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_clone";

   procedure FLAC_u_metadata_object_delete (object : access FLAC_format_h.FLAC_u_StreamMetadata)  -- ../include/FLAC/metadata.h:1358
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_delete";

   function FLAC_u_metadata_object_is_equal (block1 : access constant FLAC_format_h.FLAC_u_StreamMetadata; block2 : access constant FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1373
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_is_equal";

   function FLAC_u_metadata_object_application_set_data
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      data : access unsigned_char;
      length : utypes_uuint32_t_h.uint32_t;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1396
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_application_set_data";

   function FLAC_u_metadata_object_seektable_resize_points (object : access FLAC_format_h.FLAC_u_StreamMetadata; new_num_points : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1414
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_resize_points";

   procedure FLAC_u_metadata_object_seektable_set_point
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      point_num : utypes_uuint32_t_h.uint32_t;
      point : FLAC_format_h.FLAC_u_StreamMetadata_SeekPoint)  -- ../include/FLAC/metadata.h:1426
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_set_point";

   function FLAC_u_metadata_object_seektable_insert_point
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      point_num : utypes_uuint32_t_h.uint32_t;
      point : FLAC_format_h.FLAC_u_StreamMetadata_SeekPoint) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1440
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_insert_point";

   function FLAC_u_metadata_object_seektable_delete_point (object : access FLAC_format_h.FLAC_u_StreamMetadata; point_num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1453
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_delete_point";

   function FLAC_u_metadata_object_seektable_is_legal (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1466
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_is_legal";

   function FLAC_u_metadata_object_seektable_template_append_placeholders (object : access FLAC_format_h.FLAC_u_StreamMetadata; num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1483
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_template_append_placeholders";

   function FLAC_u_metadata_object_seektable_template_append_point (object : access FLAC_format_h.FLAC_u_StreamMetadata; sample_number : FLAC_ordinals_h.FLAC_u_uint64) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1500
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_template_append_point";

   function FLAC_u_metadata_object_seektable_template_append_points
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      sample_numbers : access Extensions.unsigned_long_long;
      num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1518
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_template_append_points";

   function FLAC_u_metadata_object_seektable_template_append_spaced_points
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      num : utypes_uuint32_t_h.uint32_t;
      total_samples : FLAC_ordinals_h.FLAC_u_uint64) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1540
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_template_append_spaced_points";

   function FLAC_u_metadata_object_seektable_template_append_spaced_points_by_samples
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      samples : utypes_uuint32_t_h.uint32_t;
      total_samples : FLAC_ordinals_h.FLAC_u_uint64) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1568
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_template_append_spaced_points_by_samples";

   function FLAC_u_metadata_object_seektable_template_sort (object : access FLAC_format_h.FLAC_u_StreamMetadata; compact : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1585
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_seektable_template_sort";

   function FLAC_u_metadata_object_vorbiscomment_set_vendor_string
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1610
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_set_vendor_string";

   function FLAC_u_metadata_object_vorbiscomment_resize_comments (object : access FLAC_format_h.FLAC_u_StreamMetadata; new_num_comments : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1628
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_resize_comments";

   function FLAC_u_metadata_object_vorbiscomment_set_comment
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      comment_num : utypes_uuint32_t_h.uint32_t;
      c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1655
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_set_comment";

   function FLAC_u_metadata_object_vorbiscomment_insert_comment
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      comment_num : utypes_uuint32_t_h.uint32_t;
      c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1685
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_insert_comment";

   function FLAC_u_metadata_object_vorbiscomment_append_comment
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1710
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_append_comment";

   function FLAC_u_metadata_object_vorbiscomment_replace_comment
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      c_all : FLAC_ordinals_h.FLAC_u_bool;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1745
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_replace_comment";

   function FLAC_u_metadata_object_vorbiscomment_delete_comment (object : access FLAC_format_h.FLAC_u_StreamMetadata; comment_num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1758
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_delete_comment";

   function FLAC_u_metadata_object_vorbiscomment_entry_from_name_value_pair
     (c_entry : access FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      field_name : Interfaces.C.Strings.chars_ptr;
      field_value : Interfaces.C.Strings.chars_ptr) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1779
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_entry_from_name_value_pair";

   function FLAC_u_metadata_object_vorbiscomment_entry_to_name_value_pair
     (c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      field_name : System.Address;
      field_value : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1800
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_entry_to_name_value_pair";

   function FLAC_u_metadata_object_vorbiscomment_entry_matches
     (c_entry : FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;
      field_name : Interfaces.C.Strings.chars_ptr;
      field_name_length : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1814
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_entry_matches";

   function FLAC_u_metadata_object_vorbiscomment_find_entry_from
     (object : access constant FLAC_format_h.FLAC_u_StreamMetadata;
      offset : utypes_uuint32_t_h.uint32_t;
      field_name : Interfaces.C.Strings.chars_ptr) return int  -- ../include/FLAC/metadata.h:1833
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_find_entry_from";

   function FLAC_u_metadata_object_vorbiscomment_remove_entry_matching (object : access FLAC_format_h.FLAC_u_StreamMetadata; field_name : Interfaces.C.Strings.chars_ptr) return int  -- ../include/FLAC/metadata.h:1846
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_remove_entry_matching";

   function FLAC_u_metadata_object_vorbiscomment_remove_entries_matching (object : access FLAC_format_h.FLAC_u_StreamMetadata; field_name : Interfaces.C.Strings.chars_ptr) return int  -- ../include/FLAC/metadata.h:1859
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_vorbiscomment_remove_entries_matching";

   function FLAC_u_metadata_object_cuesheet_track_new return access FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track  -- ../include/FLAC/metadata.h:1868
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_new";

   function FLAC_u_metadata_object_cuesheet_track_clone (object : access constant FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track) return access FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track  -- ../include/FLAC/metadata.h:1883
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_clone";

   procedure FLAC_u_metadata_object_cuesheet_track_delete (object : access FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track)  -- ../include/FLAC/metadata.h:1891
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_delete";

   function FLAC_u_metadata_object_cuesheet_track_resize_indices
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      track_num : utypes_uuint32_t_h.uint32_t;
      new_num_indices : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1912
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_resize_indices";

   function FLAC_u_metadata_object_cuesheet_track_insert_index
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      track_num : utypes_uuint32_t_h.uint32_t;
      index_num : utypes_uuint32_t_h.uint32_t;
      index : FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Index) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1935
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_insert_index";

   function FLAC_u_metadata_object_cuesheet_track_insert_blank_index
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      track_num : utypes_uuint32_t_h.uint32_t;
      index_num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1959
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_insert_blank_index";

   function FLAC_u_metadata_object_cuesheet_track_delete_index
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      track_num : utypes_uuint32_t_h.uint32_t;
      index_num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1978
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_track_delete_index";

   function FLAC_u_metadata_object_cuesheet_resize_tracks (object : access FLAC_format_h.FLAC_u_StreamMetadata; new_num_tracks : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:1996
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_resize_tracks";

   function FLAC_u_metadata_object_cuesheet_set_track
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      track_num : utypes_uuint32_t_h.uint32_t;
      track : access FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2018
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_set_track";

   function FLAC_u_metadata_object_cuesheet_insert_track
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      track_num : utypes_uuint32_t_h.uint32_t;
      track : access FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2041
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_insert_track";

   function FLAC_u_metadata_object_cuesheet_insert_blank_track (object : access FLAC_format_h.FLAC_u_StreamMetadata; track_num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2060
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_insert_blank_track";

   function FLAC_u_metadata_object_cuesheet_delete_track (object : access FLAC_format_h.FLAC_u_StreamMetadata; track_num : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2075
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_delete_track";

   function FLAC_u_metadata_object_cuesheet_is_legal
     (object : access constant FLAC_format_h.FLAC_u_StreamMetadata;
      check_cd_da_subset : FLAC_ordinals_h.FLAC_u_bool;
      violation : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2096
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_is_legal";

   function FLAC_u_metadata_object_cuesheet_calculate_cddb_id (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC/metadata.h:2109
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_cuesheet_calculate_cddb_id";

   function FLAC_u_metadata_object_picture_set_mime_type
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      mime_type : Interfaces.C.Strings.chars_ptr;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2132
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_picture_set_mime_type";

   function FLAC_u_metadata_object_picture_set_description
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      description : access unsigned_char;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2154
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_picture_set_description";

   function FLAC_u_metadata_object_picture_set_data
     (object : access FLAC_format_h.FLAC_u_StreamMetadata;
      data : access unsigned_char;
      length : FLAC_ordinals_h.FLAC_u_uint32;
      copy : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2179
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_picture_set_data";

   function FLAC_u_metadata_object_picture_is_legal (object : access constant FLAC_format_h.FLAC_u_StreamMetadata; violation : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/metadata.h:2198
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_picture_is_legal";

   function FLAC_u_metadata_object_get_raw (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access unsigned_char  -- ../include/FLAC/metadata.h:2212
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_get_raw";

   function FLAC_u_metadata_object_set_raw (buffer : access unsigned_char; length : FLAC_ordinals_h.FLAC_u_uint32) return access FLAC_format_h.FLAC_u_StreamMetadata  -- ../include/FLAC/metadata.h:2227
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__metadata_object_set_raw";

end FLAC_metadata_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
