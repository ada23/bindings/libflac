pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_localefwd_h is

   type locale is null record;   -- incomplete struct

   type ctype_base is null record;   -- incomplete struct

   package ctype_wchar_t is
      type ctype is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use ctype_wchar_t;

   package ctype_char is
      type ctype is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use ctype_char;



   type codecvt_base is null record;   -- incomplete struct

   package codecvt_char32_t_char_uu_mbstate_t is
      type codecvt is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use codecvt_char32_t_char_uu_mbstate_t;

   package codecvt_char16_t_char_uu_mbstate_t is
      type codecvt is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use codecvt_char16_t_char_uu_mbstate_t;

   package codecvt_wchar_t_char_uu_mbstate_t is
      type codecvt is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use codecvt_wchar_t_char_uu_mbstate_t;

   package codecvt_char_char_uu_mbstate_t is
      type codecvt is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use codecvt_char_char_uu_mbstate_t;



   type time_base is null record;   -- incomplete struct

   type money_base is null record;   -- incomplete struct

   type messages_base is null record;   -- incomplete struct

end cpp_12_1_0_bits_localefwd_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
