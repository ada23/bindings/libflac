pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_stl_construct_h is

   package u_Destroy_aux_1 is
      type u_Destroy_aux is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use u_Destroy_aux_1;



   package u_Destroy_n_aux_1 is
      type u_Destroy_n_aux is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use u_Destroy_n_aux_1;



end cpp_12_1_0_bits_stl_construct_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
