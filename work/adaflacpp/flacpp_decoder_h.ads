pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with FLAC_stream_decoder_h;
with Interfaces.C.Extensions;
with FLAC_format_h;
with FLAC_ordinals_h;
with utypes_uuint32_t_h;
with System;
limited with ustdio_h;
with Interfaces.C.Strings;
limited with cpp_12_1_0_bits_basic_string_h;

package FLACpp_decoder_h is

   package Class_Stream is
      type State is record
         state_u : aliased FLAC_stream_decoder_h.FLAC_u_StreamDecoderState;  -- ../include/FLAC++/decoder.h:111
      end record
      with Convention => C_Pass_By_Copy;
      type Stream is abstract tagged limited record
         decoder_u : access FLAC_stream_decoder_h.FLAC_u_StreamDecoder;  -- ../include/FLAC++/decoder.h:188
      end record
      with Import => True,
           Convention => CPP;

      function New_Stream return Stream is abstract;  -- ../include/FLAC++/decoder.h:114
      pragma CPP_Constructor (New_Stream, "_ZN4FLAC7Decoder6StreamC1Ev");

      procedure Delete_Stream (this : access Stream)  -- ../include/FLAC++/decoder.h:115
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6StreamD1Ev";

      procedure Delete_And_Free_Stream (this : access Stream)  -- ../include/FLAC++/decoder.h:115
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6StreamD0Ev";

      function is_valid (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream8is_validEv";

      --  skipped func __conv_op 

      function set_ogg_serial_number (this : access Stream; value : long) return Extensions.bool  -- ../include/FLAC++/decoder.h:125
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream21set_ogg_serial_numberEl";

      function set_md5_checking (this : access Stream; value : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/decoder.h:126
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream16set_md5_checkingEb";

      function set_metadata_respond (this : access Stream; c_type : FLAC_format_h.FLAC_u_MetadataType) return Extensions.bool  -- ../include/FLAC++/decoder.h:127
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream20set_metadata_respondE18FLAC__MetadataType";

      function set_metadata_respond_application (this : access Stream; id : access unsigned_char) return Extensions.bool  -- ../include/FLAC++/decoder.h:128
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream32set_metadata_respond_applicationEPKh";

      function set_metadata_respond_all (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:129
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream24set_metadata_respond_allEv";

      function set_metadata_ignore (this : access Stream; c_type : FLAC_format_h.FLAC_u_MetadataType) return Extensions.bool  -- ../include/FLAC++/decoder.h:130
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream19set_metadata_ignoreE18FLAC__MetadataType";

      function set_metadata_ignore_application (this : access Stream; id : access unsigned_char) return Extensions.bool  -- ../include/FLAC++/decoder.h:131
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream31set_metadata_ignore_applicationEPKh";

      function set_metadata_ignore_all (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:132
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream23set_metadata_ignore_allEv";

      function get_state (this : access constant Stream'Class) return State  -- ../include/FLAC++/decoder.h:135
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream9get_stateEv";

      function get_md5_checking (this : access constant Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:136
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream16get_md5_checkingEv";

      function get_total_samples (this : access constant Stream) return FLAC_ordinals_h.FLAC_u_uint64  -- ../include/FLAC++/decoder.h:137
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream17get_total_samplesEv";

      function get_channels (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/decoder.h:138
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream12get_channelsEv";

      function get_channel_assignment (this : access constant Stream) return FLAC_format_h.FLAC_u_ChannelAssignment  -- ../include/FLAC++/decoder.h:139
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream22get_channel_assignmentEv";

      function get_bits_per_sample (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/decoder.h:140
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream19get_bits_per_sampleEv";

      function get_sample_rate (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/decoder.h:141
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream15get_sample_rateEv";

      function get_blocksize (this : access constant Stream) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/decoder.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream13get_blocksizeEv";

      function get_decode_position (this : access constant Stream; position : access Extensions.unsigned_long_long) return Extensions.bool  -- ../include/FLAC++/decoder.h:143
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC7Decoder6Stream19get_decode_positionEPy";

      function init (this : access Stream) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:145
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream4initEv";

      function init_ogg (this : access Stream) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:146
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream8init_oggEv";

      function finish (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:148
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream6finishEv";

      function flush (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:150
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream5flushEv";

      function reset (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:151
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream5resetEv";

      function process_single (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:153
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream14process_singleEv";

      function process_until_end_of_metadata (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:154
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream29process_until_end_of_metadataEv";

      function process_until_end_of_stream (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:155
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream27process_until_end_of_streamEv";

      function skip_single_frame (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:156
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream17skip_single_frameEv";

      function seek_absolute (this : access Stream; sample : FLAC_ordinals_h.FLAC_u_uint64) return Extensions.bool  -- ../include/FLAC++/decoder.h:158
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream13seek_absoluteEy";

      function read_callback
        (this : access Stream;
         buffer : access unsigned_char;
         bytes : access unsigned_long) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderReadStatus is abstract;  -- ../include/FLAC++/decoder.h:161

      function seek_callback (this : access Stream; absolute_byte_offset : FLAC_ordinals_h.FLAC_u_uint64) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderSeekStatus  -- ../include/FLAC++/decoder.h:164
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream13seek_callbackEy";

      function tell_callback (this : access Stream; absolute_byte_offset : access Extensions.unsigned_long_long) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderTellStatus  -- ../include/FLAC++/decoder.h:167
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream13tell_callbackEPy";

      function length_callback (this : access Stream; stream_length : access Extensions.unsigned_long_long) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderLengthStatus  -- ../include/FLAC++/decoder.h:170
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream15length_callbackEPy";

      function eof_callback (this : access Stream) return Extensions.bool  -- ../include/FLAC++/decoder.h:173
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream12eof_callbackEv";

      function write_callback
        (this : access Stream;
         frame : access constant FLAC_format_h.FLAC_u_Frame;
         buffer : System.Address) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderWriteStatus is abstract;  -- ../include/FLAC++/decoder.h:176

      procedure metadata_callback (this : access Stream; metadata : access constant FLAC_format_h.FLAC_u_StreamMetadata)  -- ../include/FLAC++/decoder.h:179
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream17metadata_callbackEPK20FLAC__StreamMetadata";

      procedure error_callback (this : access Stream; status : FLAC_stream_decoder_h.FLAC_u_StreamDecoderErrorStatus) is abstract;  -- ../include/FLAC++/decoder.h:182

      function read_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         buffer : access unsigned_char;
         bytes : access unsigned_long;
         client_data : System.Address) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderReadStatus  -- ../include/FLAC++/decoder.h:190
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream14read_callback_EPK19FLAC__StreamDecoderPhPmPv";

      function seek_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         absolute_byte_offset : FLAC_ordinals_h.FLAC_u_uint64;
         client_data : System.Address) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderSeekStatus  -- ../include/FLAC++/decoder.h:191
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream14seek_callback_EPK19FLAC__StreamDecoderyPv";

      function tell_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         absolute_byte_offset : access Extensions.unsigned_long_long;
         client_data : System.Address) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderTellStatus  -- ../include/FLAC++/decoder.h:192
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream14tell_callback_EPK19FLAC__StreamDecoderPyPv";

      function length_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         stream_length : access Extensions.unsigned_long_long;
         client_data : System.Address) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderLengthStatus  -- ../include/FLAC++/decoder.h:193
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream16length_callback_EPK19FLAC__StreamDecoderPyPv";

      function eof_callback_u (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder; client_data : System.Address) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC++/decoder.h:194
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream13eof_callback_EPK19FLAC__StreamDecoderPv";

      function write_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         frame : access constant FLAC_format_h.FLAC_u_Frame;
         buffer : System.Address;
         client_data : System.Address) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderWriteStatus  -- ../include/FLAC++/decoder.h:195
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream15write_callback_EPK19FLAC__StreamDecoderPK11FLAC__FramePKPKiPv";

      procedure metadata_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         metadata : access constant FLAC_format_h.FLAC_u_StreamMetadata;
         client_data : System.Address)  -- ../include/FLAC++/decoder.h:196
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream18metadata_callback_EPK19FLAC__StreamDecoderPK20FLAC__StreamMetadataPv";

      procedure error_callback_u
        (decoder : access constant FLAC_stream_decoder_h.FLAC_u_StreamDecoder;
         status : FLAC_stream_decoder_h.FLAC_u_StreamDecoderErrorStatus;
         client_data : System.Address)  -- ../include/FLAC++/decoder.h:197
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6Stream15error_callback_EPK19FLAC__StreamDecoder30FLAC__StreamDecoderErrorStatusPv";

      procedure Assign_Stream (this : access Stream'Class; arg2 : access constant Stream'Class)  -- ../include/FLAC++/decoder.h:201
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder6StreamaSERKS1_";
   end;
   use Class_Stream;
   package Class_File is
      type File is limited new Stream with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_File return File;  -- ../include/FLAC++/decoder.h:225
      pragma CPP_Constructor (New_File, "_ZN4FLAC7Decoder4FileC1Ev");

      procedure Delete_File (this : access File)  -- ../include/FLAC++/decoder.h:226
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4FileD1Ev";

      procedure Delete_And_Free_File (this : access File)  -- ../include/FLAC++/decoder.h:226
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4FileD0Ev";

      function init (this : access File; the_file : access ustdio_h.uu_sFILE) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:229
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File4initEP7__sFILE";

      function init (this : access File; filename : Interfaces.C.Strings.chars_ptr) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:230
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File4initEPKc";

      function init (this : access File; filename : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:231
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File4initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE";

      function init_ogg (this : access File; the_file : access ustdio_h.uu_sFILE) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:233
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File8init_oggEP7__sFILE";

      function init_ogg (this : access File; filename : Interfaces.C.Strings.chars_ptr) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:234
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File8init_oggEPKc";

      function init_ogg (this : access File; filename : access constant cpp_12_1_0_bits_basic_string_h.Class_basic_string.basic_string) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderInitStatus  -- ../include/FLAC++/decoder.h:235
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File8init_oggERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE";

      function read_callback
        (this : access File;
         buffer : access unsigned_char;
         bytes : access unsigned_long) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderReadStatus  -- ../include/FLAC++/decoder.h:238
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4File13read_callbackEPhPm";

      procedure Assign_File (this : access File'Class; arg2 : access constant File'Class)  -- ../include/FLAC++/decoder.h:242
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC7Decoder4FileaSERKS1_";
   end;
   use Class_File;
end FLACpp_decoder_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
