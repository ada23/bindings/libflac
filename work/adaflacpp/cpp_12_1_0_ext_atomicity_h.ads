pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_ext_atomicity_h is

   --  skipped func __is_single_threaded

   --  skipped func __exchange_and_add

   --  skipped func __atomic_add

   --  skipped func __exchange_and_add_single

   --  skipped func __atomic_add_single

   --  skipped func __exchange_and_add_dispatch

   --  skipped func __atomic_add_dispatch

end cpp_12_1_0_ext_atomicity_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
