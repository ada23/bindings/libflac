pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with FLAC_format_h;
with Interfaces.C.Extensions;
with utypes_uuint32_t_h;
with FLAC_ordinals_h;
with Interfaces.C.Strings;
with System;
with FLAC_metadata_h;
with sys_utypes_uoff_t_h;
with FLAC_callback_h;

package FLACpp_metadata_h is

   package Class_Prototype is
      type Prototype is tagged limited record
         object_u : access FLAC_format_h.FLAC_u_StreamMetadata;  -- ../include/FLAC++/metadata.h:148
         is_reference_u : aliased Extensions.bool;  -- ../include/FLAC++/metadata.h:225
      end record
      with Import => True,
           Convention => CPP;

      function New_Prototype (arg1 : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Prototype;  -- ../include/FLAC++/metadata.h:116
      pragma CPP_Constructor (New_Prototype, "_ZN4FLAC8Metadata9PrototypeC1ERK20FLAC__StreamMetadata");

      function New_Prototype (arg1 : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Prototype;  -- ../include/FLAC++/metadata.h:117
      pragma CPP_Constructor (New_Prototype, "_ZN4FLAC8Metadata9PrototypeC1EPK20FLAC__StreamMetadata");

      function New_Prototype (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return Prototype;  -- ../include/FLAC++/metadata.h:130
      pragma CPP_Constructor (New_Prototype, "_ZN4FLAC8Metadata9PrototypeC1EP20FLAC__StreamMetadatab");

      function Assign_Prototype (this : access Prototype'Class; arg2 : access constant Prototype'Class) return access Prototype  -- ../include/FLAC++/metadata.h:134
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9PrototypeaSERKS1_";

      function Assign_Prototype (this : access Prototype'Class; arg2 : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Prototype  -- ../include/FLAC++/metadata.h:135
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9PrototypeaSERK20FLAC__StreamMetadata";

      function Assign_Prototype (this : access Prototype'Class; arg2 : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Prototype  -- ../include/FLAC++/metadata.h:136
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9PrototypeaSEPK20FLAC__StreamMetadata";

      function assign_object
        (this : access Prototype'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access Prototype  -- ../include/FLAC++/metadata.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9Prototype13assign_objectEP20FLAC__StreamMetadatab";

      procedure clear (this : access Prototype)  -- ../include/FLAC++/metadata.h:146
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9Prototype5clearEv";

      procedure Delete_Prototype (this : access Prototype)  -- ../include/FLAC++/metadata.h:152
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9PrototypeD1Ev";

      procedure Delete_And_Free_Prototype (this : access Prototype)  -- ../include/FLAC++/metadata.h:152
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9PrototypeD0Ev";

      function operator_eq (this : access constant Prototype'Class; object : access constant Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:243
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9PrototypeeqERKS1_";

      function operator_eq (this : access constant Prototype'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:246
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9PrototypeeqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant Prototype'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:249
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9PrototypeeqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Prototype'Class; object : access constant Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:256
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9PrototypeneERKS1_";

      function operator_ne (this : access constant Prototype'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:259
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9PrototypeneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Prototype'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:262
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9PrototypeneEPK20FLAC__StreamMetadata";

      function is_valid (this : access constant Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:265
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9Prototype8is_validEv";

      function get_is_last (this : access constant Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:184
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9Prototype11get_is_lastEv";

      function get_type (this : access constant Prototype'Class) return FLAC_format_h.FLAC_u_MetadataType  -- ../include/FLAC++/metadata.h:191
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9Prototype8get_typeEv";

      function get_length (this : access constant Prototype'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:202
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9Prototype10get_lengthEv";

      procedure set_is_last (this : access Prototype'Class; arg2 : Extensions.bool)  -- ../include/FLAC++/metadata.h:210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9Prototype11set_is_lastEb";

      --  skipped func __conv_op 

      function New_Prototype return Prototype;  -- ../include/FLAC++/metadata.h:222
      pragma CPP_Constructor (New_Prototype, "_ZN4FLAC8Metadata9PrototypeC1Ev");

      procedure set_reference (this : access Prototype'Class; x : Extensions.bool)  -- ../include/FLAC++/metadata.h:226
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9Prototype13set_referenceEb";
   end;
   use Class_Prototype;
   function construct_block (object : access FLAC_format_h.FLAC_u_StreamMetadata) return access Prototype  -- ../include/FLAC++/metadata.h:234
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata5local15construct_blockEP20FLAC__StreamMetadata";

   function clone (arg1 : access constant Prototype'Class) return access Prototype  -- ../include/FLAC++/metadata.h:272
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata5cloneEPKNS0_9PrototypeE";

   package Class_StreamInfo is
      type StreamInfo is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_StreamInfo return StreamInfo;  -- ../include/FLAC++/metadata.h:281
      pragma CPP_Constructor (New_StreamInfo, "_ZN4FLAC8Metadata10StreamInfoC1Ev");

      function New_StreamInfo (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return StreamInfo;  -- ../include/FLAC++/metadata.h:288
      pragma CPP_Constructor (New_StreamInfo, "_ZN4FLAC8Metadata10StreamInfoC1ERK20FLAC__StreamMetadata");

      function New_StreamInfo (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return StreamInfo;  -- ../include/FLAC++/metadata.h:289
      pragma CPP_Constructor (New_StreamInfo, "_ZN4FLAC8Metadata10StreamInfoC1EPK20FLAC__StreamMetadata");

      function New_StreamInfo (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return StreamInfo;  -- ../include/FLAC++/metadata.h:295
      pragma CPP_Constructor (New_StreamInfo, "_ZN4FLAC8Metadata10StreamInfoC1EP20FLAC__StreamMetadatab");

      procedure Delete_StreamInfo (this : access StreamInfo)  -- ../include/FLAC++/metadata.h:297
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfoD1Ev";

      procedure Delete_And_Free_StreamInfo (this : access StreamInfo)  -- ../include/FLAC++/metadata.h:297
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfoD0Ev";

      function Assign_StreamInfo (this : access StreamInfo'Class; object : access constant StreamInfo'Class) return access StreamInfo  -- ../include/FLAC++/metadata.h:301
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfoaSERKS1_";

      function Assign_StreamInfo (this : access StreamInfo'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access StreamInfo  -- ../include/FLAC++/metadata.h:302
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfoaSERK20FLAC__StreamMetadata";

      function Assign_StreamInfo (this : access StreamInfo'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access StreamInfo  -- ../include/FLAC++/metadata.h:303
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfoaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access StreamInfo'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access StreamInfo  -- ../include/FLAC++/metadata.h:309
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant StreamInfo'Class; object : access constant StreamInfo'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:313
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfoeqERKS1_";

      function operator_eq (this : access constant StreamInfo'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:314
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfoeqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant StreamInfo'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:315
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfoeqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant StreamInfo'Class; object : access constant StreamInfo'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:320
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfoneERKS1_";

      function operator_ne (this : access constant StreamInfo'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:321
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfoneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant StreamInfo'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:322
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfoneEPK20FLAC__StreamMetadata";

      function get_min_blocksize (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:327
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo17get_min_blocksizeEv";

      function get_max_blocksize (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:328
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo17get_max_blocksizeEv";

      function get_min_framesize (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:329
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo17get_min_framesizeEv";

      function get_max_framesize (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:330
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo17get_max_framesizeEv";

      function get_sample_rate (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:331
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo15get_sample_rateEv";

      function get_channels (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:332
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo12get_channelsEv";

      function get_bits_per_sample (this : access constant StreamInfo'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:333
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo19get_bits_per_sampleEv";

      function get_total_samples (this : access constant StreamInfo'Class) return FLAC_ordinals_h.FLAC_u_uint64  -- ../include/FLAC++/metadata.h:334
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo17get_total_samplesEv";

      function get_md5sum (this : access constant StreamInfo'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:335
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata10StreamInfo10get_md5sumEv";

      procedure set_min_blocksize (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:337
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo17set_min_blocksizeEj";

      procedure set_max_blocksize (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:338
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo17set_max_blocksizeEj";

      procedure set_min_framesize (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:339
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo17set_min_framesizeEj";

      procedure set_max_framesize (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:340
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo17set_max_framesizeEj";

      procedure set_sample_rate (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:341
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo15set_sample_rateEj";

      procedure set_channels (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:342
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo12set_channelsEj";

      procedure set_bits_per_sample (this : access StreamInfo'Class; value : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:343
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo19set_bits_per_sampleEj";

      procedure set_total_samples (this : access StreamInfo'Class; value : FLAC_ordinals_h.FLAC_u_uint64)  -- ../include/FLAC++/metadata.h:344
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo17set_total_samplesEy";

      procedure set_md5sum (this : access StreamInfo'Class; value : access unsigned_char)  -- ../include/FLAC++/metadata.h:345
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata10StreamInfo10set_md5sumEPKh";
   end;
   use Class_StreamInfo;
   package Class_Padding is
      type Padding is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_Padding return Padding;  -- ../include/FLAC++/metadata.h:355
      pragma CPP_Constructor (New_Padding, "_ZN4FLAC8Metadata7PaddingC1Ev");

      function New_Padding (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Padding;  -- ../include/FLAC++/metadata.h:362
      pragma CPP_Constructor (New_Padding, "_ZN4FLAC8Metadata7PaddingC1ERK20FLAC__StreamMetadata");

      function New_Padding (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Padding;  -- ../include/FLAC++/metadata.h:363
      pragma CPP_Constructor (New_Padding, "_ZN4FLAC8Metadata7PaddingC1EPK20FLAC__StreamMetadata");

      function New_Padding (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return Padding;  -- ../include/FLAC++/metadata.h:369
      pragma CPP_Constructor (New_Padding, "_ZN4FLAC8Metadata7PaddingC1EP20FLAC__StreamMetadatab");

      function New_Padding (length : utypes_uuint32_t_h.uint32_t) return Padding;  -- ../include/FLAC++/metadata.h:373
      pragma CPP_Constructor (New_Padding, "_ZN4FLAC8Metadata7PaddingC1Ej");

      procedure Delete_Padding (this : access Padding)  -- ../include/FLAC++/metadata.h:375
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PaddingD1Ev";

      procedure Delete_And_Free_Padding (this : access Padding)  -- ../include/FLAC++/metadata.h:375
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PaddingD0Ev";

      function Assign_Padding (this : access Padding'Class; object : access constant Padding'Class) return access Padding  -- ../include/FLAC++/metadata.h:379
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PaddingaSERKS1_";

      function Assign_Padding (this : access Padding'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Padding  -- ../include/FLAC++/metadata.h:380
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PaddingaSERK20FLAC__StreamMetadata";

      function Assign_Padding (this : access Padding'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Padding  -- ../include/FLAC++/metadata.h:381
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PaddingaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access Padding'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access Padding  -- ../include/FLAC++/metadata.h:387
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Padding6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant Padding'Class; object : access constant Padding'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:391
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PaddingeqERKS1_";

      function operator_eq (this : access constant Padding'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:392
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PaddingeqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant Padding'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:393
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PaddingeqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Padding'Class; object : access constant Padding'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:398
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PaddingneERKS1_";

      function operator_ne (this : access constant Padding'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:399
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PaddingneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Padding'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:400
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PaddingneEPK20FLAC__StreamMetadata";

      procedure set_length (this : access Padding'Class; length : utypes_uuint32_t_h.uint32_t)  -- ../include/FLAC++/metadata.h:405
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Padding10set_lengthEj";
   end;
   use Class_Padding;
   package Class_Application is
      type Application is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_Application return Application;  -- ../include/FLAC++/metadata.h:414
      pragma CPP_Constructor (New_Application, "_ZN4FLAC8Metadata11ApplicationC1Ev");

      function New_Application (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Application;  -- ../include/FLAC++/metadata.h:421
      pragma CPP_Constructor (New_Application, "_ZN4FLAC8Metadata11ApplicationC1ERK20FLAC__StreamMetadata");

      function New_Application (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Application;  -- ../include/FLAC++/metadata.h:422
      pragma CPP_Constructor (New_Application, "_ZN4FLAC8Metadata11ApplicationC1EPK20FLAC__StreamMetadata");

      function New_Application (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return Application;  -- ../include/FLAC++/metadata.h:428
      pragma CPP_Constructor (New_Application, "_ZN4FLAC8Metadata11ApplicationC1EP20FLAC__StreamMetadatab");

      procedure Delete_Application (this : access Application)  -- ../include/FLAC++/metadata.h:430
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11ApplicationD1Ev";

      procedure Delete_And_Free_Application (this : access Application)  -- ../include/FLAC++/metadata.h:430
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11ApplicationD0Ev";

      function Assign_Application (this : access Application'Class; object : access constant Application'Class) return access Application  -- ../include/FLAC++/metadata.h:434
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11ApplicationaSERKS1_";

      function Assign_Application (this : access Application'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Application  -- ../include/FLAC++/metadata.h:435
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11ApplicationaSERK20FLAC__StreamMetadata";

      function Assign_Application (this : access Application'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Application  -- ../include/FLAC++/metadata.h:436
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11ApplicationaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access Application'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access Application  -- ../include/FLAC++/metadata.h:442
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11Application6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant Application'Class; object : access constant Application'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:446
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11ApplicationeqERKS1_";

      function operator_eq (this : access constant Application'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:447
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11ApplicationeqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant Application'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:448
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11ApplicationeqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Application'Class; object : access constant Application'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:453
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11ApplicationneERKS1_";

      function operator_ne (this : access constant Application'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:454
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11ApplicationneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Application'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:455
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11ApplicationneEPK20FLAC__StreamMetadata";

      function get_id (this : access constant Application'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:458
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11Application6get_idEv";

      function get_data (this : access constant Application'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:459
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata11Application8get_dataEv";

      procedure set_id (this : access Application'Class; value : access unsigned_char)  -- ../include/FLAC++/metadata.h:461
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11Application6set_idEPKh";

      function set_data
        (this : access Application'Class;
         data : access unsigned_char;
         length : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:463
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11Application8set_dataEPKhj";

      function set_data
        (this : access Application'Class;
         data : access unsigned_char;
         length : utypes_uuint32_t_h.uint32_t;
         copy : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:464
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata11Application8set_dataEPhjb";
   end;
   use Class_Application;
   package Class_SeekTable is
      type SeekTable is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_SeekTable return SeekTable;  -- ../include/FLAC++/metadata.h:473
      pragma CPP_Constructor (New_SeekTable, "_ZN4FLAC8Metadata9SeekTableC1Ev");

      function New_SeekTable (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return SeekTable;  -- ../include/FLAC++/metadata.h:480
      pragma CPP_Constructor (New_SeekTable, "_ZN4FLAC8Metadata9SeekTableC1ERK20FLAC__StreamMetadata");

      function New_SeekTable (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return SeekTable;  -- ../include/FLAC++/metadata.h:481
      pragma CPP_Constructor (New_SeekTable, "_ZN4FLAC8Metadata9SeekTableC1EPK20FLAC__StreamMetadata");

      function New_SeekTable (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return SeekTable;  -- ../include/FLAC++/metadata.h:487
      pragma CPP_Constructor (New_SeekTable, "_ZN4FLAC8Metadata9SeekTableC1EP20FLAC__StreamMetadatab");

      procedure Delete_SeekTable (this : access SeekTable)  -- ../include/FLAC++/metadata.h:489
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTableD1Ev";

      procedure Delete_And_Free_SeekTable (this : access SeekTable)  -- ../include/FLAC++/metadata.h:489
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTableD0Ev";

      function Assign_SeekTable (this : access SeekTable'Class; object : access constant SeekTable'Class) return access SeekTable  -- ../include/FLAC++/metadata.h:493
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTableaSERKS1_";

      function Assign_SeekTable (this : access SeekTable'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access SeekTable  -- ../include/FLAC++/metadata.h:494
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTableaSERK20FLAC__StreamMetadata";

      function Assign_SeekTable (this : access SeekTable'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access SeekTable  -- ../include/FLAC++/metadata.h:495
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTableaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access SeekTable'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access SeekTable  -- ../include/FLAC++/metadata.h:501
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant SeekTable'Class; object : access constant SeekTable'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:505
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTableeqERKS1_";

      function operator_eq (this : access constant SeekTable'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:506
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTableeqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant SeekTable'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:507
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTableeqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant SeekTable'Class; object : access constant SeekTable'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:512
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTableneERKS1_";

      function operator_ne (this : access constant SeekTable'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:513
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTableneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant SeekTable'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:514
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTableneEPK20FLAC__StreamMetadata";

      function get_num_points (this : access constant SeekTable'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:517
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTable14get_num_pointsEv";

      function get_point (this : access constant SeekTable'Class; index : utypes_uuint32_t_h.uint32_t) return FLAC_format_h.FLAC_u_StreamMetadata_SeekPoint  -- ../include/FLAC++/metadata.h:518
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTable9get_pointEj";

      function resize_points (this : access SeekTable'Class; new_num_points : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:521
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable13resize_pointsEj";

      procedure set_point
        (this : access SeekTable'Class;
         index : utypes_uuint32_t_h.uint32_t;
         point : access constant FLAC_format_h.FLAC_u_StreamMetadata_SeekPoint)  -- ../include/FLAC++/metadata.h:524
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable9set_pointEjRK30FLAC__StreamMetadata_SeekPoint";

      function insert_point
        (this : access SeekTable'Class;
         index : utypes_uuint32_t_h.uint32_t;
         point : access constant FLAC_format_h.FLAC_u_StreamMetadata_SeekPoint) return Extensions.bool  -- ../include/FLAC++/metadata.h:527
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable12insert_pointEjRK30FLAC__StreamMetadata_SeekPoint";

      function delete_point (this : access SeekTable'Class; index : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:530
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable12delete_pointEj";

      function is_legal (this : access constant SeekTable'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:533
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata9SeekTable8is_legalEv";

      function template_append_placeholders (this : access SeekTable'Class; num : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:536
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable28template_append_placeholdersEj";

      function template_append_point (this : access SeekTable'Class; sample_number : FLAC_ordinals_h.FLAC_u_uint64) return Extensions.bool  -- ../include/FLAC++/metadata.h:539
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable21template_append_pointEy";

      function template_append_points
        (this : access SeekTable'Class;
         sample_numbers : access Extensions.unsigned_long_long;
         num : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:542
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable22template_append_pointsEPyj";

      function template_append_spaced_points
        (this : access SeekTable'Class;
         num : utypes_uuint32_t_h.uint32_t;
         total_samples : FLAC_ordinals_h.FLAC_u_uint64) return Extensions.bool  -- ../include/FLAC++/metadata.h:545
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable29template_append_spaced_pointsEjy";

      function template_append_spaced_points_by_samples
        (this : access SeekTable'Class;
         samples : utypes_uuint32_t_h.uint32_t;
         total_samples : FLAC_ordinals_h.FLAC_u_uint64) return Extensions.bool  -- ../include/FLAC++/metadata.h:548
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable40template_append_spaced_points_by_samplesEjy";

      function template_sort (this : access SeekTable'Class; compact : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:551
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata9SeekTable13template_sortEb";
   end;
   use Class_SeekTable;
   package Class_VorbisComment is
      type c_Entry is record
         is_valid_u : aliased Extensions.bool;  -- ../include/FLAC++/metadata.h:622
         entry_u : aliased FLAC_format_h.FLAC_u_StreamMetadata_VorbisComment_Entry;  -- ../include/FLAC++/metadata.h:623
         field_name_u : Interfaces.C.Strings.chars_ptr;  -- ../include/FLAC++/metadata.h:624
         field_name_length_u : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC++/metadata.h:625
         field_value_u : Interfaces.C.Strings.chars_ptr;  -- ../include/FLAC++/metadata.h:626
         field_value_length_u : aliased utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC++/metadata.h:627
      end record
      with Convention => C_Pass_By_Copy;
      type VorbisComment is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_VorbisComment return VorbisComment;  -- ../include/FLAC++/metadata.h:642
      pragma CPP_Constructor (New_VorbisComment, "_ZN4FLAC8Metadata13VorbisCommentC1Ev");

      function New_VorbisComment (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return VorbisComment;  -- ../include/FLAC++/metadata.h:649
      pragma CPP_Constructor (New_VorbisComment, "_ZN4FLAC8Metadata13VorbisCommentC1ERK20FLAC__StreamMetadata");

      function New_VorbisComment (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return VorbisComment;  -- ../include/FLAC++/metadata.h:650
      pragma CPP_Constructor (New_VorbisComment, "_ZN4FLAC8Metadata13VorbisCommentC1EPK20FLAC__StreamMetadata");

      function New_VorbisComment (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return VorbisComment;  -- ../include/FLAC++/metadata.h:656
      pragma CPP_Constructor (New_VorbisComment, "_ZN4FLAC8Metadata13VorbisCommentC1EP20FLAC__StreamMetadatab");

      procedure Delete_VorbisComment (this : access VorbisComment)  -- ../include/FLAC++/metadata.h:658
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisCommentD1Ev";

      procedure Delete_And_Free_VorbisComment (this : access VorbisComment)  -- ../include/FLAC++/metadata.h:658
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisCommentD0Ev";

      function Assign_VorbisComment (this : access VorbisComment'Class; object : access constant VorbisComment'Class) return access VorbisComment  -- ../include/FLAC++/metadata.h:662
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisCommentaSERKS1_";

      function Assign_VorbisComment (this : access VorbisComment'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access VorbisComment  -- ../include/FLAC++/metadata.h:663
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisCommentaSERK20FLAC__StreamMetadata";

      function Assign_VorbisComment (this : access VorbisComment'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access VorbisComment  -- ../include/FLAC++/metadata.h:664
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisCommentaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access VorbisComment'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access VorbisComment  -- ../include/FLAC++/metadata.h:670
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant VorbisComment'Class; object : access constant VorbisComment'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:674
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisCommenteqERKS1_";

      function operator_eq (this : access constant VorbisComment'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:675
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisCommenteqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant VorbisComment'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:676
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisCommenteqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant VorbisComment'Class; object : access constant VorbisComment'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:681
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisCommentneERKS1_";

      function operator_ne (this : access constant VorbisComment'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:682
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisCommentneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant VorbisComment'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:683
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisCommentneEPK20FLAC__StreamMetadata";

      function get_num_comments (this : access constant VorbisComment'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:686
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisComment16get_num_commentsEv";

      function get_vendor_string (this : access constant VorbisComment'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:687
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisComment17get_vendor_stringEv";

      function get_comment (this : access constant VorbisComment'Class; index : utypes_uuint32_t_h.uint32_t) return c_Entry  -- ../include/FLAC++/metadata.h:688
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata13VorbisComment11get_commentEj";

      function set_vendor_string (this : access VorbisComment'Class; string : access unsigned_char) return Extensions.bool  -- ../include/FLAC++/metadata.h:691
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment17set_vendor_stringEPKh";

      function resize_comments (this : access VorbisComment'Class; new_num_comments : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:694
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment15resize_commentsEj";

      function set_comment
        (this : access VorbisComment'Class;
         index : utypes_uuint32_t_h.uint32_t;
         c_entry : access constant c_Entry'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:697
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment11set_commentEjRKNS1_5EntryE";

      function insert_comment
        (this : access VorbisComment'Class;
         index : utypes_uuint32_t_h.uint32_t;
         c_entry : access constant c_Entry'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:700
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment14insert_commentEjRKNS1_5EntryE";

      function append_comment (this : access VorbisComment'Class; c_entry : access constant c_Entry'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:703
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment14append_commentERKNS1_5EntryE";

      function replace_comment
        (this : access VorbisComment'Class;
         c_entry : access constant c_Entry'Class;
         c_all : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:706
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment15replace_commentERKNS1_5EntryEb";

      function delete_comment (this : access VorbisComment'Class; index : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:709
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment14delete_commentEj";

      function find_entry_from
        (this : access VorbisComment'Class;
         offset : utypes_uuint32_t_h.uint32_t;
         field_name : Interfaces.C.Strings.chars_ptr) return int  -- ../include/FLAC++/metadata.h:712
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment15find_entry_fromEjPKc";

      function remove_entry_matching (this : access VorbisComment'Class; field_name : Interfaces.C.Strings.chars_ptr) return int  -- ../include/FLAC++/metadata.h:715
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment21remove_entry_matchingEPKc";

      function remove_entries_matching (this : access VorbisComment'Class; field_name : Interfaces.C.Strings.chars_ptr) return int  -- ../include/FLAC++/metadata.h:718
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata13VorbisComment23remove_entries_matchingEPKc";
   end;
   use Class_VorbisComment;
   package Class_CueSheet is
      type Track is record
         object_u : access FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Track;  -- ../include/FLAC++/metadata.h:735
      end record
      with Convention => C_Pass_By_Copy;
      type CueSheet is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_CueSheet return CueSheet;  -- ../include/FLAC++/metadata.h:769
      pragma CPP_Constructor (New_CueSheet, "_ZN4FLAC8Metadata8CueSheetC1Ev");

      function New_CueSheet (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return CueSheet;  -- ../include/FLAC++/metadata.h:776
      pragma CPP_Constructor (New_CueSheet, "_ZN4FLAC8Metadata8CueSheetC1ERK20FLAC__StreamMetadata");

      function New_CueSheet (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return CueSheet;  -- ../include/FLAC++/metadata.h:777
      pragma CPP_Constructor (New_CueSheet, "_ZN4FLAC8Metadata8CueSheetC1EPK20FLAC__StreamMetadata");

      function New_CueSheet (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return CueSheet;  -- ../include/FLAC++/metadata.h:783
      pragma CPP_Constructor (New_CueSheet, "_ZN4FLAC8Metadata8CueSheetC1EP20FLAC__StreamMetadatab");

      procedure Delete_CueSheet (this : access CueSheet)  -- ../include/FLAC++/metadata.h:785
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheetD1Ev";

      procedure Delete_And_Free_CueSheet (this : access CueSheet)  -- ../include/FLAC++/metadata.h:785
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheetD0Ev";

      function Assign_CueSheet (this : access CueSheet'Class; object : access constant CueSheet'Class) return access CueSheet  -- ../include/FLAC++/metadata.h:789
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheetaSERKS1_";

      function Assign_CueSheet (this : access CueSheet'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access CueSheet  -- ../include/FLAC++/metadata.h:790
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheetaSERK20FLAC__StreamMetadata";

      function Assign_CueSheet (this : access CueSheet'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access CueSheet  -- ../include/FLAC++/metadata.h:791
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheetaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access CueSheet'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access CueSheet  -- ../include/FLAC++/metadata.h:797
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant CueSheet'Class; object : access constant CueSheet'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:801
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheeteqERKS1_";

      function operator_eq (this : access constant CueSheet'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:802
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheeteqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant CueSheet'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:803
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheeteqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant CueSheet'Class; object : access constant CueSheet'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:808
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheetneERKS1_";

      function operator_ne (this : access constant CueSheet'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:809
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheetneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant CueSheet'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:810
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheetneEPK20FLAC__StreamMetadata";

      function get_media_catalog_number (this : access constant CueSheet'Class) return Interfaces.C.Strings.chars_ptr  -- ../include/FLAC++/metadata.h:813
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet24get_media_catalog_numberEv";

      function get_lead_in (this : access constant CueSheet'Class) return FLAC_ordinals_h.FLAC_u_uint64  -- ../include/FLAC++/metadata.h:814
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet11get_lead_inEv";

      function get_is_cd (this : access constant CueSheet'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:815
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet9get_is_cdEv";

      function get_num_tracks (this : access constant CueSheet'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:817
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet14get_num_tracksEv";

      function get_track (this : access constant CueSheet'Class; i : utypes_uuint32_t_h.uint32_t) return Track  -- ../include/FLAC++/metadata.h:818
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet9get_trackEj";

      procedure set_media_catalog_number (this : access CueSheet'Class; value : Interfaces.C.Strings.chars_ptr)  -- ../include/FLAC++/metadata.h:820
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet24set_media_catalog_numberEPKc";

      procedure set_lead_in (this : access CueSheet'Class; value : FLAC_ordinals_h.FLAC_u_uint64)  -- ../include/FLAC++/metadata.h:821
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet11set_lead_inEy";

      procedure set_is_cd (this : access CueSheet'Class; value : Extensions.bool)  -- ../include/FLAC++/metadata.h:822
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet9set_is_cdEb";

      procedure set_index
        (this : access CueSheet'Class;
         track_num : utypes_uuint32_t_h.uint32_t;
         index_num : utypes_uuint32_t_h.uint32_t;
         index : access constant FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Index)  -- ../include/FLAC++/metadata.h:824
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet9set_indexEjjRK35FLAC__StreamMetadata_CueSheet_Index";

      function resize_indices
        (this : access CueSheet'Class;
         track_num : utypes_uuint32_t_h.uint32_t;
         new_num_indices : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:827
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet14resize_indicesEjj";

      function insert_index
        (this : access CueSheet'Class;
         track_num : utypes_uuint32_t_h.uint32_t;
         index_num : utypes_uuint32_t_h.uint32_t;
         index : access constant FLAC_format_h.FLAC_u_StreamMetadata_CueSheet_Index) return Extensions.bool  -- ../include/FLAC++/metadata.h:830
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet12insert_indexEjjRK35FLAC__StreamMetadata_CueSheet_Index";

      function insert_blank_index
        (this : access CueSheet'Class;
         track_num : utypes_uuint32_t_h.uint32_t;
         index_num : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:833
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet18insert_blank_indexEjj";

      function delete_index
        (this : access CueSheet'Class;
         track_num : utypes_uuint32_t_h.uint32_t;
         index_num : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:836
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet12delete_indexEjj";

      function resize_tracks (this : access CueSheet'Class; new_num_tracks : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:839
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet13resize_tracksEj";

      function set_track
        (this : access CueSheet'Class;
         i : utypes_uuint32_t_h.uint32_t;
         track : access constant Track'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:842
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet9set_trackEjRKNS1_5TrackE";

      function insert_track
        (this : access CueSheet'Class;
         i : utypes_uuint32_t_h.uint32_t;
         track : access constant Track'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:845
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet12insert_trackEjRKNS1_5TrackE";

      function insert_blank_track (this : access CueSheet'Class; i : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:848
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet18insert_blank_trackEj";

      function delete_track (this : access CueSheet'Class; i : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:851
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8CueSheet12delete_trackEj";

      function is_legal
        (this : access constant CueSheet'Class;
         check_cd_da_subset : Extensions.bool;
         violation : System.Address) return Extensions.bool  -- ../include/FLAC++/metadata.h:854
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet8is_legalEbPPKc";

      function calculate_cddb_id (this : access constant CueSheet'Class) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC++/metadata.h:857
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8CueSheet17calculate_cddb_idEv";
   end;
   use Class_CueSheet;
   package Class_Picture is
      type Picture is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_Picture return Picture;  -- ../include/FLAC++/metadata.h:866
      pragma CPP_Constructor (New_Picture, "_ZN4FLAC8Metadata7PictureC1Ev");

      function New_Picture (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Picture;  -- ../include/FLAC++/metadata.h:873
      pragma CPP_Constructor (New_Picture, "_ZN4FLAC8Metadata7PictureC1ERK20FLAC__StreamMetadata");

      function New_Picture (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Picture;  -- ../include/FLAC++/metadata.h:874
      pragma CPP_Constructor (New_Picture, "_ZN4FLAC8Metadata7PictureC1EPK20FLAC__StreamMetadata");

      function New_Picture (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return Picture;  -- ../include/FLAC++/metadata.h:880
      pragma CPP_Constructor (New_Picture, "_ZN4FLAC8Metadata7PictureC1EP20FLAC__StreamMetadatab");

      procedure Delete_Picture (this : access Picture)  -- ../include/FLAC++/metadata.h:882
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PictureD1Ev";

      procedure Delete_And_Free_Picture (this : access Picture)  -- ../include/FLAC++/metadata.h:882
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PictureD0Ev";

      function Assign_Picture (this : access Picture'Class; object : access constant Picture'Class) return access Picture  -- ../include/FLAC++/metadata.h:886
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PictureaSERKS1_";

      function Assign_Picture (this : access Picture'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Picture  -- ../include/FLAC++/metadata.h:887
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PictureaSERK20FLAC__StreamMetadata";

      function Assign_Picture (this : access Picture'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Picture  -- ../include/FLAC++/metadata.h:888
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7PictureaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access Picture'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access Picture  -- ../include/FLAC++/metadata.h:894
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Picture6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant Picture'Class; object : access constant Picture'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:898
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PictureeqERKS1_";

      function operator_eq (this : access constant Picture'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:899
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PictureeqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant Picture'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:900
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PictureeqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Picture'Class; object : access constant Picture'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:905
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PictureneERKS1_";

      function operator_ne (this : access constant Picture'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:906
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PictureneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Picture'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:907
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7PictureneEPK20FLAC__StreamMetadata";

      function get_type (this : access constant Picture'Class) return FLAC_format_h.FLAC_u_StreamMetadata_Picture_Type  -- ../include/FLAC++/metadata.h:910
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture8get_typeEv";

      function get_mime_type (this : access constant Picture'Class) return Interfaces.C.Strings.chars_ptr  -- ../include/FLAC++/metadata.h:911
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture13get_mime_typeEv";

      function get_description (this : access constant Picture'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:912
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture15get_descriptionEv";

      function get_width (this : access constant Picture'Class) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC++/metadata.h:913
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture9get_widthEv";

      function get_height (this : access constant Picture'Class) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC++/metadata.h:914
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture10get_heightEv";

      function get_depth (this : access constant Picture'Class) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC++/metadata.h:915
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture9get_depthEv";

      function get_colors (this : access constant Picture'Class) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC++/metadata.h:916
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture10get_colorsEv";

      function get_data_length (this : access constant Picture'Class) return FLAC_ordinals_h.FLAC_u_uint32  -- ../include/FLAC++/metadata.h:917
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture15get_data_lengthEv";

      function get_data (this : access constant Picture'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:918
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture8get_dataEv";

      procedure set_type (this : access Picture'Class; c_type : FLAC_format_h.FLAC_u_StreamMetadata_Picture_Type)  -- ../include/FLAC++/metadata.h:920
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Picture8set_typeE33FLAC__StreamMetadata_Picture_Type";

      function set_mime_type (this : access Picture'Class; string : Interfaces.C.Strings.chars_ptr) return Extensions.bool  -- ../include/FLAC++/metadata.h:923
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Picture13set_mime_typeEPKc";

      function set_description (this : access Picture'Class; string : access unsigned_char) return Extensions.bool  -- ../include/FLAC++/metadata.h:926
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Picture15set_descriptionEPKh";

      procedure set_width (this : access constant Picture'Class; value : FLAC_ordinals_h.FLAC_u_uint32)  -- ../include/FLAC++/metadata.h:928
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture9set_widthEj";

      procedure set_height (this : access constant Picture'Class; value : FLAC_ordinals_h.FLAC_u_uint32)  -- ../include/FLAC++/metadata.h:929
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture10set_heightEj";

      procedure set_depth (this : access constant Picture'Class; value : FLAC_ordinals_h.FLAC_u_uint32)  -- ../include/FLAC++/metadata.h:930
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture9set_depthEj";

      procedure set_colors (this : access constant Picture'Class; value : FLAC_ordinals_h.FLAC_u_uint32)  -- ../include/FLAC++/metadata.h:931
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Picture10set_colorsEj";

      function set_data
        (this : access Picture'Class;
         data : access unsigned_char;
         data_length : FLAC_ordinals_h.FLAC_u_uint32) return Extensions.bool  -- ../include/FLAC++/metadata.h:934
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Picture8set_dataEPKhj";

      function is_legal (this : access Picture'Class; violation : System.Address) return Extensions.bool  -- ../include/FLAC++/metadata.h:937
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Picture8is_legalEPPKc";
   end;
   use Class_Picture;
   package Class_Unknown is
      type Unknown is limited new Prototype with record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_Unknown return Unknown;  -- ../include/FLAC++/metadata.h:948
      pragma CPP_Constructor (New_Unknown, "_ZN4FLAC8Metadata7UnknownC1Ev");

      function New_Unknown (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Unknown;  -- ../include/FLAC++/metadata.h:955
      pragma CPP_Constructor (New_Unknown, "_ZN4FLAC8Metadata7UnknownC1ERK20FLAC__StreamMetadata");

      function New_Unknown (object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Unknown;  -- ../include/FLAC++/metadata.h:956
      pragma CPP_Constructor (New_Unknown, "_ZN4FLAC8Metadata7UnknownC1EPK20FLAC__StreamMetadata");

      function New_Unknown (object : access FLAC_format_h.FLAC_u_StreamMetadata; copy : Extensions.bool) return Unknown;  -- ../include/FLAC++/metadata.h:962
      pragma CPP_Constructor (New_Unknown, "_ZN4FLAC8Metadata7UnknownC1EP20FLAC__StreamMetadatab");

      procedure Delete_Unknown (this : access Unknown)  -- ../include/FLAC++/metadata.h:964
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7UnknownD1Ev";

      procedure Delete_And_Free_Unknown (this : access Unknown)  -- ../include/FLAC++/metadata.h:964
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7UnknownD0Ev";

      function Assign_Unknown (this : access Unknown'Class; object : access constant Unknown'Class) return access Unknown  -- ../include/FLAC++/metadata.h:968
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7UnknownaSERKS1_";

      function Assign_Unknown (this : access Unknown'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Unknown  -- ../include/FLAC++/metadata.h:969
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7UnknownaSERK20FLAC__StreamMetadata";

      function Assign_Unknown (this : access Unknown'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return access Unknown  -- ../include/FLAC++/metadata.h:970
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7UnknownaSEPK20FLAC__StreamMetadata";

      function assign
        (this : access Unknown'Class;
         object : access FLAC_format_h.FLAC_u_StreamMetadata;
         copy : Extensions.bool) return access Unknown  -- ../include/FLAC++/metadata.h:976
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Unknown6assignEP20FLAC__StreamMetadatab";

      function operator_eq (this : access constant Unknown'Class; object : access constant Unknown'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:980
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7UnknowneqERKS1_";

      function operator_eq (this : access constant Unknown'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:981
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7UnknowneqERK20FLAC__StreamMetadata";

      function operator_eq (this : access constant Unknown'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:982
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7UnknowneqEPK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Unknown'Class; object : access constant Unknown'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:987
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7UnknownneERKS1_";

      function operator_ne (this : access constant Unknown'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:988
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7UnknownneERK20FLAC__StreamMetadata";

      function operator_ne (this : access constant Unknown'Class; object : access constant FLAC_format_h.FLAC_u_StreamMetadata) return Extensions.bool  -- ../include/FLAC++/metadata.h:989
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7UnknownneEPK20FLAC__StreamMetadata";

      function get_data (this : access constant Unknown'Class) return access unsigned_char  -- ../include/FLAC++/metadata.h:992
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata7Unknown8get_dataEv";

      function set_data
        (this : access Unknown'Class;
         data : access unsigned_char;
         length : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:995
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Unknown8set_dataEPKhj";

      function set_data
        (this : access Unknown'Class;
         data : access unsigned_char;
         length : utypes_uuint32_t_h.uint32_t;
         copy : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:996
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata7Unknown8set_dataEPhjb";
   end;
   use Class_Unknown;
   function get_streaminfo (filename : Interfaces.C.Strings.chars_ptr; streaminfo : access StreamInfo'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1014
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata14get_streaminfoEPKcRNS0_10StreamInfoE";

   function get_tags (filename : Interfaces.C.Strings.chars_ptr; tags : System.Address) return Extensions.bool  -- ../include/FLAC++/metadata.h:1016
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata8get_tagsEPKcRPNS0_13VorbisCommentE";

   function get_tags (filename : Interfaces.C.Strings.chars_ptr; tags : access VorbisComment'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1017
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata8get_tagsEPKcRNS0_13VorbisCommentE";

   function get_cuesheet (filename : Interfaces.C.Strings.chars_ptr; cuesheet : System.Address) return Extensions.bool  -- ../include/FLAC++/metadata.h:1019
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata12get_cuesheetEPKcRPNS0_8CueSheetE";

   function get_cuesheet (filename : Interfaces.C.Strings.chars_ptr; cuesheet : access CueSheet'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1020
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata12get_cuesheetEPKcRNS0_8CueSheetE";

   function get_picture
     (filename : Interfaces.C.Strings.chars_ptr;
      picture : System.Address;
      c_type : FLAC_format_h.FLAC_u_StreamMetadata_Picture_Type;
      mime_type : Interfaces.C.Strings.chars_ptr;
      description : access unsigned_char;
      max_width : utypes_uuint32_t_h.uint32_t;
      max_height : utypes_uuint32_t_h.uint32_t;
      max_depth : utypes_uuint32_t_h.uint32_t;
      max_colors : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:1022
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata11get_pictureEPKcRPNS0_7PictureE33FLAC__StreamMetadata_Picture_TypeS2_PKhjjjj";

   function get_picture
     (filename : Interfaces.C.Strings.chars_ptr;
      picture : access Picture'Class;
      c_type : FLAC_format_h.FLAC_u_StreamMetadata_Picture_Type;
      mime_type : Interfaces.C.Strings.chars_ptr;
      description : access unsigned_char;
      max_width : utypes_uuint32_t_h.uint32_t;
      max_height : utypes_uuint32_t_h.uint32_t;
      max_depth : utypes_uuint32_t_h.uint32_t;
      max_colors : utypes_uuint32_t_h.uint32_t) return Extensions.bool  -- ../include/FLAC++/metadata.h:1023
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN4FLAC8Metadata11get_pictureEPKcRNS0_7PictureE33FLAC__StreamMetadata_Picture_TypeS2_PKhjjjj";

   package Class_SimpleIterator is
      type Status is record
         status_u : aliased FLAC_metadata_h.FLAC_u_Metadata_SimpleIteratorStatus;  -- ../include/FLAC++/metadata.h:1072
      end record
      with Convention => C_Pass_By_Copy;
      type SimpleIterator is tagged limited record
         iterator_u : access FLAC_metadata_h.FLAC_u_Metadata_SimpleIterator;  -- ../include/FLAC++/metadata.h:1099
      end record
      with Import => True,
           Convention => CPP;

      function New_SimpleIterator return SimpleIterator;  -- ../include/FLAC++/metadata.h:1075
      pragma CPP_Constructor (New_SimpleIterator, "_ZN4FLAC8Metadata14SimpleIteratorC1Ev");

      procedure Delete_SimpleIterator (this : access SimpleIterator)  -- ../include/FLAC++/metadata.h:1076
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIteratorD1Ev";

      procedure Delete_And_Free_SimpleIterator (this : access SimpleIterator)  -- ../include/FLAC++/metadata.h:1076
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIteratorD0Ev";

      function is_valid (this : access constant SimpleIterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1078
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata14SimpleIterator8is_validEv";

      function init
        (this : access SimpleIterator'Class;
         filename : Interfaces.C.Strings.chars_ptr;
         read_only : Extensions.bool;
         preserve_file_stats : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1080
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator4initEPKcbb";

      function status (this : access SimpleIterator'Class) return Status  -- ../include/FLAC++/metadata.h:1082
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator6statusEv";

      function is_writable (this : access constant SimpleIterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1083
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata14SimpleIterator11is_writableEv";

      function next (this : access SimpleIterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1085
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator4nextEv";

      function prev (this : access SimpleIterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1086
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator4prevEv";

      function is_last (this : access constant SimpleIterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1087
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata14SimpleIterator7is_lastEv";

      function get_block_offset (this : access constant SimpleIterator'Class) return sys_utypes_uoff_t_h.off_t  -- ../include/FLAC++/metadata.h:1089
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata14SimpleIterator16get_block_offsetEv";

      function get_block_type (this : access constant SimpleIterator'Class) return FLAC_format_h.FLAC_u_MetadataType  -- ../include/FLAC++/metadata.h:1090
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata14SimpleIterator14get_block_typeEv";

      function get_block_length (this : access constant SimpleIterator'Class) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC++/metadata.h:1091
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata14SimpleIterator16get_block_lengthEv";

      function get_application_id (this : access SimpleIterator'Class; id : access unsigned_char) return Extensions.bool  -- ../include/FLAC++/metadata.h:1092
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator18get_application_idEPh";

      function get_block (this : access SimpleIterator'Class) return access Prototype  -- ../include/FLAC++/metadata.h:1093
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator9get_blockEv";

      function set_block
        (this : access SimpleIterator'Class;
         block : access Prototype'Class;
         use_padding : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1094
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator9set_blockEPNS0_9PrototypeEb";

      function insert_block_after
        (this : access SimpleIterator'Class;
         block : access Prototype'Class;
         use_padding : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1095
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator18insert_block_afterEPNS0_9PrototypeEb";

      function delete_block (this : access SimpleIterator'Class; use_padding : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1096
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator12delete_blockEb";

      procedure clear (this : access SimpleIterator'Class)  -- ../include/FLAC++/metadata.h:1100
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIterator5clearEv";

      function Assign_SimpleIterator (this : access SimpleIterator'Class; arg2 : access constant SimpleIterator'Class) return access SimpleIterator  -- ../include/FLAC++/metadata.h:1104
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata14SimpleIteratoraSERKS1_";
   end;
   use Class_SimpleIterator;
   package Class_Chain is
      type Status is record
         status_u : aliased FLAC_metadata_h.FLAC_u_Metadata_ChainStatus;  -- ../include/FLAC++/metadata.h:1162
      end record
      with Convention => C_Pass_By_Copy;
      type Chain is tagged limited record
         chain_u : access FLAC_metadata_h.FLAC_u_Metadata_Chain;  -- ../include/FLAC++/metadata.h:1187
      end record
      with Import => True,
           Convention => CPP;

      function New_Chain return Chain;  -- ../include/FLAC++/metadata.h:1165
      pragma CPP_Constructor (New_Chain, "_ZN4FLAC8Metadata5ChainC1Ev");

      procedure Delete_Chain (this : access Chain)  -- ../include/FLAC++/metadata.h:1166
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5ChainD1Ev";

      procedure Delete_And_Free_Chain (this : access Chain)  -- ../include/FLAC++/metadata.h:1166
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5ChainD0Ev";

      function is_valid (this : access constant Chain'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1170
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata5Chain8is_validEv";

      function status (this : access Chain'Class) return Status  -- ../include/FLAC++/metadata.h:1172
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain6statusEv";

      function read
        (this : access Chain'Class;
         filename : Interfaces.C.Strings.chars_ptr;
         is_ogg : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1174
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain4readEPKcb";

      function read
        (this : access Chain'Class;
         handle : FLAC_callback_h.FLAC_u_IOHandle;
         callbacks : FLAC_callback_h.FLAC_u_IOCallbacks;
         is_ogg : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1175
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain4readEPv17FLAC__IOCallbacksb";

      function check_if_tempfile_needed (this : access Chain'Class; use_padding : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1177
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain24check_if_tempfile_neededEb";

      function write
        (this : access Chain'Class;
         use_padding : Extensions.bool;
         preserve_file_stats : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1179
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain5writeEbb";

      function write
        (this : access Chain'Class;
         use_padding : Extensions.bool;
         handle : FLAC_callback_h.FLAC_u_IOHandle;
         callbacks : FLAC_callback_h.FLAC_u_IOCallbacks) return Extensions.bool  -- ../include/FLAC++/metadata.h:1180
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain5writeEbPv17FLAC__IOCallbacks";

      function write
        (this : access Chain'Class;
         use_padding : Extensions.bool;
         handle : FLAC_callback_h.FLAC_u_IOHandle;
         callbacks : FLAC_callback_h.FLAC_u_IOCallbacks;
         temp_handle : FLAC_callback_h.FLAC_u_IOHandle;
         temp_callbacks : FLAC_callback_h.FLAC_u_IOCallbacks) return Extensions.bool  -- ../include/FLAC++/metadata.h:1181
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain5writeEbPv17FLAC__IOCallbacksS2_S3_";

      procedure merge_padding (this : access Chain'Class)  -- ../include/FLAC++/metadata.h:1183
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain13merge_paddingEv";

      procedure sort_padding (this : access Chain'Class)  -- ../include/FLAC++/metadata.h:1184
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain12sort_paddingEv";

      procedure clear (this : access Chain)  -- ../include/FLAC++/metadata.h:1188
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5Chain5clearEv";

      function Assign_Chain (this : access Chain'Class; arg2 : access constant Chain'Class) return access Chain  -- ../include/FLAC++/metadata.h:1192
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata5ChainaSERKS1_";
   end;
   use Class_Chain;
   package Class_Iterator is
      type Iterator is tagged limited record
         iterator_u : access FLAC_metadata_h.FLAC_u_Metadata_Iterator;  -- ../include/FLAC++/metadata.h:1221
      end record
      with Import => True,
           Convention => CPP;

      function New_Iterator return Iterator;  -- ../include/FLAC++/metadata.h:1202
      pragma CPP_Constructor (New_Iterator, "_ZN4FLAC8Metadata8IteratorC1Ev");

      procedure Delete_Iterator (this : access Iterator)  -- ../include/FLAC++/metadata.h:1203
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8IteratorD1Ev";

      procedure Delete_And_Free_Iterator (this : access Iterator)  -- ../include/FLAC++/metadata.h:1203
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8IteratorD0Ev";

      function is_valid (this : access constant Iterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1205
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8Iterator8is_validEv";

      procedure init (this : access Iterator'Class; chain : access Chain'Class)  -- ../include/FLAC++/metadata.h:1208
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator4initERNS0_5ChainE";

      function next (this : access Iterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1210
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator4nextEv";

      function prev (this : access Iterator'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1211
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator4prevEv";

      function get_block_type (this : access constant Iterator'Class) return FLAC_format_h.FLAC_u_MetadataType  -- ../include/FLAC++/metadata.h:1213
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNK4FLAC8Metadata8Iterator14get_block_typeEv";

      function get_block (this : access Iterator'Class) return access Prototype  -- ../include/FLAC++/metadata.h:1214
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator9get_blockEv";

      function set_block (this : access Iterator'Class; block : access Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1215
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator9set_blockEPNS0_9PrototypeE";

      function delete_block (this : access Iterator'Class; replace_with_padding : Extensions.bool) return Extensions.bool  -- ../include/FLAC++/metadata.h:1216
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator12delete_blockEb";

      function insert_block_before (this : access Iterator'Class; block : access Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1217
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator19insert_block_beforeEPNS0_9PrototypeE";

      function insert_block_after (this : access Iterator'Class; block : access Prototype'Class) return Extensions.bool  -- ../include/FLAC++/metadata.h:1218
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator18insert_block_afterEPNS0_9PrototypeE";

      procedure clear (this : access Iterator)  -- ../include/FLAC++/metadata.h:1222
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8Iterator5clearEv";

      function Assign_Iterator (this : access Iterator'Class; arg2 : access constant Iterator'Class) return access Iterator  -- ../include/FLAC++/metadata.h:1226
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN4FLAC8Metadata8IteratoraSERKS1_";
   end;
   use Class_Iterator;
end FLACpp_metadata_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
