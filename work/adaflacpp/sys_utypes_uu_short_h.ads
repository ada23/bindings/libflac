pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package sys_utypes_uu_short_h is

   subtype u_short is unsigned_short;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_u_short.h:30

end sys_utypes_uu_short_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
