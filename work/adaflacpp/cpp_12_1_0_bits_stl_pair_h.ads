pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_12_1_0_bits_stl_pair_h is

   package Class_piecewise_construct_t is
      type piecewise_construct_t is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_piecewise_construct_t return piecewise_construct_t;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stl_pair.h:80
      pragma CPP_Constructor (New_piecewise_construct_t, "_ZNSt21piecewise_construct_tC1Ev");
   end;
   use Class_piecewise_construct_t;
   piecewise_construct : aliased constant piecewise_construct_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/stl_pair.h:83
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt19piecewise_construct";

   package uu_pair_get_1 is
      type uu_pair_get is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_pair_get_1;

   package uu_pair_get_0 is
      type uu_pair_get is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_pair_get_0;



end cpp_12_1_0_bits_stl_pair_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
