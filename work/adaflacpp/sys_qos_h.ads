pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package sys_qos_h is

   QOS_MIN_RELATIVE_PRIORITY : constant := (-15);  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/qos.h:153

   subtype qos_class_t is unsigned;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/qos.h:130

   function qos_class_self return qos_class_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/qos.h:172
   with Import => True, 
        Convention => C, 
        External_Name => "qos_class_self";

   function qos_class_main return qos_class_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/qos.h:194
   with Import => True, 
        Convention => C, 
        External_Name => "qos_class_main";

end sys_qos_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
