pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;
with Interfaces.C.Strings;
with System;

package runetype_h is

   type u_RuneEntry is record
      uu_min : aliased arm_utypes_h.uu_darwin_rune_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:61
      uu_max : aliased arm_utypes_h.uu_darwin_rune_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:62
      uu_map : aliased arm_utypes_h.uu_darwin_rune_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:63
      uu_types : access unsigned;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:64
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:65

   type u_RuneRange is record
      uu_nranges : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:68
      uu_ranges : access u_RuneEntry;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:69
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:70

   subtype anon_array2272 is Interfaces.C.char_array (0 .. 13);
   type u_RuneCharClass is record
      uu_name : aliased anon_array2272;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:73
      uu_mask : aliased arm_utypes_h.uu_uint32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:74
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:75

   subtype anon_array1566 is Interfaces.C.char_array (0 .. 7);
   subtype anon_array2275 is Interfaces.C.char_array (0 .. 31);
   type anon_array2289 is array (0 .. 255) of aliased arm_utypes_h.uu_uint32_t;
   type anon_array2291 is array (0 .. 255) of aliased arm_utypes_h.uu_darwin_rune_t;
   type u_RuneLocale is record
      uu_magic : aliased anon_array1566;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:78
      uu_encoding : aliased anon_array2275;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:79
      uu_sgetrune : access function
           (arg1 : Interfaces.C.Strings.chars_ptr;
            arg2 : arm_utypes_h.uu_darwin_size_t;
            arg3 : System.Address) return arm_utypes_h.uu_darwin_rune_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:81
      uu_sputrune : access function
           (arg1 : arm_utypes_h.uu_darwin_rune_t;
            arg2 : Interfaces.C.Strings.chars_ptr;
            arg3 : arm_utypes_h.uu_darwin_size_t;
            arg4 : System.Address) return int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:82
      uu_invalid_rune : aliased arm_utypes_h.uu_darwin_rune_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:83
      uu_runetype : aliased anon_array2289;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:85
      uu_maplower : aliased anon_array2291;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:86
      uu_mapupper : aliased anon_array2291;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:87
      uu_runetype_ext : aliased u_RuneRange;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:94
      uu_maplower_ext : aliased u_RuneRange;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:95
      uu_mapupper_ext : aliased u_RuneRange;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:96
      uu_variable : System.Address;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:98
      uu_variable_len : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:99
      uu_ncharclasses : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:104
      uu_charclasses : access u_RuneCharClass;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:105
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/runetype.h:106

end runetype_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
