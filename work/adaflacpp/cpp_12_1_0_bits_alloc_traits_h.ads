pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with cpp_12_1_0_type_traits;

package cpp_12_1_0_bits_alloc_traits_h is

   type uu_allocator_traits_base is record
      null;
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/alloc_traits.h:49

   package uu_is_allocator_Class_allocator.allocator_address is
      type uu_is_allocator is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_allocator_Class_allocator.allocator_address;

   package uu_is_allocator_Class_allocator.allocator_address is
      type uu_is_allocator is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_allocator_Class_allocator.allocator_address;

   package uu_is_allocator_Class_allocator.allocator_address is
      type uu_is_allocator is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_allocator_Class_allocator.allocator_address;

   package uu_is_allocator_Class_allocator.allocator_address is
      type uu_is_allocator is limited record
         parent : aliased cpp_12_1_0_type_traits.Class_integral_constant.integral_constant;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use uu_is_allocator_Class_allocator.allocator_address;



end cpp_12_1_0_bits_alloc_traits_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
