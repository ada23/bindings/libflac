pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with utypes_uuint16_t_h;
with utypes_uuint32_t_h;
with utypes_uuint64_t_h;
with System;
with sys_utypes_uuintptr_t_h;

package libkern_arm_OSByteOrder_h is

   --  skipped func _OSSwapInt16

   --  skipped func _OSSwapInt32

   --  skipped func _OSSwapInt64

   pragma Compile_Time_Warning (True, "packed layout may be incorrect");
   type u_OSUnalignedU16 is record
      uu_val : utypes_uuint16_t_h.uint16_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:65
   end record
   with Convention => C_Pass_By_Copy,
        Pack => True,
        Alignment => 1;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:64

   pragma Compile_Time_Warning (True, "packed layout may be incorrect");
   type u_OSUnalignedU32 is record
      uu_val : utypes_uuint32_t_h.uint32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:69
   end record
   with Convention => C_Pass_By_Copy,
        Pack => True,
        Alignment => 1;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:68

   pragma Compile_Time_Warning (True, "packed layout may be incorrect");
   type u_OSUnalignedU64 is record
      uu_val : utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:73
   end record
   with Convention => C_Pass_By_Copy,
        Pack => True,
        Alignment => 1;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:72

   function OSReadSwapInt16 (u_base : System.Address; u_offset : sys_utypes_uuintptr_t_h.uintptr_t) return utypes_uuint16_t_h.uint16_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:89
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL15OSReadSwapInt16PVKvm";

   function OSReadSwapInt32 (u_base : System.Address; u_offset : sys_utypes_uuintptr_t_h.uintptr_t) return utypes_uuint32_t_h.uint32_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:111
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL15OSReadSwapInt32PVKvm";

   function OSReadSwapInt64 (u_base : System.Address; u_offset : sys_utypes_uuintptr_t_h.uintptr_t) return utypes_uuint64_t_h.uint64_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:133
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL15OSReadSwapInt64PVKvm";

   procedure OSWriteSwapInt16
     (u_base : System.Address;
      u_offset : sys_utypes_uuintptr_t_h.uintptr_t;
      u_data : utypes_uuint16_t_h.uint16_t)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:158
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL16OSWriteSwapInt16PVvmt";

   procedure OSWriteSwapInt32
     (u_base : System.Address;
      u_offset : sys_utypes_uuintptr_t_h.uintptr_t;
      u_data : utypes_uuint32_t_h.uint32_t)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:182
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL16OSWriteSwapInt32PVvmj";

   procedure OSWriteSwapInt64
     (u_base : System.Address;
      u_offset : sys_utypes_uuintptr_t_h.uintptr_t;
      u_data : utypes_uuint64_t_h.uint64_t)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/libkern/arm/OSByteOrder.h:206
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZL16OSWriteSwapInt64PVvmy";

end libkern_arm_OSByteOrder_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
