pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with sys_upthread_upthread_types_h;
limited with pthread_sched_h;
with System;
with sys_utypes_usize_t_h;
with sys_upthread_upthread_t_h;
limited with sys_utypes_utimespec_h;
with sys_upthread_upthread_key_t_h;
with Interfaces.C.Extensions;
with Interfaces.C.Strings;
with sys_utypes_umach_port_t_h;

package pthread_h is

   --  arg-macro: procedure pthread_cleanup_push (func, val)
   --    { struct __darwin_pthread_handler_rec __handler; pthread_t __self := pthread_self(); __handler.__routine := func; __handler.__arg := val; __handler.__next := __self.__cleanup_stack; __self.__cleanup_stack := and__handler;
   --  arg-macro: procedure pthread_cleanup_pop (execute)
   --    __self.__cleanup_stack := __handler.__next; if (execute) (__handler.__routine)(__handler.__arg); }
   PTHREAD_CREATE_JOINABLE : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:135
   PTHREAD_CREATE_DETACHED : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:136

   PTHREAD_INHERIT_SCHED : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:138
   PTHREAD_EXPLICIT_SCHED : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:139

   PTHREAD_CANCEL_ENABLE : constant := 16#01#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:141
   PTHREAD_CANCEL_DISABLE : constant := 16#00#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:142
   PTHREAD_CANCEL_DEFERRED : constant := 16#02#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:143
   PTHREAD_CANCEL_ASYNCHRONOUS : constant := 16#00#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:144
   --  unsupported macro: PTHREAD_CANCELED ((void *) 1)

   PTHREAD_SCOPE_SYSTEM : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:150
   PTHREAD_SCOPE_PROCESS : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:151

   PTHREAD_PROCESS_SHARED : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:153
   PTHREAD_PROCESS_PRIVATE : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:154

   PTHREAD_PRIO_NONE : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:159
   PTHREAD_PRIO_INHERIT : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:160
   PTHREAD_PRIO_PROTECT : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:161

   PTHREAD_MUTEX_NORMAL : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:166
   PTHREAD_MUTEX_ERRORCHECK : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:167
   PTHREAD_MUTEX_RECURSIVE : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:168
   --  unsupported macro: PTHREAD_MUTEX_DEFAULT PTHREAD_MUTEX_NORMAL

   PTHREAD_MUTEX_POLICY_FAIRSHARE_NP : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:174
   PTHREAD_MUTEX_POLICY_FIRSTFIT_NP : constant := 3;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:175
   --  unsupported macro: PTHREAD_RWLOCK_INITIALIZER {_PTHREAD_RWLOCK_SIG_init, {0}}
   --  unsupported macro: PTHREAD_MUTEX_INITIALIZER {_PTHREAD_MUTEX_SIG_init, {0}}
   --  unsupported macro: PTHREAD_COND_INITIALIZER {_PTHREAD_COND_SIG_init, {0}}
   --  unsupported macro: PTHREAD_ONCE_INIT {_PTHREAD_ONCE_SIG_init, {0}}
   --  unsupported macro: PTHREAD_JIT_WRITE_ALLOW_CALLBACKS_NP(...) __attribute__((__used__, __section__("__DATA_CONST,__pth_jit_func"))) static const pthread_jit_write_callback_t __pthread_jit_write_callback_allowlist[] = { __VA_ARGS__, NULL }

   function pthread_atfork
     (arg1 : access procedure;
      arg2 : access procedure;
      arg3 : access procedure) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:226
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_atfork";

   function pthread_attr_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:230
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_destroy";

   function pthread_attr_getdetachstate (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:233
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getdetachstate";

   function pthread_attr_getguardsize (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access unsigned_long) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:236
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getguardsize";

   function pthread_attr_getinheritsched (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:239
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getinheritsched";

   function pthread_attr_getschedparam (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access pthread_sched_h.sched_param) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:242
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getschedparam";

   function pthread_attr_getschedpolicy (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:246
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getschedpolicy";

   function pthread_attr_getscope (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:249
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getscope";

   function pthread_attr_getstack
     (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t;
      arg2 : System.Address;
      arg3 : access unsigned_long) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:252
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getstack";

   function pthread_attr_getstackaddr (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:256
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getstackaddr";

   function pthread_attr_getstacksize (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access unsigned_long) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:260
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_getstacksize";

   function pthread_attr_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:263
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_init";

   function pthread_attr_setdetachstate (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:266
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setdetachstate";

   function pthread_attr_setguardsize (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:269
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setguardsize";

   function pthread_attr_setinheritsched (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:272
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setinheritsched";

   function pthread_attr_setschedparam (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : access constant pthread_sched_h.sched_param) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:275
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setschedparam";

   function pthread_attr_setschedpolicy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:279
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setschedpolicy";

   function pthread_attr_setscope (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:282
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setscope";

   function pthread_attr_setstack
     (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t;
      arg2 : System.Address;
      arg3 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:285
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setstack";

   function pthread_attr_setstackaddr (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:288
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setstackaddr";

   function pthread_attr_setstacksize (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_attr_t; arg2 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:291
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_attr_setstacksize";

   function pthread_cancel (arg1 : sys_upthread_upthread_t_h.pthread_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:294
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_cancel";

   function pthread_cond_broadcast (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:297
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_cond_broadcast";

   function pthread_cond_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:300
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_cond_destroy";

   function pthread_cond_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t; arg2 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_condattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:303
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_cond_init";

   function pthread_cond_signal (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:309
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_cond_signal";

   function pthread_cond_timedwait
     (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t;
      arg2 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t;
      arg3 : access constant sys_utypes_utimespec_h.timespec) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:313
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_cond_timedwait";

   function pthread_cond_wait (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t; arg2 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:320
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_cond_wait";

   function pthread_condattr_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_condattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:324
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_condattr_destroy";

   function pthread_condattr_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_condattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:327
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_condattr_init";

   function pthread_condattr_getpshared (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_condattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:330
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_condattr_getpshared";

   function pthread_condattr_setpshared (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_condattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:334
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_condattr_setpshared";

   function pthread_create
     (arg1 : System.Address;
      arg2 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t;
      arg3 : access function (arg1 : System.Address) return System.Address;
      arg4 : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:338
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_create";

   function pthread_detach (arg1 : sys_upthread_upthread_t_h.pthread_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:349
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_detach";

   function pthread_equal (arg1 : sys_upthread_upthread_t_h.pthread_t; arg2 : sys_upthread_upthread_t_h.pthread_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:352
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_equal";

   procedure pthread_exit (arg1 : System.Address)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:356
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_exit";

   function pthread_getconcurrency return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:359
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_getconcurrency";

   function pthread_getschedparam
     (arg1 : sys_upthread_upthread_t_h.pthread_t;
      arg2 : access int;
      arg3 : access pthread_sched_h.sched_param) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:362
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_getschedparam";

   function pthread_getspecific (arg1 : sys_upthread_upthread_key_t_h.pthread_key_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:367
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_getspecific";

   function pthread_join (arg1 : sys_upthread_upthread_t_h.pthread_t; arg2 : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:371
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_join";

   function pthread_key_create (arg1 : access unsigned_long; arg2 : access procedure (arg1 : System.Address)) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:375
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_key_create";

   function pthread_key_delete (arg1 : sys_upthread_upthread_key_t_h.pthread_key_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:378
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_key_delete";

   function pthread_mutex_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:381
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_destroy";

   function pthread_mutex_getprioceiling (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:384
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_getprioceiling";

   function pthread_mutex_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t; arg2 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:388
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_init";

   function pthread_mutex_lock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:393
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_lock";

   function pthread_mutex_setprioceiling
     (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t;
      arg2 : int;
      arg3 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:396
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_setprioceiling";

   function pthread_mutex_trylock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:401
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_trylock";

   function pthread_mutex_unlock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:405
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutex_unlock";

   function pthread_mutexattr_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:408
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_mutexattr_destroy";

   function pthread_mutexattr_getprioceiling (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:411
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_getprioceiling";

   function pthread_mutexattr_getprotocol (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:415
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_getprotocol";

   function pthread_mutexattr_getpshared (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:419
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_getpshared";

   function pthread_mutexattr_gettype (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:423
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_gettype";

   function pthread_mutexattr_getpolicy_np (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:427
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_getpolicy_np";

   function pthread_mutexattr_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:431
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_init";

   function pthread_mutexattr_setprioceiling (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:434
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_setprioceiling";

   function pthread_mutexattr_setprotocol (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:437
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_setprotocol";

   function pthread_mutexattr_setpshared (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:440
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_setpshared";

   function pthread_mutexattr_settype (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:443
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_settype";

   function pthread_mutexattr_setpolicy_np (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutexattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:446
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mutexattr_setpolicy_np";

   function pthread_once (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_once_t; arg2 : access procedure) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:450
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_once";

   function pthread_rwlock_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:453
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_destroy";

   function pthread_rwlock_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t; arg2 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_rwlockattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:456
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_init";

   function pthread_rwlock_rdlock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:462
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_rdlock";

   function pthread_rwlock_tryrdlock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:466
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_tryrdlock";

   function pthread_rwlock_trywrlock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:470
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_trywrlock";

   function pthread_rwlock_wrlock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:474
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_wrlock";

   function pthread_rwlock_unlock (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlock_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:478
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_rwlock_unlock";

   function pthread_rwlockattr_destroy (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlockattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:481
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_rwlockattr_destroy";

   function pthread_rwlockattr_getpshared (arg1 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_rwlockattr_t; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:484
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_rwlockattr_getpshared";

   function pthread_rwlockattr_init (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlockattr_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:488
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_rwlockattr_init";

   function pthread_rwlockattr_setpshared (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_rwlockattr_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:491
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_rwlockattr_setpshared";

   function pthread_self return sys_upthread_upthread_t_h.pthread_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:494
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_self";

   function pthread_setcancelstate (arg1 : int; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:498
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_setcancelstate";

   function pthread_setcanceltype (arg1 : int; arg2 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:503
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_setcanceltype";

   function pthread_setconcurrency (arg1 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:507
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_setconcurrency";

   function pthread_setschedparam
     (arg1 : sys_upthread_upthread_t_h.pthread_t;
      arg2 : int;
      arg3 : access constant pthread_sched_h.sched_param) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:510
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_setschedparam";

   function pthread_setspecific (arg1 : sys_upthread_upthread_key_t_h.pthread_key_t; arg2 : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:514
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_setspecific";

   procedure pthread_testcancel  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:518
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_testcancel";

   function pthread_is_threaded_np return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:524
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_is_threaded_np";

   function pthread_threadid_np (arg1 : sys_upthread_upthread_t_h.pthread_t; arg2 : access Extensions.unsigned_long_long) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:527
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_threadid_np";

   function pthread_getname_np
     (arg1 : sys_upthread_upthread_t_h.pthread_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:531
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_getname_np";

   function pthread_setname_np (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:535
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_setname_np";

   function pthread_main_np return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:539
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_main_np";

   function pthread_mach_thread_np (arg1 : sys_upthread_upthread_t_h.pthread_t) return sys_utypes_umach_port_t_h.mach_port_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:543
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_mach_thread_np";

   function pthread_get_stacksize_np (arg1 : sys_upthread_upthread_t_h.pthread_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:546
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_get_stacksize_np";

   function pthread_get_stackaddr_np (arg1 : sys_upthread_upthread_t_h.pthread_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:549
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_get_stackaddr_np";

   function pthread_cond_signal_thread_np (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t; arg2 : sys_upthread_upthread_t_h.pthread_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:553
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_cond_signal_thread_np";

   function pthread_cond_timedwait_relative_np
     (arg1 : access sys_upthread_upthread_types_h.u_opaque_pthread_cond_t;
      arg2 : access sys_upthread_upthread_types_h.u_opaque_pthread_mutex_t;
      arg3 : access constant sys_utypes_utimespec_h.timespec) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:558
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_cond_timedwait_relative_np";

   function pthread_create_suspended_np
     (arg1 : System.Address;
      arg2 : access constant sys_upthread_upthread_types_h.u_opaque_pthread_attr_t;
      arg3 : access function (arg1 : System.Address) return System.Address;
      arg4 : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:564
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_create_suspended_np";

   function pthread_kill (arg1 : sys_upthread_upthread_t_h.pthread_t; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:573
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_kill";

   function pthread_from_mach_thread_np (arg1 : sys_utypes_umach_port_t_h.mach_port_t) return sys_upthread_upthread_t_h.pthread_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:576
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_from_mach_thread_np";

   function pthread_sigmask
     (arg1 : int;
      arg2 : access unsigned;
      arg3 : access unsigned) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:579
   with Import => True, 
        Convention => C, 
        External_Name => "_pthread_sigmask";

   procedure pthread_yield_np  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:584
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_yield_np";

   procedure pthread_jit_write_protect_np (enabled : int)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:588
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_jit_write_protect_np";

   function pthread_jit_write_protect_supported_np return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:592
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_jit_write_protect_supported_np";

   type pthread_jit_write_callback_t is access function (arg1 : System.Address) return int
   with Convention => C;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:608

   function pthread_jit_write_with_callback_np (callback : pthread_jit_write_callback_t; ctx : System.Address) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:697
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_jit_write_with_callback_np";

   procedure pthread_jit_write_freeze_callbacks_np  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:727
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_jit_write_freeze_callbacks_np";

   function pthread_cpu_number_np (cpu_number_out : access unsigned_long) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread.h:746
   with Import => True, 
        Convention => C, 
        External_Name => "pthread_cpu_number_np";

end pthread_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
