pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with utypes_h;

package utypes_uwctype_t_h is

   subtype wctype_t is utypes_h.uu_darwin_wctype_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_types/_wctype_t.h:32

end utypes_uwctype_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
