pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package pthread_sched_h is

   subtype anon_array2160 is Interfaces.C.char_array (0 .. 3);
   type sched_param is record
      sched_priority : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/sched.h:35
      uu_opaque : aliased anon_array2160;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/sched.h:35
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/sched.h:35

   function sched_yield return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/sched.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "sched_yield";

   function sched_get_priority_min (arg1 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/sched.h:41
   with Import => True, 
        Convention => C, 
        External_Name => "sched_get_priority_min";

   function sched_get_priority_max (arg1 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/pthread/sched.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "sched_get_priority_max";

end pthread_sched_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
