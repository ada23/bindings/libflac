pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h;

package cpp_12_1_0_bits_char_traits_h is

   package u_Char_types_wchar_t is
      type u_Char_types is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use u_Char_types_wchar_t;

   package u_Char_types_char is
      type u_Char_types is limited record
         null;
      end record
      with Convention => C_Pass_By_Copy

   end;
   use u_Char_types_char;



   package char_traits_wchar_t is
      type char_traits is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      procedure assign (uu_c1 : access char_type; uu_c2 : access char_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:110
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE6assignERwRKw";

      function eq (uu_c1 : access char_type; uu_c2 : access char_type) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE2eqERKwS3_";

      function lt (uu_c1 : access char_type; uu_c2 : access char_type) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:125
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE2ltERKwS3_";

      function compare
        (uu_s1 : access char_type;
         uu_s2 : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:169
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE7compareEPKwS3_m";

      function length (uu_p : access char_type) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE6lengthEPKw";

      function find
        (uu_s : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
         uu_a : access char_type) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:193
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE4findEPKwmRS2_";

      type char_type;
      function move
        (uu_s1 : access char_type;
         uu_s2 : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:205
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE4moveEPwPKwm";

      function copy
        (uu_s1 : access char_type;
         uu_s2 : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:255
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE4copyEPwPKwm";

      function assign
        (uu_s : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
         uu_a : char_type) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:274
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE6assignEPwmw";

      function to_char_type (uu_c : access int_type) return char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:147
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE12to_char_typeERKm";

      type int_type;
      function to_int_type (uu_c : access char_type) return int_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:151
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE11to_int_typeERKw";

      function eq_int_type (uu_c1 : access int_type; uu_c2 : access int_type) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:155
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE11eq_int_typeERKmS3_";

      function eof return int_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:159
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE3eofEv";

      function not_eof (uu_c : access int_type) return int_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:163
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIwE7not_eofERKm";

   end;
   use char_traits_wchar_t;

   package char_traits_char is
      type char_traits is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      procedure assign (uu_c1 : access char_type; uu_c2 : access char_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:110
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE6assignERcRKc";

      function eq (uu_c1 : access char_type; uu_c2 : access char_type) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:121
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE2eqERKcS3_";

      function lt (uu_c1 : access char_type; uu_c2 : access char_type) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:125
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE2ltERKcS3_";

      function compare
        (uu_s1 : access char_type;
         uu_s2 : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return int  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:169
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE7compareEPKcS3_m";

      function length (uu_p : access char_type) return cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:182
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE6lengthEPKc";

      function find
        (uu_s : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
         uu_a : access char_type) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:193
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE4findEPKcmRS2_";

      type char_type;
      function move
        (uu_s1 : access char_type;
         uu_s2 : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:205
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE4moveEPcPKcm";

      function copy
        (uu_s1 : access char_type;
         uu_s2 : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:255
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE4copyEPcPKcm";

      function assign
        (uu_s : access char_type;
         uu_n : cpp_12_1_0_aarch64_apple_darwin21_bits_cppconfig_h.size_t;
         uu_a : char_type) return access char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:274
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE6assignEPcmc";

      function to_char_type (uu_c : access int_type) return char_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:147
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE12to_char_typeERKm";

      type int_type;
      function to_int_type (uu_c : access char_type) return int_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:151
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE11to_int_typeERKc";

      function eq_int_type (uu_c1 : access int_type; uu_c2 : access int_type) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:155
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE11eq_int_typeERKmS3_";

      function eof return int_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:159
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE3eofEv";

      function not_eof (uu_c : access int_type) return int_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/char_traits.h:163
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZN9__gnu_cxx11char_traitsIcE7not_eofERKm";

   end;
   use char_traits_char;



end cpp_12_1_0_bits_char_traits_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
