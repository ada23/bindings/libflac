pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package cpp_12_1_0_bits_allocator_h is

   function operator_eq (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:200
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSteqRKSaIcES1_";

   function operator_eq (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:200
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSteqRKSaIwES1_";

   function operator_eq (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:200
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSteqRKSaIDsES1_";

   function operator_eq (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:200
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSteqRKSaIDiES1_";

   function operator_ne (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:205
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZStneRKSaIcES1_";

   function operator_ne (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:205
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZStneRKSaIwES1_";

   function operator_ne (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:205
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZStneRKSaIDsES1_";

   function operator_ne (arg1 : access constant allocator; arg2 : access constant allocator) return Extensions.bool  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/allocator.h:205
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZStneRKSaIDiES1_";

end cpp_12_1_0_bits_allocator_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
