pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_uwint_t_h;
limited with ustdio_h;
with Interfaces.C.Strings;
with sys_utypes_usize_t_h;
with arm_utypes_h;
with System;
limited with time_h;
with Interfaces.C.Extensions;

package wchar_h is

   function btowc (arg1 : int) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:97
   with Import => True, 
        Convention => C, 
        External_Name => "btowc";

   function fgetwc (arg1 : access ustdio_h.uu_sFILE) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:98
   with Import => True, 
        Convention => C, 
        External_Name => "fgetwc";

   function fgetws
     (arg1 : access wchar_t;
      arg2 : int;
      arg3 : access ustdio_h.uu_sFILE) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:99
   with Import => True, 
        Convention => C, 
        External_Name => "fgetws";

   function fputwc (arg1 : wchar_t; arg2 : access ustdio_h.uu_sFILE) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:100
   with Import => True, 
        Convention => C, 
        External_Name => "fputwc";

   function fputws (arg1 : access wchar_t; arg2 : access ustdio_h.uu_sFILE) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:101
   with Import => True, 
        Convention => C, 
        External_Name => "fputws";

   function fwide (arg1 : access ustdio_h.uu_sFILE; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:102
   with Import => True, 
        Convention => C, 
        External_Name => "fwide";

   function fwprintf (arg1 : access ustdio_h.uu_sFILE; arg2 : access wchar_t  -- , ...
      ) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:103
   with Import => True, 
        Convention => C, 
        External_Name => "fwprintf";

   function fwscanf (arg1 : access ustdio_h.uu_sFILE; arg2 : access wchar_t  -- , ...
      ) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:104
   with Import => True, 
        Convention => C, 
        External_Name => "fwscanf";

   function getwc (arg1 : access ustdio_h.uu_sFILE) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:105
   with Import => True, 
        Convention => C, 
        External_Name => "getwc";

   function getwchar return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:106
   with Import => True, 
        Convention => C, 
        External_Name => "getwchar";

   function mbrlen
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : sys_utypes_usize_t_h.size_t;
      arg3 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:107
   with Import => True, 
        Convention => C, 
        External_Name => "mbrlen";

   function mbrtowc
     (arg1 : access wchar_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : sys_utypes_usize_t_h.size_t;
      arg4 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:108
   with Import => True, 
        Convention => C, 
        External_Name => "mbrtowc";

   function mbsinit (arg1 : access constant arm_utypes_h.uu_mbstate_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:110
   with Import => True, 
        Convention => C, 
        External_Name => "mbsinit";

   function mbsrtowcs
     (arg1 : access wchar_t;
      arg2 : System.Address;
      arg3 : sys_utypes_usize_t_h.size_t;
      arg4 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:111
   with Import => True, 
        Convention => C, 
        External_Name => "mbsrtowcs";

   function putwc (arg1 : wchar_t; arg2 : access ustdio_h.uu_sFILE) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:113
   with Import => True, 
        Convention => C, 
        External_Name => "putwc";

   function putwchar (arg1 : wchar_t) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:114
   with Import => True, 
        Convention => C, 
        External_Name => "putwchar";

   function swprintf
     (arg1 : access wchar_t;
      arg2 : sys_utypes_usize_t_h.size_t;
      arg3 : access wchar_t  -- , ...
      ) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:115
   with Import => True, 
        Convention => C, 
        External_Name => "swprintf";

   function swscanf (arg1 : access wchar_t; arg2 : access wchar_t  -- , ...
      ) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:116
   with Import => True, 
        Convention => C, 
        External_Name => "swscanf";

   function ungetwc (arg1 : sys_utypes_uwint_t_h.wint_t; arg2 : access ustdio_h.uu_sFILE) return sys_utypes_uwint_t_h.wint_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:117
   with Import => True, 
        Convention => C, 
        External_Name => "ungetwc";

   function vfwprintf
     (arg1 : access ustdio_h.uu_sFILE;
      arg2 : access wchar_t;
      arg3 : arm_utypes_h.uu_darwin_va_list) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:118
   with Import => True, 
        Convention => C, 
        External_Name => "vfwprintf";

   function vswprintf
     (arg1 : access wchar_t;
      arg2 : sys_utypes_usize_t_h.size_t;
      arg3 : access wchar_t;
      arg4 : arm_utypes_h.uu_darwin_va_list) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:120
   with Import => True, 
        Convention => C, 
        External_Name => "vswprintf";

   function vwprintf (arg1 : access wchar_t; arg2 : arm_utypes_h.uu_darwin_va_list) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:122
   with Import => True, 
        Convention => C, 
        External_Name => "vwprintf";

   function wcrtomb
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : wchar_t;
      arg3 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:123
   with Import => True, 
        Convention => C, 
        External_Name => "wcrtomb";

   function wcscat (arg1 : access wchar_t; arg2 : access wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:124
   with Import => True, 
        Convention => C, 
        External_Name => "wcscat";

   function wcschr (arg1 : access wchar_t; arg2 : wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:125
   with Import => True, 
        Convention => C, 
        External_Name => "wcschr";

   function wcscmp (arg1 : access wchar_t; arg2 : access wchar_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:126
   with Import => True, 
        Convention => C, 
        External_Name => "wcscmp";

   function wcscoll (arg1 : access wchar_t; arg2 : access wchar_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:127
   with Import => True, 
        Convention => C, 
        External_Name => "wcscoll";

   function wcscpy (arg1 : access wchar_t; arg2 : access wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:128
   with Import => True, 
        Convention => C, 
        External_Name => "wcscpy";

   function wcscspn (arg1 : access wchar_t; arg2 : access wchar_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:129
   with Import => True, 
        Convention => C, 
        External_Name => "wcscspn";

   function wcsftime
     (arg1 : access wchar_t;
      arg2 : sys_utypes_usize_t_h.size_t;
      arg3 : access wchar_t;
      arg4 : access constant time_h.tm) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:130
   with Import => True, 
        Convention => C, 
        External_Name => "_wcsftime";

   function wcslen (arg1 : access wchar_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:132
   with Import => True, 
        Convention => C, 
        External_Name => "wcslen";

   function wcsncat
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:133
   with Import => True, 
        Convention => C, 
        External_Name => "wcsncat";

   function wcsncmp
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:134
   with Import => True, 
        Convention => C, 
        External_Name => "wcsncmp";

   function wcsncpy
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:135
   with Import => True, 
        Convention => C, 
        External_Name => "wcsncpy";

   function wcspbrk (arg1 : access wchar_t; arg2 : access wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:136
   with Import => True, 
        Convention => C, 
        External_Name => "wcspbrk";

   function wcsrchr (arg1 : access wchar_t; arg2 : wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:137
   with Import => True, 
        Convention => C, 
        External_Name => "wcsrchr";

   function wcsrtombs
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : System.Address;
      arg3 : sys_utypes_usize_t_h.size_t;
      arg4 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:138
   with Import => True, 
        Convention => C, 
        External_Name => "wcsrtombs";

   function wcsspn (arg1 : access wchar_t; arg2 : access wchar_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:140
   with Import => True, 
        Convention => C, 
        External_Name => "wcsspn";

   function wcsstr (arg1 : access wchar_t; arg2 : access wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:141
   with Import => True, 
        Convention => C, 
        External_Name => "wcsstr";

   function wcsxfrm
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:142
   with Import => True, 
        Convention => C, 
        External_Name => "wcsxfrm";

   function wctob (arg1 : sys_utypes_uwint_t_h.wint_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:143
   with Import => True, 
        Convention => C, 
        External_Name => "wctob";

   function wcstod (arg1 : access wchar_t; arg2 : System.Address) return double  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:144
   with Import => True, 
        Convention => C, 
        External_Name => "wcstod";

   function wcstok
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : System.Address) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:145
   with Import => True, 
        Convention => C, 
        External_Name => "wcstok";

   function wcstol
     (arg1 : access wchar_t;
      arg2 : System.Address;
      arg3 : int) return long  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:147
   with Import => True, 
        Convention => C, 
        External_Name => "wcstol";

   function wcstoul
     (arg1 : access wchar_t;
      arg2 : System.Address;
      arg3 : int) return unsigned_long  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:149
   with Import => True, 
        Convention => C, 
        External_Name => "wcstoul";

   function wmemchr
     (arg1 : access wchar_t;
      arg2 : wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:150
   with Import => True, 
        Convention => C, 
        External_Name => "wmemchr";

   function wmemcmp
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:151
   with Import => True, 
        Convention => C, 
        External_Name => "wmemcmp";

   function wmemcpy
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:152
   with Import => True, 
        Convention => C, 
        External_Name => "wmemcpy";

   function wmemmove
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:153
   with Import => True, 
        Convention => C, 
        External_Name => "wmemmove";

   function wmemset
     (arg1 : access wchar_t;
      arg2 : wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:154
   with Import => True, 
        Convention => C, 
        External_Name => "wmemset";

   function wprintf (arg1 : access wchar_t  -- , ...
      ) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:155
   with Import => True, 
        Convention => C, 
        External_Name => "wprintf";

   function wscanf (arg1 : access wchar_t  -- , ...
      ) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:156
   with Import => True, 
        Convention => C, 
        External_Name => "wscanf";

   function wcswidth (arg1 : access wchar_t; arg2 : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:157
   with Import => True, 
        Convention => C, 
        External_Name => "wcswidth";

   function wcwidth (arg1 : wchar_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:158
   with Import => True, 
        Convention => C, 
        External_Name => "wcwidth";

   function vfwscanf
     (arg1 : access ustdio_h.uu_sFILE;
      arg2 : access wchar_t;
      arg3 : arm_utypes_h.uu_darwin_va_list) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:170
   with Import => True, 
        Convention => C, 
        External_Name => "vfwscanf";

   function vswscanf
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : arm_utypes_h.uu_darwin_va_list) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:172
   with Import => True, 
        Convention => C, 
        External_Name => "vswscanf";

   function vwscanf (arg1 : access wchar_t; arg2 : arm_utypes_h.uu_darwin_va_list) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:174
   with Import => True, 
        Convention => C, 
        External_Name => "vwscanf";

   function wcstof (arg1 : access wchar_t; arg2 : System.Address) return float  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:175
   with Import => True, 
        Convention => C, 
        External_Name => "wcstof";

   function wcstold (arg1 : access wchar_t; arg2 : System.Address) return long_double  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:177
   with Import => True, 
        Convention => C, 
        External_Name => "wcstold";

   function wcstoll
     (arg1 : access wchar_t;
      arg2 : System.Address;
      arg3 : int) return Long_Long_Integer  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:180
   with Import => True, 
        Convention => C, 
        External_Name => "wcstoll";

   function wcstoull
     (arg1 : access wchar_t;
      arg2 : System.Address;
      arg3 : int) return Extensions.unsigned_long_long  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:182
   with Import => True, 
        Convention => C, 
        External_Name => "wcstoull";

   function mbsnrtowcs
     (arg1 : access wchar_t;
      arg2 : System.Address;
      arg3 : sys_utypes_usize_t_h.size_t;
      arg4 : sys_utypes_usize_t_h.size_t;
      arg5 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:195
   with Import => True, 
        Convention => C, 
        External_Name => "mbsnrtowcs";

   function wcpcpy (arg1 : access wchar_t; arg2 : access wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:197
   with Import => True, 
        Convention => C, 
        External_Name => "wcpcpy";

   function wcpncpy
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:198
   with Import => True, 
        Convention => C, 
        External_Name => "wcpncpy";

   function wcsdup (arg1 : access wchar_t) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:199
   with Import => True, 
        Convention => C, 
        External_Name => "wcsdup";

   function wcscasecmp (arg1 : access wchar_t; arg2 : access wchar_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:200
   with Import => True, 
        Convention => C, 
        External_Name => "wcscasecmp";

   function wcsncasecmp
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      n : sys_utypes_usize_t_h.size_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:201
   with Import => True, 
        Convention => C, 
        External_Name => "wcsncasecmp";

   function wcsnlen (arg1 : access wchar_t; arg2 : sys_utypes_usize_t_h.size_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:202
   with Import => True, 
        Convention => C, 
        External_Name => "wcsnlen";

   function wcsnrtombs
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : System.Address;
      arg3 : sys_utypes_usize_t_h.size_t;
      arg4 : sys_utypes_usize_t_h.size_t;
      arg5 : access arm_utypes_h.uu_mbstate_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:203
   with Import => True, 
        Convention => C, 
        External_Name => "wcsnrtombs";

   function open_wmemstream (uu_bufp : System.Address; uu_sizep : access unsigned_long) return access ustdio_h.uu_sFILE  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:205
   with Import => True, 
        Convention => C, 
        External_Name => "open_wmemstream";

   function fgetwln (arg1 : access ustdio_h.uu_sFILE; arg2 : access unsigned_long) return access wchar_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:215
   with Import => True, 
        Convention => C, 
        External_Name => "fgetwln";

   function wcslcat
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:216
   with Import => True, 
        Convention => C, 
        External_Name => "wcslcat";

   function wcslcpy
     (arg1 : access wchar_t;
      arg2 : access wchar_t;
      arg3 : sys_utypes_usize_t_h.size_t) return sys_utypes_usize_t_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/wchar.h:217
   with Import => True, 
        Convention => C, 
        External_Name => "wcslcpy";

end wchar_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
