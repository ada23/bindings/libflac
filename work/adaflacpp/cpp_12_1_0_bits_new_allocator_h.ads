pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
with Interfaces.C.Strings;

package cpp_12_1_0_bits_new_allocator_h is

   package uu_new_allocator_char32_t is
      type uu_new_allocator is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_uu_new_allocator return uu_new_allocator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:80
      pragma CPP_Constructor (New_uu_new_allocator, "_ZNSt15__new_allocatorIDiEC1Ev");

      procedure Delete_uu_new_allocator (this : access uu_new_allocator)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:90
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIDiED1Ev";

      function address (this : access constant uu_new_allocator; uu_x : reference) return pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:93
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIDiE7addressERDi";

      function address (this : access constant uu_new_allocator; uu_x : const_reference) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:97
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIDiE7addressERKDi";

      function allocate
        (this : access uu_new_allocator;
         uu_n : size_type;
         arg3 : System.Address) return access char32_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:112
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIDiE8allocateEmPKv";

      procedure deallocate
        (this : access uu_new_allocator;
         uu_p : access char32_t;
         uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIDiE10deallocateEPDim";

      function max_size (this : access constant uu_new_allocator) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:167
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIDiE8max_sizeEv";

      --  skipped func _M_max_size

   end;
   use uu_new_allocator_char32_t;

   package uu_new_allocator_char16_t is
      type uu_new_allocator is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_uu_new_allocator return uu_new_allocator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:80
      pragma CPP_Constructor (New_uu_new_allocator, "_ZNSt15__new_allocatorIDsEC1Ev");

      procedure Delete_uu_new_allocator (this : access uu_new_allocator)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:90
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIDsED1Ev";

      function address (this : access constant uu_new_allocator; uu_x : reference) return pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:93
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIDsE7addressERDs";

      function address (this : access constant uu_new_allocator; uu_x : const_reference) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:97
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIDsE7addressERKDs";

      function allocate
        (this : access uu_new_allocator;
         uu_n : size_type;
         arg3 : System.Address) return access char16_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:112
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIDsE8allocateEmPKv";

      procedure deallocate
        (this : access uu_new_allocator;
         uu_p : access char16_t;
         uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIDsE10deallocateEPDsm";

      function max_size (this : access constant uu_new_allocator) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:167
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIDsE8max_sizeEv";

      --  skipped func _M_max_size

   end;
   use uu_new_allocator_char16_t;

   package uu_new_allocator_wchar_t is
      type uu_new_allocator is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_uu_new_allocator return uu_new_allocator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:80
      pragma CPP_Constructor (New_uu_new_allocator, "_ZNSt15__new_allocatorIwEC1Ev");

      procedure Delete_uu_new_allocator (this : access uu_new_allocator)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:90
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIwED1Ev";

      function address (this : access constant uu_new_allocator; uu_x : reference) return pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:93
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIwE7addressERw";

      function address (this : access constant uu_new_allocator; uu_x : const_reference) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:97
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIwE7addressERKw";

      function allocate
        (this : access uu_new_allocator;
         uu_n : size_type;
         arg3 : System.Address) return access wchar_t  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:112
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIwE8allocateEmPKv";

      procedure deallocate
        (this : access uu_new_allocator;
         uu_p : access wchar_t;
         uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIwE10deallocateEPwm";

      function max_size (this : access constant uu_new_allocator) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:167
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIwE8max_sizeEv";

      --  skipped func _M_max_size

   end;
   use uu_new_allocator_wchar_t;

   package uu_new_allocator_char is
      type uu_new_allocator is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_uu_new_allocator return uu_new_allocator;  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:80
      pragma CPP_Constructor (New_uu_new_allocator, "_ZNSt15__new_allocatorIcEC1Ev");

      procedure Delete_uu_new_allocator (this : access uu_new_allocator)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:90
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIcED1Ev";

      function address (this : access constant uu_new_allocator; uu_x : reference) return pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:93
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIcE7addressERc";

      function address (this : access constant uu_new_allocator; uu_x : const_reference) return const_pointer  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:97
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIcE7addressERKc";

      function allocate
        (this : access uu_new_allocator;
         uu_n : size_type;
         arg3 : System.Address) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:112
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIcE8allocateEmPKv";

      procedure deallocate
        (this : access uu_new_allocator;
         uu_p : Interfaces.C.Strings.chars_ptr;
         uu_n : size_type)  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:142
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNSt15__new_allocatorIcE10deallocateEPcm";

      function max_size (this : access constant uu_new_allocator) return size_type  -- /opt/gcc-12.1.0-aarch64/include/c++/12.1.0/bits/new_allocator.h:167
      with Import => True, 
           Convention => CPP, 
           External_Name => "_ZNKSt15__new_allocatorIcE8max_sizeEv";

      --  skipped func _M_max_size

   end;
   use uu_new_allocator_char;



end cpp_12_1_0_bits_new_allocator_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
