pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_uint8_t_h;
with utypes_uuint8_t_h;
with sys_utypes_uint16_t_h;
with sys_utypes_uint32_t_h;
with sys_utypes_uint64_t_h;
with utypes_uuint16_t_h;
with utypes_uuint32_t_h;
with utypes_uuint64_t_h;

package FLAC_ordinals_h is

   subtype FLAC_u_int8 is sys_utypes_uint8_t_h.int8_t;  -- ../include/FLAC/ordinals.h:41

   subtype FLAC_u_uint8 is utypes_uuint8_t_h.uint8_t;  -- ../include/FLAC/ordinals.h:42

   subtype FLAC_u_int16 is sys_utypes_uint16_t_h.int16_t;  -- ../include/FLAC/ordinals.h:44

   subtype FLAC_u_int32 is sys_utypes_uint32_t_h.int32_t;  -- ../include/FLAC/ordinals.h:45

   subtype FLAC_u_int64 is sys_utypes_uint64_t_h.int64_t;  -- ../include/FLAC/ordinals.h:46

   subtype FLAC_u_uint16 is utypes_uuint16_t_h.uint16_t;  -- ../include/FLAC/ordinals.h:47

   subtype FLAC_u_uint32 is utypes_uuint32_t_h.uint32_t;  -- ../include/FLAC/ordinals.h:48

   subtype FLAC_u_uint64 is utypes_uuint64_t_h.uint64_t;  -- ../include/FLAC/ordinals.h:49

   subtype FLAC_u_bool is int;  -- ../include/FLAC/ordinals.h:51

   subtype FLAC_u_byte is FLAC_u_uint8;  -- ../include/FLAC/ordinals.h:53

end FLAC_ordinals_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
