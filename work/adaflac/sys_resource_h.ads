pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;
with sys_utypes_utimeval_h;
with System;
with utypes_uuint8_t_h;
with utypes_uuint64_t_h;
with utypes_uuint32_t_h;
with sys_utypes_uint32_t_h;
with sys_utypes_uid_t_h;

package sys_resource_h is

   PRIO_PROCESS : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:100
   PRIO_PGRP : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:101
   PRIO_USER : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:102

   PRIO_DARWIN_THREAD : constant := 3;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:105
   PRIO_DARWIN_PROCESS : constant := 4;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:106

   PRIO_MIN : constant := -20;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:112
   PRIO_MAX : constant := 20;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:113

   PRIO_DARWIN_BG : constant := 16#1000#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:120

   PRIO_DARWIN_NONUI : constant := 16#1001#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:126

   RUSAGE_SELF : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:140
   RUSAGE_CHILDREN : constant := -1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:141
   --  unsupported macro: ru_first ru_ixrss
   --  unsupported macro: ru_last ru_nivcsw

   RUSAGE_INFO_V0 : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:186
   RUSAGE_INFO_V1 : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:187
   RUSAGE_INFO_V2 : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:188
   RUSAGE_INFO_V3 : constant := 3;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:189
   RUSAGE_INFO_V4 : constant := 4;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:190
   RUSAGE_INFO_V5 : constant := 5;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:191
   RUSAGE_INFO_V6 : constant := 6;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:192
   --  unsupported macro: RUSAGE_INFO_CURRENT RUSAGE_INFO_V6

   RU_PROC_RUNS_RESLIDE : constant := 16#00000001#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:198
   --  unsupported macro: RLIM_INFINITY (((__uint64_t)1 << 63) - 1)
   --  unsupported macro: RLIM_SAVED_MAX RLIM_INFINITY
   --  unsupported macro: RLIM_SAVED_CUR RLIM_INFINITY

   RLIMIT_CPU : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:438
   RLIMIT_FSIZE : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:439
   RLIMIT_DATA : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:440
   RLIMIT_STACK : constant := 3;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:441
   RLIMIT_CORE : constant := 4;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:442
   RLIMIT_AS : constant := 5;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:443
   --  unsupported macro: RLIMIT_RSS RLIMIT_AS

   RLIMIT_MEMLOCK : constant := 6;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:446
   RLIMIT_NPROC : constant := 7;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:447

   RLIMIT_NOFILE : constant := 8;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:449

   RLIM_NLIMITS : constant := 9;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:451

   RLIMIT_WAKEUPS_MONITOR : constant := 16#1#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:470
   RLIMIT_CPU_USAGE_MONITOR : constant := 16#2#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:471
   RLIMIT_THREAD_CPULIMITS : constant := 16#3#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:472
   RLIMIT_FOOTPRINT_INTERVAL : constant := 16#4#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:473

   WAKEMON_ENABLE : constant := 16#01#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:478
   WAKEMON_DISABLE : constant := 16#02#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:479
   WAKEMON_GET_PARAMS : constant := 16#04#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:480
   WAKEMON_SET_DEFAULTS : constant := 16#08#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:481
   WAKEMON_MAKE_FATAL : constant := 16#10#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:482

   CPUMON_MAKE_FATAL : constant := 16#1000#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:487

   FOOTPRINT_INTERVAL_RESET : constant := 16#1#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:492

   IOPOL_TYPE_DISK : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:502
   IOPOL_TYPE_VFS_ATIME_UPDATES : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:503
   IOPOL_TYPE_VFS_MATERIALIZE_DATALESS_FILES : constant := 3;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:504
   IOPOL_TYPE_VFS_STATFS_NO_DATA_VOLUME : constant := 4;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:505
   IOPOL_TYPE_VFS_TRIGGER_RESOLVE : constant := 5;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:506
   IOPOL_TYPE_VFS_IGNORE_CONTENT_PROTECTION : constant := 6;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:507
   IOPOL_TYPE_VFS_IGNORE_PERMISSIONS : constant := 7;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:508
   IOPOL_TYPE_VFS_SKIP_MTIME_UPDATE : constant := 8;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:509
   IOPOL_TYPE_VFS_ALLOW_LOW_SPACE_WRITES : constant := 9;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:510
   IOPOL_TYPE_VFS_DISALLOW_RW_FOR_O_EVTONLY : constant := 10;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:511

   IOPOL_SCOPE_PROCESS : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:514
   IOPOL_SCOPE_THREAD : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:515
   IOPOL_SCOPE_DARWIN_BG : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:516

   IOPOL_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:519
   IOPOL_IMPORTANT : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:520
   IOPOL_PASSIVE : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:521
   IOPOL_THROTTLE : constant := 3;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:522
   IOPOL_UTILITY : constant := 4;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:523
   IOPOL_STANDARD : constant := 5;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:524
   --  unsupported macro: IOPOL_APPLICATION IOPOL_STANDARD
   --  unsupported macro: IOPOL_NORMAL IOPOL_IMPORTANT

   IOPOL_ATIME_UPDATES_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:531
   IOPOL_ATIME_UPDATES_OFF : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:532

   IOPOL_MATERIALIZE_DATALESS_FILES_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:534
   IOPOL_MATERIALIZE_DATALESS_FILES_OFF : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:535
   IOPOL_MATERIALIZE_DATALESS_FILES_ON : constant := 2;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:536

   IOPOL_VFS_STATFS_NO_DATA_VOLUME_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:538
   IOPOL_VFS_STATFS_FORCE_NO_DATA_VOLUME : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:539

   IOPOL_VFS_TRIGGER_RESOLVE_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:541
   IOPOL_VFS_TRIGGER_RESOLVE_OFF : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:542

   IOPOL_VFS_CONTENT_PROTECTION_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:544
   IOPOL_VFS_CONTENT_PROTECTION_IGNORE : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:545

   IOPOL_VFS_IGNORE_PERMISSIONS_OFF : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:547
   IOPOL_VFS_IGNORE_PERMISSIONS_ON : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:548

   IOPOL_VFS_SKIP_MTIME_UPDATE_OFF : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:550
   IOPOL_VFS_SKIP_MTIME_UPDATE_ON : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:551

   IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_OFF : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:553
   IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_ON : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:554

   IOPOL_VFS_DISALLOW_RW_FOR_O_EVTONLY_DEFAULT : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:556
   IOPOL_VFS_DISALLOW_RW_FOR_O_EVTONLY_ON : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:557

   subtype rlim_t is arm_utypes_h.uu_uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:89

   type rusage is record
      ru_utime : aliased sys_utypes_utimeval_h.timeval;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:153
      ru_stime : aliased sys_utypes_utimeval_h.timeval;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:154
      ru_maxrss : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:163
      ru_ixrss : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:165
      ru_idrss : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:166
      ru_isrss : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:167
      ru_minflt : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:168
      ru_majflt : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:169
      ru_nswap : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:170
      ru_inblock : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:171
      ru_oublock : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:172
      ru_msgsnd : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:173
      ru_msgrcv : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:174
      ru_nsignals : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:175
      ru_nvcsw : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:176
      ru_nivcsw : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:177
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:152

   type rusage_info_t is new System.Address;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:200

   type anon_array1901 is array (0 .. 15) of aliased utypes_uuint8_t_h.uint8_t;
   type rusage_info_v0 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:203
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:204
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:205
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:206
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:207
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:208
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:209
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:210
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:211
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:212
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:213
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:202

   type rusage_info_v1 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:217
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:218
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:219
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:220
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:221
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:222
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:223
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:224
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:225
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:226
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:227
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:228
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:229
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:230
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:231
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:232
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:233
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:216

   type rusage_info_v2 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:237
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:238
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:239
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:240
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:241
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:242
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:243
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:244
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:245
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:246
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:247
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:248
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:249
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:250
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:251
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:252
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:253
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:254
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:255
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:236

   type rusage_info_v3 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:259
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:260
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:261
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:262
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:263
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:264
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:265
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:266
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:267
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:268
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:269
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:270
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:271
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:272
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:273
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:274
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:275
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:276
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:277
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:278
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:279
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:280
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:281
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:282
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:283
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:284
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:285
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:286
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:258

   type rusage_info_v4 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:290
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:291
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:292
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:293
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:294
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:295
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:296
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:297
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:298
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:299
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:300
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:301
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:302
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:303
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:304
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:305
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:306
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:307
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:308
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:309
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:310
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:311
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:312
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:313
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:314
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:315
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:316
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:317
      ri_logical_writes : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:318
      ri_lifetime_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:319
      ri_instructions : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:320
      ri_cycles : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:321
      ri_billed_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:322
      ri_serviced_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:323
      ri_interval_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:324
      ri_runnable_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:325
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:289

   type rusage_info_v5 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:329
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:330
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:331
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:332
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:333
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:334
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:335
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:336
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:337
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:338
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:339
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:340
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:341
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:342
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:343
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:344
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:345
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:346
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:347
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:348
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:349
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:350
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:351
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:352
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:353
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:354
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:355
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:356
      ri_logical_writes : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:357
      ri_lifetime_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:358
      ri_instructions : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:359
      ri_cycles : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:360
      ri_billed_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:361
      ri_serviced_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:362
      ri_interval_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:363
      ri_runnable_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:364
      ri_flags : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:365
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:328

   type anon_array1916 is array (0 .. 13) of aliased utypes_uuint64_t_h.uint64_t;
   type rusage_info_v6 is record
      ri_uuid : aliased anon_array1901;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:369
      ri_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:370
      ri_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:371
      ri_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:372
      ri_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:373
      ri_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:374
      ri_wired_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:375
      ri_resident_size : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:376
      ri_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:377
      ri_proc_start_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:378
      ri_proc_exit_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:379
      ri_child_user_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:380
      ri_child_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:381
      ri_child_pkg_idle_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:382
      ri_child_interrupt_wkups : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:383
      ri_child_pageins : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:384
      ri_child_elapsed_abstime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:385
      ri_diskio_bytesread : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:386
      ri_diskio_byteswritten : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:387
      ri_cpu_time_qos_default : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:388
      ri_cpu_time_qos_maintenance : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:389
      ri_cpu_time_qos_background : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:390
      ri_cpu_time_qos_utility : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:391
      ri_cpu_time_qos_legacy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:392
      ri_cpu_time_qos_user_initiated : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:393
      ri_cpu_time_qos_user_interactive : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:394
      ri_billed_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:395
      ri_serviced_system_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:396
      ri_logical_writes : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:397
      ri_lifetime_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:398
      ri_instructions : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:399
      ri_cycles : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:400
      ri_billed_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:401
      ri_serviced_energy : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:402
      ri_interval_max_phys_footprint : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:403
      ri_runnable_time : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:404
      ri_flags : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:405
      ri_user_ptime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:406
      ri_system_ptime : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:407
      ri_pinstructions : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:408
      ri_pcycles : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:409
      ri_energy_nj : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:410
      ri_penergy_nj : aliased utypes_uuint64_t_h.uint64_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:411
      ri_reserved : aliased anon_array1916;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:412
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:368

   subtype rusage_info_current is rusage_info_v6;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:415

   type rlimit is record
      rlim_cur : aliased rlim_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:460
      rlim_max : aliased rlim_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:461
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:459

   type proc_rlimit_control_wakeupmon is record
      wm_flags : aliased utypes_uuint32_t_h.uint32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:495
      wm_rate : aliased sys_utypes_uint32_t_h.int32_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:496
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:494

   function getpriority (arg1 : int; arg2 : sys_utypes_uid_t_h.id_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:563
   with Import => True, 
        Convention => C, 
        External_Name => "getpriority";

   function getiopolicy_np (arg1 : int; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:565
   with Import => True, 
        Convention => C, 
        External_Name => "getiopolicy_np";

   function getrlimit (arg1 : int; arg2 : access rlimit) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:567
   with Import => True, 
        Convention => C, 
        External_Name => "_getrlimit";

   function getrusage (arg1 : int; arg2 : access rusage) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:568
   with Import => True, 
        Convention => C, 
        External_Name => "getrusage";

   function setpriority
     (arg1 : int;
      arg2 : sys_utypes_uid_t_h.id_t;
      arg3 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:569
   with Import => True, 
        Convention => C, 
        External_Name => "setpriority";

   function setiopolicy_np
     (arg1 : int;
      arg2 : int;
      arg3 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:571
   with Import => True, 
        Convention => C, 
        External_Name => "setiopolicy_np";

   function setrlimit (arg1 : int; arg2 : access constant rlimit) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/resource.h:573
   with Import => True, 
        Convention => C, 
        External_Name => "_setrlimit";

end sys_resource_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
