pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;

package sys_utypes_uin_port_t_h is

   subtype in_port_t is arm_utypes_h.uu_uint16_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_in_port_t.h:31

end sys_utypes_uin_port_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
