pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package assert_h is

   --  unsupported macro: assert(e) (__builtin_expect(!(e), 0) ? __assert_rtn(__func__, __ASSERT_FILE_NAME, __LINE__, #e) : (void)0)
   --  skipped func __assert_rtn

   --  skipped func __eprintf

end assert_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
