pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package FLAC_assert_h is

   --  arg-macro: procedure FLAC__ASSERT (x)
   --    assert(x)
   --  arg-macro: procedure FLAC__ASSERT_DECLARATION (x)
   --    x
end FLAC_assert_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
