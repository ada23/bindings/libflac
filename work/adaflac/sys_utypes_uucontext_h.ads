pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;
with sys_utypes_usigaltstack_h;
with arm_utypes_h;
limited with arm_umcontext_h;

package sys_utypes_uucontext_h is

   type uu_darwin_ucontext;
   type uu_darwin_ucontext is record
      uc_onstack : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:45
      uc_sigmask : aliased sys_utypes_h.uu_darwin_sigset_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:46
      uc_stack : aliased sys_utypes_usigaltstack_h.uu_darwin_sigaltstack;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:47
      uc_link : access uu_darwin_ucontext;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:48
      uc_mcsize : aliased arm_utypes_h.uu_darwin_size_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:49
      uc_mcontext : access arm_umcontext_h.uu_darwin_mcontext64;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:50
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:43

   subtype ucontext_t is uu_darwin_ucontext;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_ucontext.h:57

end sys_utypes_uucontext_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
