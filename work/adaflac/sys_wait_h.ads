pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with sys_utypes_upid_t_h;
with sys_utypes_uid_t_h;
limited with sys_signal_h;
limited with sys_resource_h;

package sys_wait_h is

   WNOHANG : constant := 16#00000001#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:121
   WUNTRACED : constant := 16#00000002#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:122

   WCOREFLAG : constant := 8#200#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:132
   --  arg-macro: function WEXITSTATUS (x)
   --    return (_W_INT(x) >> 8) and 16#000000ff#;
   --  arg-macro: function WSTOPSIG (x)
   --    return _W_INT(x) >> 8;
   --  arg-macro: function WIFCONTINUED (x)
   --    return _WSTATUS(x) = _WSTOPPED  and then  WSTOPSIG(x) = 16#13#;
   --  arg-macro: function WIFSTOPPED (x)
   --    return _WSTATUS(x) = _WSTOPPED  and then  WSTOPSIG(x) /= 16#13#;
   --  arg-macro: function WIFEXITED (x)
   --    return _WSTATUS(x) = 0;
   --  arg-macro: function WIFSIGNALED (x)
   --    return _WSTATUS(x) /= _WSTOPPED  and then  _WSTATUS(x) /= 0;
   --  arg-macro: function WTERMSIG (x)
   --    return _WSTATUS(x);
   --  arg-macro: function WCOREDUMP (x)
   --    return _W_INT(x) and WCOREFLAG;
   --  arg-macro: function W_EXITCODE (ret, sig)
   --    return (ret) << 8 or (sig);
   --  arg-macro: function W_STOPCODE (sig)
   --    return (sig) << 8 or _WSTOPPED;

   WEXITED : constant := 16#00000004#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:168

   WSTOPPED : constant := 16#00000008#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:171

   WCONTINUED : constant := 16#00000010#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:173
   WNOWAIT : constant := 16#00000020#;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:174

   WAIT_ANY : constant := (-1);  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:183
   WAIT_MYPGRP : constant := 0;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:184
   --  unsupported macro: w_termsig w_T.w_Termsig
   --  unsupported macro: w_coredump w_T.w_Coredump
   --  unsupported macro: w_retcode w_T.w_Retcode
   --  unsupported macro: w_stopval w_S.w_Stopval
   --  unsupported macro: w_stopsig w_S.w_Stopsig

   type idtype_t is 
     (P_ALL,
      P_PID,
      P_PGID)
   with Convention => C;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:83

   type anon_anon_3 is record
      w_Termsig : Extensions.Unsigned_7;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:201
      w_Coredump : Extensions.Unsigned_1;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:202
      w_Retcode : aliased unsigned_char;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:203
      w_Filler : aliased unsigned_short;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:204
   end record
   with Convention => C_Pass_By_Copy,
        Pack => True,
        Alignment => 4;
   type anon_anon_4 is record
      w_Stopval : aliased unsigned_char;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:220
      w_Stopsig : aliased unsigned_char;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:221
      w_Filler : aliased unsigned_short;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:222
   end record
   with Convention => C_Pass_By_Copy;
   type wait (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            w_status : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:195
         when 1 =>
            w_T : aliased anon_anon_3;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:212
         when others =>
            w_S : aliased anon_anon_4;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:229
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:194

   function wait (arg1 : access int) return sys_utypes_upid_t_h.pid_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:248
   with Import => True, 
        Convention => C, 
        External_Name => "_wait";

   function waitpid
     (arg1 : sys_utypes_upid_t_h.pid_t;
      arg2 : access int;
      arg3 : int) return sys_utypes_upid_t_h.pid_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:249
   with Import => True, 
        Convention => C, 
        External_Name => "_waitpid";

   function waitid
     (arg1 : idtype_t;
      arg2 : sys_utypes_uid_t_h.id_t;
      arg3 : access sys_signal_h.uu_siginfo;
      arg4 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:251
   with Import => True, 
        Convention => C, 
        External_Name => "_waitid";

   function wait3
     (arg1 : access int;
      arg2 : int;
      arg3 : access sys_resource_h.rusage) return sys_utypes_upid_t_h.pid_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:254
   with Import => True, 
        Convention => C, 
        External_Name => "wait3";

   function wait4
     (arg1 : sys_utypes_upid_t_h.pid_t;
      arg2 : access int;
      arg3 : int;
      arg4 : access sys_resource_h.rusage) return sys_utypes_upid_t_h.pid_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/wait.h:255
   with Import => True, 
        Convention => C, 
        External_Name => "wait4";

end sys_wait_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
