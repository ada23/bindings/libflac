pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_upthread_upthread_types_h;

package sys_upthread_upthread_mutexattr_t_h is

   subtype pthread_mutexattr_t is sys_upthread_upthread_types_h.uu_darwin_pthread_mutexattr_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_pthread/_pthread_mutexattr_t.h:31

end sys_upthread_upthread_mutexattr_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
