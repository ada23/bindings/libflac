pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with arm_utypes_h;

package sys_utypes_uva_list_h is

   subtype va_list is arm_utypes_h.uu_darwin_va_list;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_va_list.h:32

end sys_utypes_uva_list_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
