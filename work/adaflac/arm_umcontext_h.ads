pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with mach_arm_ustructs_h;

package arm_umcontext_h is

   type uu_darwin_mcontext32 is record
      uu_es : aliased mach_arm_ustructs_h.uu_darwin_arm_exception_state;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:43
      uu_ss : aliased mach_arm_ustructs_h.uu_darwin_arm_thread_state;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:44
      uu_fs : aliased mach_arm_ustructs_h.uu_darwin_arm_vfp_state;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:45
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:41

   type uu_darwin_mcontext64 is record
      uu_es : aliased mach_arm_ustructs_h.uu_darwin_arm_exception_state64;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:66
      uu_ss : aliased mach_arm_ustructs_h.uu_darwin_arm_thread_state64;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:67
      uu_ns : aliased mach_arm_ustructs_h.uu_darwin_arm_neon_state64;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:68
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:64

   type mcontext_t is access all uu_darwin_mcontext64;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/_mcontext.h:85

end arm_umcontext_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
