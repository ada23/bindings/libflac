pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;

package sys_utypes_upid_t_h is

   subtype pid_t is sys_utypes_h.uu_darwin_pid_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_pid_t.h:31

end sys_utypes_upid_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
