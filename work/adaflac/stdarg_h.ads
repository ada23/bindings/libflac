pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;

package stdarg_h is

   subtype uu_gnuc_va_list is System.Address;  -- /opt/gcc-12.1.0-aarch64/lib/gcc/aarch64-apple-darwin21/12.1.0/include/stdarg.h:40

end stdarg_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
