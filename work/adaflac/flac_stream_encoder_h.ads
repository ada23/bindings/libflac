pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;
with sys_utypes_usize_t_h;
with utypes_uuint32_t_h;
with FLAC_ordinals_h;
with Interfaces.C.Extensions;
limited with FLAC_format_h;
with FLAC_stream_decoder_h;
limited with ustdio_h;

package FLAC_stream_encoder_h is

   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_OK : constant := 0;  --  ../include/FLAC/stream_encoder.h:291
   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_NOT_COMPILED_WITH_MULTITHREADING_ENABLED : constant := 1;  --  ../include/FLAC/stream_encoder.h:292
   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_ALREADY_INITIALIZED : constant := 2;  --  ../include/FLAC/stream_encoder.h:293
   FLAC_u_STREAM_ENCODER_SET_NUM_THREADS_TOO_MANY_THREADS : constant := 3;  --  ../include/FLAC/stream_encoder.h:294

   type FLAC_u_StreamEncoderState is 
     (FLAC_u_STREAM_ENCODER_OK,
      FLAC_u_STREAM_ENCODER_UNINITIALIZED,
      FLAC_u_STREAM_ENCODER_OGG_ERROR,
      FLAC_u_STREAM_ENCODER_VERIFY_DECODER_ERROR,
      FLAC_u_STREAM_ENCODER_VERIFY_MISMATCH_IN_AUDIO_DATA,
      FLAC_u_STREAM_ENCODER_CLIENT_ERROR,
      FLAC_u_STREAM_ENCODER_IO_ERROR,
      FLAC_u_STREAM_ENCODER_FRAMING_ERROR,
      FLAC_u_STREAM_ENCODER_MEMORY_ALLOCATION_ERROR)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:281

   FLAC_u_StreamEncoderStateString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:288
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderStateString";

   type FLAC_u_StreamEncoderInitStatus is 
     (FLAC_u_STREAM_ENCODER_INIT_STATUS_OK,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_ENCODER_ERROR,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_UNSUPPORTED_CONTAINER,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_CALLBACKS,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_NUMBER_OF_CHANNELS,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_BITS_PER_SAMPLE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_SAMPLE_RATE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_BLOCK_SIZE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_MAX_LPC_ORDER,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_QLP_COEFF_PRECISION,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_BLOCK_SIZE_TOO_SMALL_FOR_LPC_ORDER,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_NOT_STREAMABLE,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_INVALID_METADATA,
      FLAC_u_STREAM_ENCODER_INIT_STATUS_ALREADY_INITIALIZED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:355

   FLAC_u_StreamEncoderInitStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:362
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderInitStatusString";

   type FLAC_u_StreamEncoderReadStatus is 
     (FLAC_u_STREAM_ENCODER_READ_STATUS_CONTINUE,
      FLAC_u_STREAM_ENCODER_READ_STATUS_END_OF_STREAM,
      FLAC_u_STREAM_ENCODER_READ_STATUS_ABORT,
      FLAC_u_STREAM_ENCODER_READ_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:381

   FLAC_u_StreamEncoderReadStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:388
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderReadStatusString";

   type FLAC_u_StreamEncoderWriteStatus is 
     (FLAC_u_STREAM_ENCODER_WRITE_STATUS_OK,
      FLAC_u_STREAM_ENCODER_WRITE_STATUS_FATAL_ERROR)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:401

   FLAC_u_StreamEncoderWriteStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:408
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderWriteStatusString";

   type FLAC_u_StreamEncoderSeekStatus is 
     (FLAC_u_STREAM_ENCODER_SEEK_STATUS_OK,
      FLAC_u_STREAM_ENCODER_SEEK_STATUS_ERROR,
      FLAC_u_STREAM_ENCODER_SEEK_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:424

   FLAC_u_StreamEncoderSeekStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:431
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderSeekStatusString";

   type FLAC_u_StreamEncoderTellStatus is 
     (FLAC_u_STREAM_ENCODER_TELL_STATUS_OK,
      FLAC_u_STREAM_ENCODER_TELL_STATUS_ERROR,
      FLAC_u_STREAM_ENCODER_TELL_STATUS_UNSUPPORTED)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:447

   FLAC_u_StreamEncoderTellStatusString : array (size_t) of Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:454
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__StreamEncoderTellStatusString";

   type FLAC_u_StreamEncoderProtected is null record;   -- incomplete struct

   type FLAC_u_StreamEncoderPrivate is null record;   -- incomplete struct

   type FLAC_u_StreamEncoder is record
      protected_u : access FLAC_u_StreamEncoderProtected;  -- ../include/FLAC/stream_encoder.h:470
      private_u : access FLAC_u_StreamEncoderPrivate;  -- ../include/FLAC/stream_encoder.h:471
   end record
   with Convention => C_Pass_By_Copy;  -- ../include/FLAC/stream_encoder.h:472

   type FLAC_u_StreamEncoderReadCallback is access function
        (arg1 : access constant FLAC_u_StreamEncoder;
         arg2 : access unsigned_char;
         arg3 : access unsigned_long;
         arg4 : System.Address) return FLAC_u_StreamEncoderReadStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:523

   type FLAC_u_StreamEncoderWriteCallback is access function
        (arg1 : access constant FLAC_u_StreamEncoder;
         arg2 : access unsigned_char;
         arg3 : sys_utypes_usize_t_h.size_t;
         arg4 : utypes_uuint32_t_h.uint32_t;
         arg5 : utypes_uuint32_t_h.uint32_t;
         arg6 : System.Address) return FLAC_u_StreamEncoderWriteStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:561

   type FLAC_u_StreamEncoderSeekCallback is access function
        (arg1 : access constant FLAC_u_StreamEncoder;
         arg2 : FLAC_ordinals_h.FLAC_u_uint64;
         arg3 : System.Address) return FLAC_u_StreamEncoderSeekStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:595

   type FLAC_u_StreamEncoderTellCallback is access function
        (arg1 : access constant FLAC_u_StreamEncoder;
         arg2 : access Extensions.unsigned_long_long;
         arg3 : System.Address) return FLAC_u_StreamEncoderTellStatus
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:640

   type FLAC_u_StreamEncoderMetadataCallback is access procedure
        (arg1 : access constant FLAC_u_StreamEncoder;
         arg2 : access constant FLAC_format_h.FLAC_u_StreamMetadata;
         arg3 : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:659

   type FLAC_u_StreamEncoderProgressCallback is access procedure
        (arg1 : access constant FLAC_u_StreamEncoder;
         arg2 : FLAC_ordinals_h.FLAC_u_uint64;
         arg3 : FLAC_ordinals_h.FLAC_u_uint64;
         arg4 : utypes_uuint32_t_h.uint32_t;
         arg5 : utypes_uuint32_t_h.uint32_t;
         arg6 : System.Address)
   with Convention => C;  -- ../include/FLAC/stream_encoder.h:682

   function FLAC_u_stream_encoder_new return access FLAC_u_StreamEncoder  -- ../include/FLAC/stream_encoder.h:698
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_new";

   procedure FLAC_u_stream_encoder_delete (encoder : access FLAC_u_StreamEncoder)  -- ../include/FLAC/stream_encoder.h:706
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_delete";

   function FLAC_u_stream_encoder_set_ogg_serial_number (encoder : access FLAC_u_StreamEncoder; serial_number : long) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:732
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_ogg_serial_number";

   function FLAC_u_stream_encoder_set_verify (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:748
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_verify";

   function FLAC_u_stream_encoder_set_streamable_subset (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:766
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_streamable_subset";

   function FLAC_u_stream_encoder_set_channels (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:778
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_channels";

   function FLAC_u_stream_encoder_set_bits_per_sample (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:794
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_bits_per_sample";

   function FLAC_u_stream_encoder_set_sample_rate (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:806
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_sample_rate";

   function FLAC_u_stream_encoder_set_compression_level (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:869
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_compression_level";

   function FLAC_u_stream_encoder_set_blocksize (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:884
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_blocksize";

   function FLAC_u_stream_encoder_set_do_mid_side_stereo (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:898
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_mid_side_stereo";

   function FLAC_u_stream_encoder_set_loose_mid_side_stereo (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:914
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_loose_mid_side_stereo";

   function FLAC_u_stream_encoder_set_apodization (encoder : access FLAC_u_StreamEncoder; specification : Interfaces.C.Strings.chars_ptr) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:996
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_apodization";

   function FLAC_u_stream_encoder_set_max_lpc_order (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1008
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_max_lpc_order";

   function FLAC_u_stream_encoder_set_qlp_coeff_precision (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1022
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_qlp_coeff_precision";

   function FLAC_u_stream_encoder_set_do_qlp_coeff_prec_search (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1036
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_qlp_coeff_prec_search";

   function FLAC_u_stream_encoder_set_do_escape_coding (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1048
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_escape_coding";

   function FLAC_u_stream_encoder_set_do_exhaustive_model_search (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1062
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_do_exhaustive_model_search";

   function FLAC_u_stream_encoder_set_min_residual_partition_order (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1085
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_min_residual_partition_order";

   function FLAC_u_stream_encoder_set_max_residual_partition_order (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1108
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_max_residual_partition_order";

   function FLAC_u_stream_encoder_set_num_threads (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1154
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_num_threads";

   function FLAC_u_stream_encoder_set_rice_parameter_search_dist (encoder : access FLAC_u_StreamEncoder; value : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1166
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_rice_parameter_search_dist";

   function FLAC_u_stream_encoder_set_total_samples_estimate (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_uint64) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1182
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_total_samples_estimate";

   function FLAC_u_stream_encoder_set_metadata
     (encoder : access FLAC_u_StreamEncoder;
      metadata : System.Address;
      num_blocks : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1265
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_metadata";

   function FLAC_u_stream_encoder_set_limit_min_bitrate (encoder : access FLAC_u_StreamEncoder; value : FLAC_ordinals_h.FLAC_u_bool) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1283
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_set_limit_min_bitrate";

   function FLAC_u_stream_encoder_get_state (encoder : access constant FLAC_u_StreamEncoder) return FLAC_u_StreamEncoderState  -- ../include/FLAC/stream_encoder.h:1293
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_state";

   function FLAC_u_stream_encoder_get_verify_decoder_state (encoder : access constant FLAC_u_StreamEncoder) return FLAC_stream_decoder_h.FLAC_u_StreamDecoderState  -- ../include/FLAC/stream_encoder.h:1305
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_verify_decoder_state";

   function FLAC_u_stream_encoder_get_resolved_state_string (encoder : access constant FLAC_u_StreamEncoder) return Interfaces.C.Strings.chars_ptr  -- ../include/FLAC/stream_encoder.h:1318
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_resolved_state_string";

   procedure FLAC_u_stream_encoder_get_verify_decoder_error_stats
     (encoder : access constant FLAC_u_StreamEncoder;
      absolute_sample : access Extensions.unsigned_long_long;
      frame_number : access unsigned;
      channel : access unsigned;
      sample : access unsigned;
      expected : access int;
      got : access int)  -- ../include/FLAC/stream_encoder.h:1337
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_verify_decoder_error_stats";

   function FLAC_u_stream_encoder_get_verify (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1347
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_verify";

   function FLAC_u_stream_encoder_get_streamable_subset (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1357
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_streamable_subset";

   function FLAC_u_stream_encoder_get_channels (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1367
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_channels";

   function FLAC_u_stream_encoder_get_bits_per_sample (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1377
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_bits_per_sample";

   function FLAC_u_stream_encoder_get_sample_rate (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1387
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_sample_rate";

   function FLAC_u_stream_encoder_get_blocksize (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1397
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_blocksize";

   function FLAC_u_stream_encoder_get_do_mid_side_stereo (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1407
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_mid_side_stereo";

   function FLAC_u_stream_encoder_get_loose_mid_side_stereo (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1417
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_loose_mid_side_stereo";

   function FLAC_u_stream_encoder_get_max_lpc_order (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1427
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_max_lpc_order";

   function FLAC_u_stream_encoder_get_qlp_coeff_precision (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1437
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_qlp_coeff_precision";

   function FLAC_u_stream_encoder_get_do_qlp_coeff_prec_search (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1447
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_qlp_coeff_prec_search";

   function FLAC_u_stream_encoder_get_do_escape_coding (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1457
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_escape_coding";

   function FLAC_u_stream_encoder_get_do_exhaustive_model_search (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1467
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_do_exhaustive_model_search";

   function FLAC_u_stream_encoder_get_min_residual_partition_order (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1477
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_min_residual_partition_order";

   function FLAC_u_stream_encoder_get_max_residual_partition_order (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1487
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_max_residual_partition_order";

   function FLAC_u_stream_encoder_get_num_threads (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1497
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_num_threads";

   function FLAC_u_stream_encoder_get_rice_parameter_search_dist (encoder : access constant FLAC_u_StreamEncoder) return utypes_uuint32_t_h.uint32_t  -- ../include/FLAC/stream_encoder.h:1507
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_rice_parameter_search_dist";

   function FLAC_u_stream_encoder_get_total_samples_estimate (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_uint64  -- ../include/FLAC/stream_encoder.h:1520
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_total_samples_estimate";

   function FLAC_u_stream_encoder_get_limit_min_bitrate (encoder : access constant FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1530
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_get_limit_min_bitrate";

   function FLAC_u_stream_encoder_init_stream
     (encoder : access FLAC_u_StreamEncoder;
      write_callback : FLAC_u_StreamEncoderWriteCallback;
      seek_callback : FLAC_u_StreamEncoderSeekCallback;
      tell_callback : FLAC_u_StreamEncoderTellCallback;
      metadata_callback : FLAC_u_StreamEncoderMetadataCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1593
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_stream";

   function FLAC_u_stream_encoder_init_ogg_stream
     (encoder : access FLAC_u_StreamEncoder;
      read_callback : FLAC_u_StreamEncoderReadCallback;
      write_callback : FLAC_u_StreamEncoderWriteCallback;
      seek_callback : FLAC_u_StreamEncoderSeekCallback;
      tell_callback : FLAC_u_StreamEncoderTellCallback;
      metadata_callback : FLAC_u_StreamEncoderMetadataCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1661
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_ogg_stream";

   function FLAC_u_stream_encoder_init_FILE
     (encoder : access FLAC_u_StreamEncoder;
      the_file : access ustdio_h.uu_sFILE;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1696
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_FILE";

   function FLAC_u_stream_encoder_init_ogg_FILE
     (encoder : access FLAC_u_StreamEncoder;
      the_file : access ustdio_h.uu_sFILE;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1731
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_ogg_FILE";

   function FLAC_u_stream_encoder_init_file
     (encoder : access FLAC_u_StreamEncoder;
      filename : Interfaces.C.Strings.chars_ptr;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1767
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_file";

   function FLAC_u_stream_encoder_init_ogg_file
     (encoder : access FLAC_u_StreamEncoder;
      filename : Interfaces.C.Strings.chars_ptr;
      progress_callback : FLAC_u_StreamEncoderProgressCallback;
      client_data : System.Address) return FLAC_u_StreamEncoderInitStatus  -- ../include/FLAC/stream_encoder.h:1803
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_init_ogg_file";

   function FLAC_u_stream_encoder_finish (encoder : access FLAC_u_StreamEncoder) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1831
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_finish";

   function FLAC_u_stream_encoder_process
     (encoder : access FLAC_u_StreamEncoder;
      buffer : System.Address;
      samples : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1858
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_process";

   function FLAC_u_stream_encoder_process_interleaved
     (encoder : access FLAC_u_StreamEncoder;
      buffer : access int;
      samples : utypes_uuint32_t_h.uint32_t) return FLAC_ordinals_h.FLAC_u_bool  -- ../include/FLAC/stream_encoder.h:1890
   with Import => True, 
        Convention => C, 
        External_Name => "FLAC__stream_encoder_process_interleaved";

end FLAC_stream_encoder_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
